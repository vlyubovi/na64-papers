\documentclass[pdftex,12pt,a4paper]{article}
%\usepackage[affil-it]{authblk}
\usepackage[margin=0.7in]{geometry}
\usepackage{multicol}
\usepackage{enumerate}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd}
\usepackage[pdftex]{graphicx}
\usepackage[font={small,it}]{caption}
\usepackage[margin=8pt]{subfig}
\usepackage{multirow}
\usepackage{longtable}
\usepackage{float}
\usepackage{hyperref}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand\g{\gamma}
\newcommand\ee{e^+e^-}
\newcommand\aee{A' \to e^+e^-}
\newcommand\ainv{A' \to invisible}
\newcommand\xdecay{X \rightarrow e^+  e^-}
\newcommand\pair{e^+ e^-}
\newcommand\reaction{e^- Z \to e^- Z A'; \ainv}
\newcommand\xee{X \to e^+e^-}
\newcommand\mee{m_{e^+e^-}}
\newcommand\angee{\Theta_{ e^+e^-}}


\begin{document} 

\title{\vspace{-1.5cm}{
\begin{center}
%\resizebox{7cm}
\includegraphics[scale=0.47]{na64-logo-pc.pdf}
\end{center}
\vskip1.cm
%%%%%%%%%%%%%%%%%%  Put here the correct number for your Note %%%%%%%%%%%%%%%%%%%
\begin{flushright}
\normalsize{\bf
NA64-Note-20-01-v1\\
\today}
%March 8 2017}
\end{flushright}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Put here the Title, Author list and the Abstract 
\vskip1.5cm
%\it{DRAFT}
%\vskip1.cm
\Large{\bf Reconstruction of the invariant mass  of $\ee$ pairs from the $\xee$ decays of a short-lived $X$  by  measuring two-track spatial separation.}}
%\author{NA64 Collaboration}
\author{S.N. Gninenko}
\date{\vspace{-5ex}}
\maketitle
\abstract{ The direct search for a new 16.7 MeV boson ($X$) which could explain the anomalous excess of $\ee$ pairs first 
observed in the excited $^8$Be$^*$ nucleus decays  became of a great importance, in particular,  due to the recent evidence for the existence of the X 
obtained from  the similar measurements performed with the excited $^4$He$^*$ nucleus.  
Due to its coupling to electrons, the X could be produced in the bremsstrahlung
reaction $e^- Z \to e^- Z  X$ by a 150 GeV $e^-$ beam  incident on an active WCAL of the optimal thickness  in the NA64 experiment at the CERN SPS and 
observed through the subsequent decay into a $\ee$ pair. To reconstruct the invariant mass of the pair with a high  precision, 
the $\ee$ opening angle $\angee$ has to be measured with the accuracy $\lesssim$ a few $10^{-5}$ rad, which is quite a challenge.
 An alternative solution, described in this note,  is based on the reconstruction of $\angee$ through the measurements of
 the spatial $e^+$- and $e^-$-track separation  at a large, $\simeq 10$ m,  distance from the $X$ production target. 
For these  measurements to be performed with a high signal efficiency  and  the mass 
  resolution  $\Delta \mee/\mee \lesssim 10\%$,   good double-track separation and reconstruction accuracy for  the distance  between the tracks $\lesssim 1$ mm by  the NA64 tracker  is  required. 
\par In this note we discuss the proposed technique and  estimate its precision.  Preliminary  results obtained by using the simplified  Monte Carlo simulations of the overlaid tracks
for the efficiency and accuracy of overlaid two tracks separation as a function of the distance between them are reported. Several 
 algorithms for the double-track event reconstruction are studied.  We also discuss possibility to improve 
the mass resolution by  using the measured energy of the well separated $e^+, ~e^-$ tracks in the ECAL, instead of  their momenta in the case of the 
significantly overlaid   tracks.
}

\newpage

\section{Motivation}
The ATOMKI experiment of Krasznahorkay et al. \cite{be8} has reported the observation of a 6.8 $\sigma$ excess of events
in the invariant mass distributions of $\pair$ pairs produced in the nuclear transitions of excited  $^8Be^*$ to its ground state via
internal pair creation.  This anomaly can be interpreted as the emission of a new 
protophobic gauge $X$ boson with a mass of 16.7 MeV followed by its $\xdecay$ decay assuming that the $X$ has
non-universal coupling to quarks, coupling
to electrons in the range $2\times 10^{-4} \lesssim \epsilon_e \lesssim 1.4\times 10^{-3}$ and the lifetime 
$10^{-14}\lesssim \tau_X \lesssim 10^{-12}$~s \cite{feng1,feng2}. %Interestingly, such relatively large charged lepton couplings can also resolve
%the so-called ($g_\mu - 2$ ) anomaly, a discrepancy between measured and predicted values of the muon anomalous magnetic moment.
It has motivated worldwide theoretical and experimental 
efforts towards light  and weakly coupled  vector bosons,  see, e.g. \cite{mb}-\cite{pf}. 

\par The aim of the NA64 experiment at the CERN SPS  is to search for dark photon mediator ($A'$) of the dark matter production in invisible decay mode  by using 100 GeV electrons from the H4 beam. 
The NA64 detector is described in detail in Ref.\cite{na64prd}. Below only its main features will be briefly mentioned. 
The experiment intends to observe 
$A'$s through  their  production in the reaction of high-energy electron scattering off heavy nucleus, $\reaction$ followed by the  $\ainv$ decay. The produced  $A'$s are identified through their invisible decays using the experimental signature 
of the scattered electron energy deposition in the ECAL target below 50 GeV and no energy energy deposition in the hadronic calorimeter.

\par In this note we discuss the method of the invariant mass $\mee$ reconstruction of $\ee$ pairs from the $\xee$decays of the short-lived $X$ by using measurements of the two-track separation in the NA64 tracker. For this technique the accurate reconstruction of the distance $L_{\ee}$ between the outgoing $e^+$ and  $e^-$ tracks is crucial.
In this note we also describe several algorithms for the most accurate $L_{\ee}$  value reconstruction considering as 
an example  a simplified model of clusters in the GEM tracker. The details of the double-track reconstruction 
in the Micromegas chambers can be found in the recent work of Ref.\cite{pc-two-track}.
 
 A simple model to describe the lateral shape of a cluster  in terms of charge recorded from strips  has been considered. We discuss the performance of the method to estimate the 
 double-track distance  from  the overlaid clusters  due to their transverse shape fluctuation. Starting from these functions, a simple  algorithm is developed for estimate  the 
 efficiency  and acceptance of the GEM chambers. The Monte Carlo simulation of the GEM clusters reproduces the  data collected in the experiment well therefore allowing the application of the algorithm at different detector configurations  without studying  with  the beam. For the successful run  in 2021, a few important questions which  remains to be studied are discussed in the  Summary section.  
 
\section{Method of the $\ee$ invariant mass reconstruction}
 \par The calorimetric method of the search for $\xee$ decays used by NA64 is described in \cite{na64be}. Its  application
 to the case of the  $\xee$  decay is straightforward. Briefly, a 
  high-energy electron beam is sent into an electromagnetic (e-m) calorimeter that serves as an active beam dump.
Typically the beam electron loses all its  shower energy in the dump.  If the 
$X$ exists, due to the $e-X$ coupling it would occasionally be produced    by a shower electron (or positron) 
in its scattering off a nuclei of the dump:
\begin{equation}
e^- + Z \to e^- + Z + X;~ X \to \ee .
\label{ea}
\end{equation}
Since the $X$ is penetrating and reasonably longer-lived, it would escape the beam dump, and
subsequently decays into an $\ee$ pair in a downstream set
of detectors. The pair energy would be equal to the energy missing
from the dump. The apparatus is designed to identify and measure
the energy of the $\ee$ pair in another calorimeter (ECAL).
Thus, the signature of the  $X \to \ee$ decay  is an event with two e-m-like showers
in the detector: one shower in the dump, and another one in the ECAL with the sum energy  equal to the beam energy.   

As discussed previously, the reconstruction of the invariant mass $\mee$ is crucial for the confirmation of the ATOMKI results.  
 As the remained unconstrained parameter space for the e-X coupling $\epsilon_e$  corresponds to a extremely short-lived X's with the lifetime 
 $\tau_X \lesssim 10^{-13}$ s, there are two related to the following  problems. First, the decay length of the $X$ with a mass $m_X$ and energy $E_X$ is given by 
\begin{equation}
L_X = 28.3 ~{\rm mm}  \Bigl[\frac{E_X}{100~ \text{GeV}}\Bigr] \Bigl[\frac{17~ \text{MeV}}{m_X}\Bigr]^2 \Bigl[\frac{10^{-3}}{\epsilon_e}\Bigr]^2
\label{length}
\end{equation}
Hence, the energy, $E_X$,  of the produced X in the reaction \eqref{ea}  has to be at least $E_X\gtrsim 100$ GeV,  in order to have 
the decay length or $L_X \gtrsim 30$ mm  allowing  the  $X$ to escape the WCAL dump due to the Lorentz boost.
Second,  as the X is relatively light, and its  energy
 is high, $E_X \gg \mee$, the minimal $\ee$ opening  angle is given by
\begin{equation} 
\angee^{min} \simeq  \frac{2\mee}{E_X},
\label{angle}
 \end{equation}
  As the invariant mass is 
 \begin{equation}
\mee = [E_{e^+} E_{e^-}]^{1/2} \angee
\label{mass}
\end{equation}
in order to measure it with accuracy $\Delta\mee/\mee \simeq 10\%$
 the angle $\angee$ has to be measured with precision higher than a few $10^{-5}$ rad, which is a difficult experimental  problem.  
 For the energy $E_X\simeq 100$ GeV, the angle is   $\angee \simeq 0.34$ mrad, which is quite small to be measured with precision $\lesssim 10\%$.
 
 The proposed method  of the invariant mass $\mee$ reconstruction is based on the following \cite{sng-talk}:
 \begin{itemize}
\item  as the $X$ is a short-lived particle, its decay vertex $Z_X$ is located in the vicinity of the WCAL, and as 
$Z_X \simeq L_X \simeq ~ c\tau_X E_X/m_X \ll L_D$ with $L_D=Z_{GEM1}-Z_{WC}$, $Z_X$ can be taken as $Z_X \simeq Z_{WC}+ <L_X>$, where $L_D$ is the decay 
base for the $X$ decay, and $Z_{WC}, Z_{GEM1}$ are $Z$-coordinate of the WCAL dump and the first GEM1 chamber, see Fig. \ref{fig:setup-2021}.
\item  the opening angle $\angee$ in this case can be evaluated as 
\begin{equation}
\angee \simeq \frac{ L_{\ee}}{L_D}
\label{angle-est}
\end{equation}
where $ L_{\ee}$ is the the distance between two tracks in GEM1 plane, and $L_D$ is the decay base.   
\end{itemize}

 \begin{figure}[tbh!!]
\centering
\vspace{-.0cm}{\includegraphics[width=.9\textwidth]{setup_vis_2021-2.png}}% Here is how to import EPS art
\vspace{-.0cm}{\caption{ Schematic illustration of the NA64 setup proposed for  the search for  the $\ainv$ decay in the 2021 run.
The massive HCV veto hadronic calorimeter in the SRD downstream region is used to reject large angle hadronic secondaries.\label{fig:setup-2021}}}
\end{figure}   
\section{Detector configuration for the 2021 run }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The NA64 setup configuration capable of measuring the invariant mass $\mee$  in the 2021 run is schematically shown in Fig.\ref{fig:setup-2021}, see Ref.\cite{vap-talk}. Compare to the 2018 setup version  it includes 
\begin{itemize}
\item new shorter WCAL dump with the total thickness $\simeq 30~X_0$, and a new WCAL veto counter 
\item additional  magnetic spectrometer using new MBPL magnet 
\item new  ECAL calorimeter with the larger transverse size than previously used 
\end{itemize}
The experiment  employs the 150 GeV electron beam at the new H4 beam zone  in the North Area (NA) of the CERN SPS.
%The beam delivers $\simeq 5\times 10^6~e^-$ per SPS spill of 4.8 s produced by the primary 400 GeV proton beam.
Two  GEM1(MM1) and GEM2(MM2)  chambers located upstream (downstream) the MBPL are used to detect the $\pair$ pairs and to measure the momenta of the decay  $e^+, ~e^-$ tracks. 
%The detector is additionally  equipped with a MBPL magnetand a tracker, which is a set of two-three gas electron multiplier (GEM1, 2) stations and  two-three sets ofdownstream Micromegas (MM1,2) chambers (T1, T2) for the outgoing  decay $e^-, ~e^+$  track measurements  \cite{Banerjee:2015eno}.
 The dump is  a compact e-m calorimeter WCAL
made as short as possible by doubling the absorber layer thickness to maximise the sensitivity to short lifetimes while keeping the leakage of particles at a small level.
In order not to spoil the mass resolution of the spectrometer due to multiple scattering in the detector, great care is devoted to minimise the amount of material in the active area of the detector. 
To this end,   a vacuum vessel of 8-10 v long is installed after the WCAL  to minimise multiple $\ee$ scattering, 
 The material budget of one detector, corresponding to the measurement of two projections of a particle trajectory, amounts to 0.4\% of a radiation length in the center.
The tracker is  followed by the bigger transverse size ECAL for  enhancing  the electron-positron pair  identification and  measuring  the energy of the decay $e^+, ~e^-$ , which is a matrix of $12\times 6 $ shashlik-type modules .
%The WCAL(ECAL) was assembled from the tungsten(lead) and plastic scintillator plates with wave lengths shifting fiber read-out.
The ECAL has $\simeq 40$ radiation lengths ($X_0$) and is located at a distance $\simeq 3.5$~m from the MBPL.
Downstream of the ECAL the detector is equipped with a high-efficiency veto counter, and a hermetic hadron
calorimeter (HCAL)  used as a hadron veto and for muon identification.
%The reaction \eqref{ea} typically occurs within the first few radiation lengths ($X_0$) of the WCAL. 
% For the mass range $1 \leq m_{A'} \leq~25$ MeV and energy $E_{A'}\gtrsim 20$ GeV, the opening angle $\Theta_{\ee} \simeq 2 m_{A'}/E_{A'} \lesssim 2$
%mrad of the decay $\ee$ pair is too small to be resolved in the tracker T3-T4, and the pairs are mostly detected as a single-track
%e-m  shower in the ECAL.
% At distances larger than $\simeq$ 5 m from the WCAL, the distance between the hits is  $\gtrsim 5$ mm, so the $\ee$ pair  can be  resolved in two separated tracks in the T3 and T4 tracker stations. 

The single electron events is assumed to be  collected with the hardware trigger  
\begin{equation}
Tr(X) =  PS(>E^{th}_{PS}) \cdot (WCAL < E^{th}_{WC})\cdot (ECAL> E^{th}_{EC})
\label{trigger}
\end{equation}
designed to accept events with   in-time hits in  beam-defining counters $S_i$ and clusters in the PS and ECAL with the energy thresholds  $ E^{th}_{PS}\simeq 0.3$ GeV,  $E^{th}_{WC} \lesssim 30$ , and   $E^{th}_{EC} \gtrsim 80$ GeV, respectively.  

\begin{figure}
    \includegraphics[width=.4\textwidth]{two-track-00mm.pdf}\hfill
    \includegraphics[width=.4\textwidth]{two-track-04mm.pdf}\hfill
      \\[\smallskipamount]
    \includegraphics[width=.4\textwidth]{two-track-08mm.pdf}\hfill
     \includegraphics[width=.4\textwidth]{two-track-12mm.pdf}\hfill
  \\[\smallskipamount]
    \includegraphics[width=.4\textwidth]{two-track-2mm.pdf}\hfill
        \includegraphics[width=.4\textwidth]{two-track-4mm.pdf}
          \vskip0.5cm      
    \caption{Examples of fit results obtained with MINUIT fitting program are shown for the cases of $ L_{\ee} =$  0, 0.4, 0.8, 1.2, 2.0 and 4.0 mm simulated distance between two tracks. For the case of $\L_{\ee} = 0$ mm, the algorithm based on the value  $\chi^2/ndf$  identified the event as a single  cluster, while for the case  $ L_{\ee} = 0.4$ mm the fit of the single maximum cluster with a single cluster shape results in a bad  value of $\chi^2/ndf$..}\label{fig:two-track-A}
\end{figure}
\begin{figure}
\vspace{-1cm}{
    \includegraphics[width=.55\textwidth]{fit-00mm.png}\hfill
    \includegraphics[width=.55\textwidth]{fit-04mm.png}\hfill
      \\[\smallskipamount]
%         \caption{Some examples of fits of two tracks at different distances in X projection.}\label{fig:fit}
\vskip0.5cm
    \includegraphics[width=.55\textwidth]{fit-08mm.png}\hfill
     \includegraphics[width=.55\textwidth]{fit-12mm.png}\hfill
  \\[\smallskipamount]
%     \caption{Some examples of fits of two tracks at different distances in X projection.}\label{fig:fit}
\vskip0.5cm
    \includegraphics[width=.55\textwidth]{fit-2mm.png}\hfill
        \includegraphics[width=.55\textwidth]{fit-4mm.png}
          \vskip0.5cm  
    \caption{Examples of the printed  fit  results of two tracks at different distances in X projection with the algorithm A. See, text.}\label{fig:fit-A}}
\end{figure}

\section{Simulation of two-track events}
\par In the GEM detectors used in NA64 the electron cloud induces a fast signal on the readout anode, which is segmented in two sets of about 800 strips with a pitch of 400 
$\mu$m each, perpendicular to each other. For each decay electron  trajectory one detector consequently records two projections of the track with  correlated amplitudes.
The simplified simulations of this process used in this note were performed.
\par  In the  GEM model it was assumed  that the adjacent strips in each GEM projection form a cluster with the average number of 
strips per cluster at the 10\% of the maximal amplitude in the strips equal to 3.5, which is  in a reasonable  consistent with the lateral diffusion of the charge cloud in the GEM stack. The GEM response to $e^+,~e^-$ was simulated  without taking into account the geometry of the GEM chamber, induced charge  generation and propagation, electronic noise, etc. The  charge distribution  among the  strips for the single track has been chosen to be of the Gaussian shape such that with an effective  400 $\mu$m width strips the single Gaussian fit of the cluster results in $\simeq 65-70~\mu$m space resolution for position of the cluster in each GEM plane \cite{mh-talk}. In this simplified model the fluctuations of the 
cluster amplitude  were in the range $\pm 20\%$, and the  fluctuations of the transverse shape of the cluster were assumed to be just statistical. 
Fig. \ref{fig:two-track-A} shows a few examples of the distributions of charge in adjacent strips for simulated two-track events  for different distance between the clusters. For  definiteness we will consider below events in the GEM X plane.

\section{Analysis of two-track events and results}
 At the first step an interval of strips with one or two separated maximal  strip signals was selected. For the cases considered, typically that was a range of  5-10 strips in total for a single X or Y  GEM chamber plane.  The distribution first was checked for the single-cluster or 
two-cluster hypotheses.  The choice was made  based  on the goodness of the fit criteria ($\chi^2/ndf$). 
\par For the selected strip interval for two-track candidate events on could identified  two extreme  cases  a) of well separated two clusters and b) the case of  completely overlapped clusters. See examples shown in Fig. \ref{fig:two-track-A}. 
 For the selected two-track strip range  three different A, B, and C   algorithms were used to reconstruct parameters of the tracks. 
The first step for all the three algorithm was  the same:  
 \begin{enumerate}[(i)]
\item first it finds the strip with maximum charge amplitude, which should be greater than the certain threshold. 
 This strip 
and $\pm 2$  adjacent strips make the first track interval of strips. To reduce the bias in the analysis caused by a higher noise in overlaid events the 
charge threshold per strip might be preselected. This defines group of  strips form the first cluster candidate. The cluster  was fitted to  Gaussian
with  free three parameters: peak value (p1), mean (p2)  and sigma (p3) of the distribution for the  algorithm A; and  Gaussian with the two free parameters, peak value (p1) and 
position (p2) and fixed  value of sigma was used for both B and C algorithms. 
 
\item then, outside the selected range of strips after removal of charge amplitudes of the first cluster
the algorithm find the second strip with the maximal highest charge amplitude.  This strip 
and $\pm 2$  adjacent strips make the second track interval of strips.  the second cluster was fitted also to Gaussian with the three free parameters (p3,p4,p5) 
for algorithm A, and two parameters (p3,p4) for the algorithms B and C.    

\item then fit values obtained  for  each preselected track  with Gaussian function , shown by dashed red and blue curves in Fig.\ref{fig:two-track-A} for algorithm A,
 Fig.\ref{fig:two-track-B-C} for algorithm B, and C  were considered  as an input for the 
global fit of the sum interval of two track candidate with a double Gaussian shape  : \\
a) using the  algorithms A, when  both  clusters were fitted to Gaussian  with  free three parameters. This corresponds to the case when the  shape of the GEM cluster 
has large event-to-event transverse fluctuations and can be described by a Gaussian with a fixed sigma only on average.   
An example of 
fit results is shown in Fig.\ref{fig:fit-A}. where the three parameters p1(p4), p2(p5), and p3(p6) are, respectively  peak value, mean  and sigma  of the distribution for the 
first(second) cluster \\
b)  using the  algorithms B,  when the fit of the each cluster was performed by Gaussian with the two free parameters, peak value (p1, p3) and (p2, p4)  
position and fixed  value of sigma. This (and C) is the case when transverse fluctuations of the cluster shape are assumed to be small. 
The fit results are  shown in Fig.\ref{fig:two-track-B-C} and Fig.\ref{fig:fit-B-C}, right columns. And, finally, \\
c)  using  the  algorithm C when the fit was performed for the first cluster by Gaussian with the two free parameters, peak value and 
position and fixed  value of sigma, and for the second  one  by  Gaussian with only a single  free parameter - peak
position,  and fixed  values of the amplitude,  found from the constraint on the general normalisation of the total  charge  of both clusters, and  fixed sigma value.
  The fit results are  shown in Fig.\ref{fig:two-track-B-C} and Fig.\ref{fig:fit-B-C}, left columns. 

\item Note, that in the  case when the double-track event is identified as a single track event in the X-plane, it still can be  can be used
 for calculation of the distance between the tracks if the event is identified as a double track one  in the Y-plane.  
 \end{enumerate}


\begin{figure}
 \includegraphics[width=.5\textwidth]{fit-08mm-3par-full.png} \hfill
  \includegraphics[width=.5\textwidth]{fit-08mm-4par-full.png}\hfill
     \\[\smallskipamount]
    \includegraphics[width=.5\textwidth]{fit-12mm-3par-full.png}\hfill
     \includegraphics[width=.5\textwidth]{fit-12mm-4par-full.png}\hfill
  \\[\smallskipamount]
    \includegraphics[width=.5\textwidth]{fit-16mm-3par-full.png}\hfill
        \includegraphics[width=.5\textwidth]{fit-16mm-4par-full.png}
          \vskip0.5cm      
    \caption{Examples of fits of two tracks at different distances in X projection with the algorithms B (right column) and C(left column).}\label{fig:two-track-B-C}
\end{figure}



\begin{figure}
 \includegraphics[width=.5\textwidth]{fit-08mm-3par.png} \hfill
  \includegraphics[width=.5\textwidth]{fit-08mm-4par.png}\hfill
     \\[\smallskipamount]
    \includegraphics[width=.5\textwidth]{fit-12mm-3par.png}\hfill
     \includegraphics[width=.5\textwidth]{fit-12mm-4par.png}\hfill
  \\[\smallskipamount]
    \includegraphics[width=.5\textwidth]{fit-16mm-3par.png}\hfill
        \includegraphics[width=.5\textwidth]{fit-16mm-4par.png}
          \vskip0.5cm      
    \caption{Examples of the final fit results  of two tracks at different distances in X projection.with the algorithms B (right column) and C(left column)}\label{fig:two-track-B-C-fin}
\end{figure}


\begin{figure}
 \includegraphics[width=.5\textwidth]{fitpr-08mm-3par.png} \hfill
  \includegraphics[width=.5\textwidth]{fitpr-08mm-4par.png}\hfill
     \\[\smallskipamount]
    \includegraphics[width=.5\textwidth]{fitpr-12mm-3par.png}\hfill
     \includegraphics[width=.5\textwidth]{fitpr-12mm-4par.png}\hfill
  \\[\smallskipamount]
    \includegraphics[width=.5\textwidth]{fitpr-16mm-3par.png}\hfill
        \includegraphics[width=.5\textwidth]{fitpr-16mm-4par.png}
          \vskip0.5cm      
    \caption{Examples of the printed fit results of two tracks at different distances in X projection with the algorithms B (right column) and C(left column) .}\label{fig:fit-B-C}
\end{figure}





In Fig.\ref{fig:two-track-eff} we show the efficiency for identifying double clusters as a function of the distance between the two clusters. Note that our analysis will produce a slightly lower estimate of the efficiency because of a $\sqrt{2}$ noise increase due to the overlaying of events. One sees that a 90\% efficiency for distinguishing double-clusters is reached when the clusters are separated by at least one and a half the strip pitch ($\gtrsim 0.6$ mm).
   

\begin{figure}[tbh!!]
\centering
\vspace{-.0cm}{\includegraphics[width=.5\textwidth]{two-track-eff.pdf}}% Here is how to import EPS art
\vspace{-.0cm}{\caption{  Efficiency for reconstructing two clusters in the GEM X-plane  as a function of the distance between them.\label{fig:two-track-eff}}}
\end{figure}   

\begin{figure}[tbh!!]
\centering
\includegraphics[width=.5\textwidth]{two-track-error-1.pdf}\includegraphics[width=.5\textwidth]{two-track-error-3-4par.pdf}                                                                                                                                                                                                                                                                                                                                                                                                                      
\vspace{-.0cm}{\caption{ Accuracy for reconstructing position of each of two clusters in the GEM X-plane as a function of the distance between them.
The left panel shows the results obtained by using algorithm A. The results obtained with algorithms B and C are presented on the right panel. 
The curve on the left panel shows  fit to the  points with the function $f(x)=150exp[-(x-0.4)^2/0.11]+10exp[-(x-1)^2/0.5]+70$. \label{fig:two-track-error}}}
\end{figure}   


In Fig. \ref{fig:two-track-eff} efficiency for reconstructing two clusters in the GEM X-plane  as a function of the distance between them is shown. 
One can see that the efficiency is $\gtrsim 0.6$ for the distance between tracks $ L_{\ee} \gtrsim 0.8$ mm.  
\par Fig. \ref{fig:two-track-error}  shows the reconstructed position resolution of the each tracks for double track events  as a function of the distance between them .
 The degradation of the position resolution for each of the close tracks is limited to about 50\% of the readout strip pitch. This might be  not adequate for precision track reconstruction, although such a measurement may still be used for the pattern recognition in matching of tracks.  
  A limited degradation of the GEM position resolution for close tracks with the distance $ L_{\ee} \gtrsim 0.8$ mm is  observed. In these case instead of the track momentum reconstruction one can use  the $e^+$ and $e^-$ energy measurement in the ECAL which is assumed to be performed with a reasonable accuracy as shown in Table \ref{tab:errors}  It is seen that for the well separated tracks all three algorithms gives similar results corresponding to accuracy 
 $\simeq 65-70~\mu$m, while for overlaid tracks the algorithm C provides slightly better results.  
%The bottom region  represents the same  sample of events as the ones shown in the left panel after imposing 
%HCALpass cut which reject mostly events with the HCAL energy deposition topology significantly different from those expected from the hadronic shower produced in the ECAL.  
 \begin{table}[hbt]
\caption{The comparison  between the simulated  and reconstructed values of the distance between two tracks obtained with algorithms A and C  for several distance between the tracks.}
\vskip 0.5cm
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
True distance &  Algorithm A  & Algorithm C & Two-track fit (A)   \\
 $L_{\ee}$ ,   mm &$L_{rc}$  mm &$L_{rc}$  mm & $\chi^2$/ndf  \\
\hline
\hline
0 & 0$\pm$0.07  &  -- & 0.908/1 \\
 \hline
0.4 & 0$\pm$0.235 & 0$\pm$0.175& 2.52/1\\
\hline
0.8 & 0.91$\pm$0.13& 0.87$\pm$0.09&0.42/2 \\
\hline
1.2 & 1.17$\pm$0.084& 1.25$\pm$0.066&0.59/3\\
\hline
1.6 & --& 1.58$\pm$0.064&\\
\hline 
2.0 & 1.93$\pm$0.075& --&0.44/5\\
\hline 
4.0 & 3.97$\pm$0.07&--&0.62/6 \\
\hline 
\hline
\end{tabular}
\end{center}
\label{tab:numbers}
\end{table}

%\newpage 

\begin{table}[tbh!!]
\caption{The expected error budget.}
\vskip 0.5cm
\begin{center}
\begin{tabular}{|c|c|c|}
\hline
Item & Errors &  Comment \\
\hline 
&&\\
$e^+,e^-$ momenta &  $\frac{\Delta p_{e^+}}{p_{e^+}} \simeq \frac{\Delta p_{e^-}}{p_{e^-}} \simeq 0.02$ & 
$\frac{\Delta p_{e^+}}{p_{e^+}} \simeq \frac{0.1}{\sqrt{p[GeV]}}$ in the ECAL \\
measurements &&\\
\hline
&&\\ 
distance  $L_{\ee}$ &  $0.05\lesssim \frac{\Delta L_{\ee}^{reco}}{L_{\ee}^{reco}}\lesssim 0.1$& for  $0.8\lesssim L_{\ee}\lesssim 2$ mm \\
between tracks,   mm & &  \\
%&&\\
\hline
&&\\
decay base  $ L_{D}$ &  $\frac{\Delta L_{D}}{L_{D}} \simeq \frac{L_X}{L_D} \lesssim  0.01$ & Assuming $L_D \simeq 10$ m \\
 & &  and $\epsilon_e \simeq 10^{-3}$ \\
%&&\\
\hline
&&\\
Total expected&  $\Delta \mee/\mee \lesssim 1$ MeV  &  Assuming $L_{\ee} \gtrsim 0.8$ mm \\
 \hline
\hline
\end{tabular}
\end{center}
\label{tab:errors}
\end{table}

 \par The results obtained allows one to estimate the expected overall error budget for the $\mee$ reconstruction, which are summarized in Table \ref{tab:errors}
for different algorithms used. 
 E.g. for coupling $\epsilon_e \simeq 10^{-3}$ the $X$-decay length  $L_X \simeq 30$ mm, and for $\epsilon_e \simeq 3\cdot 10^{-4}$, $L_X \simeq 30$ cm, hence
contributing to $\Delta \mee/\mee \lesssim 10\%$  (~ 2 MeV).
Finally, when two-track candidate events are identified and the tracks are reconstructed, one can use the following up-down  procedure for invariant mass $\mee$ reconstruction:
 \begin{enumerate}[(i)]
\item  Match the outgoing $e^+$ and $e^-$ tracks to the corresponding ECAL clusters, which are assumed to be well  separated (by at least about two-cell size) due to use of MBPL.
\item  Extrapolate outgoing tracks back and match them to incoming $e^+$ and $e^-$ tracks
\item Reconstruct energy and momenta,  $E_{e^+}, P_{e^+}$  and  $E_{e^-}, P_{e^-}$ of decay  tracks
\item  Reconstruct  the distance $\Delta L_{\ee}$ between  incoming tracks in the first  GEM1 station
\item Calculate $\mee$  by using  Eqs.(4,5)
 \end{enumerate}

\newpage
 
\section{Conclusion}
%%%%%%%%%%%%%%%%%
In this note we  discuss the method of the invariant mass $\mee$ reconstruction of $\ee$ pairs from the $\xee$ decays of 
the short-lived $X$ by using measurements of the two-track separation in the NA64 tracker. As an example we
consider a simplified model of the transverse  cluster shape in the GEM tracker for the induced charge on the strips. Three algorithms have been studied for reconstruction of 
the overlaid tracks. 
\par The estimate shows that slightly better  results were obtained with the algorithm C, that fit double-track distribution of charge among 
the strips  by fixing the width of the clusters and using the total clusters charge for the normalisation.
The resulting track X,Y coordinate reconstruction errors depend on the distance $L_{\ee}$ between the track in the corresponding plane 
and varies from $\simeq 65-70~\mu$m to $\simeq 100-200 \mu$m for the well separated  $L_{\ee} \gtrsim 1$ mm and overlaid   $L_{\ee} \lesssim 0.8$ mm
tracks, respectively.  Due to these  uncertainties the estimated total error for the $\mee$  reconstruction is varied from $\simeq \pm 1$ to $\simeq \pm 3$ MeV. 
The estimate also shows that for the NA64 detector upgrade,  a longer distance between  the WCAL dump and the first GEM1 station is 
preferable to increase  the $<L_{\ee}>$.    For the successful run  in 2021, a few important questions which  remains to be studied are discussed in the  last Section.  
  
\par The results presented in this note provide just the an estimate of the two-track capability of GEMS. For application these results to the run in 2021, 
it would be  important:  
 \begin{itemize}
 \item  to produce more realistic curves of two-track separation efficiency and individual  reconstruction track position accuracy shown in Figs. \ref{fig:two-track-eff} and 
 Fig.\ref{fig:two-track-error} extracted from the data themselves for both MMs and GEM;
 \item  to evaluate with these curves more  realistic invariant mass resolution reconstructed with the proposed  method as the function of the $X$ mass and 
 coupling constant $\epsilon_e$; 
 \item to evaluate realistic background level for $\mee$
 \item based on these to evaluate  what should be the minimal  allowed amount of  $n_{eot}$ to make $5\sigma$ statement about either closing the $X$ case or 
 declare the $X$ observation;
\item to answer the question if background level is significant - what are the means for its reduction?
\item what should be the optimal  composition and  configuration of the detector in order to keep background  below  a few part in 10$^{13}$,
\item how to optimise and   compromise the WCAL  length and  W$_2$ veto thickness and  acceptance and efficiency to make the upgrade cost-effective.  
\end{itemize} 
 \vskip0.5cm
{\large \bf Acknowledgements}\\
I  am grateful to B. Ketzer,  V.~Poliakov,  and V. Volkov  for useful comments and discussions.

\begin{thebibliography}{99}
\bibitem{be8}
A. Krasznahorkay et al., Phys. Rev. Lett. {\bf 116}, 042501 (2016).
%A. Krasznahorkay et al., Observation of Anomalous Internal Pair Creation in 8Be: A Possible Indication of a Light, Neutral Boson, Phys. Rev. Lett. 116, 042501 (2016).

\bibitem{feng1} J. Feng 
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo,  Phys. Rev. Lett. {\bf 117}, 071803 (2016). 

\bibitem{feng2}
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo,  Phys. Rev. D {\bf 95}, 035017 (2017).

\bibitem{mb}
M. Battaglieri  et al.., US Cosmic Visions: New Ideas in Dark Matter 2017: Community Report, arXiv:1707.04591.

\bibitem{nardi}
E. Nardi, C. D.R. Carvajal, A. Ghoshal, D. Meloni, M. Raggi,  arXiv:1802.04756. 

\bibitem{jk} J. Kozaczuk, Phys. Rev. D {\bf 97}, 015014 (2018).

\bibitem{cheng} 
Ch.-W. Chiang and   P.-Y.Tseng,  Phys. Lett. B {\bf 767}, 289 (2017). 

%\cite{Zhang:2017zap}
\bibitem{Zhang:2017zap}
  X.~Zhang and G.~A.~Miller,
  %``Can nuclear physics explain the anomaly observed in the internal pair production in the Beryllium-8 nucleus?,''
  Phys.\ Lett.\ B {\bf 773}, 159 (2017)
  

\bibitem{ia}
I. Alikhanov, E. A. Paschos,  arXiv:1710.10131. 

\bibitem{liang}	
Y. Liang, L.-B. Chen, C.-F. Qiao, Chin. Phys. C {\bf 41},  063105 (2017). 

\bibitem{bart}
B. Fornal, Int. J. Mod. Phys. A {\bf 32},  1730020 (2017).

\bibitem{pf}
P. Fayet, Eur. Phys. J. C  {\bf 77}, 53 (2017)


\bibitem{Banerjee:2015eno}
  D.~Banerjee, P.~Crivelli and A.~Rubbia,
  ``Beam Purity for Light Dark Matter Search in Beam Dump Experiments,''
  Adv.\ High Energy Phys.\  {\bf 2015},  105730 (2015).

\bibitem{na64prl} 
 D.~Banerjee {\it et al.} [NA64 Collaboration],
 ``Search for invisible decays of sub-GeV dark photons in missing-energy events at the CERN SPS,''
  Phys.\ Rev.\ Lett.\  {\bf 118},   011802 (2017).
\bibitem{na64prd} 

  D.~Banerjee {\it et al.} [NA64 Collaboration],
  ``Search for vector mediator of Dark Matter production in invisible decay mode'', Phys. Rev. D {\bf 97},  072002 (2018).
\bibitem{na64be} 
  D.~Banerjee {\it et al.} [NA64 Collaboration],
  ``Search for a new X(16.7) boson and dark photons in the NA64 experiment at CERN'', Phys. Rev. Lett. {\bf 120}, 231802 (2018). 
  \bibitem{Read:2002hq}
  A.~L.~Read,
  ``Presentation of search results: The CL(s) technique,''
  J.\ Phys.\ G {\bf 28}, 2693 (2002).

\bibitem{sng-talk}
S.N. Gninenko, "Method for searching for the $\xee$ decay with the MM tracker and $m_{\ee}$ mass reconstruction", Talk given at the NA64 analysis meeting, CERN, Friday 29 Nov, 2019. 

\bibitem{vap-talk}
V.A. Poliakov, "New zone PPE144, status and plan", Talk given at the NA64 analysis meeting, CERN, Friday 6 Dec, 2019. 

\bibitem{mmk-talk}
M.M. Kirsanov,  "Simulations of the $\xee$ decay for estimate of $m_X$ reconstruction", Talk given at the NA64 analysis meeting, CERN, Monday 16 Dec, 2019. 

\bibitem{ed-talk}
E. Depero, "Simulations of the setup for $\xee$ decay search", Talk given at the NA64 analysis meeting, CERN, Monday 16 Dec, 2019.

\bibitem{mh-talk}
M. H\"osgen, "Study on two-track separation power of triple GEM detectors at the NA64 Experiment", Talk given at the NA64 analysis meeting, CERN, Friday 6 Dec, 2019. 
\bibitem{pc-two-track}
 B. Radics, G. Janka, D. A. Cooke, S. Procureur, and P. Crivelli,  
"Double hit reconstruction in large area multiplexed detectors", Rev. Sci. Instr. 90, 093305 (2019); https://doi.org/10.1063/1.5109315 

\end{thebibliography}
 \end{document}  

