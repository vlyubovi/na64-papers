\documentclass[pdftex,12pt,a4paper]{article}
%\usepackage[affil-it]{authblk}
\usepackage[margin=0.7in]{geometry}
\usepackage{multicol}
\usepackage{enumerate}
\usepackage{siunitx}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd}
\usepackage[pdftex]{graphicx}
\usepackage[font={small,it}]{caption}
\usepackage[margin=8pt]{subfig}
%\usepackage{multirow}
\usepackage{longtable}
\usepackage{float}
%\usepackage{hyperref}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand\g{\gamma}
\newcommand\ee{e^+e^-}
\newcommand\aee{X\to e^+e^-}
\newcommand\ainv{X\to invisible}
\newcommand\xdecay{X \rightarrow e^+  e^-}
\newcommand\mudecay{\gamma + Z \rightarrow \mu^ +  \mu^-}
\newcommand\pair{e^+ e^-}

\begin{document} 

\title{\vspace{-1.5cm}{
\begin{center}
%\resizebox{7cm}
%\includegraphics[scale=0.47]{na64-logo-pc.pdf}
\end{center}
\vskip1.cm
%%%%%%%%%%%%%%%%%%  Put here the correct number for your Note %%%%%%%%%%%%%%%%%%%
\begin{flushright}
\normalsize{\bf
\today}
%March 8 2017}
\end{flushright}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Put here the Title, Author list and the Abstract 
\vskip3.5cm
\Large{\bf SPS June report }}
%\author{NA64 Collaboration}
\author{Emilio Depero}
\date{\vspace{-5ex}}
\maketitle
\newpage

\section{Study of $\mudecay$ interactions using trackers}
\subsection{Simulation of $\mudecay$ in 2018 visible mode setup}

The data from a selected dimuon sample are compared to a detailed MC simulation. The simulation was performed using 150 \si{GeV} electrons as primaries and by biasing the cross section with a factor 200 for the interaction $\mudecay$. The $\sim 8 \cdot 10^5$ events containing a dimuon production are then used for the comparison with the data. To avoid a biased sample all the cuts used for the selection in the data are applied to the simulation as well without assuming any prior knowledge over the nature of the event. These cuts will be reviewed in detail in Sec.\ref{sec:methods:data}.

A realistic beam profile was extracted from the electron calibration run 4263. The sample was further purified by requiring recorded energy between 5 \si{MeV} and 100 \si{MeV} for both SRD counters. The beam profile was then recovered by fitting the XY position recorded by the two upstream Micromegas with a 2D Gaussian. The two fits agree within 100 $\mu$m precision for both $\sigma_X \approx 4.13$ mm and $\sigma_y \approx 1.40$ mm. Fig.\ref{fig:dimuon:gemspectra} shows a comparison of the reconstructed hit-position after all these corrections were applied to the simulation in the GEM module 4 using a dimuon sample extracted from the data. All other modules show a similar behaviour.

To improve the agreement between data and Monte Carlo several strategies were used. Artificial smearing of 60 $\mu m$ is applied to the hits recorded in the simulation to reproduce the limited spatial resolution of the GEMs. This number was estimated by checking residuals of tracks after selecting different triplets for the track reconstruction and comparing the reconstructed hit to the one predicted by the tracking procedure. Hits are then separated in X-Y projection and knowledge on the original hit XY combination is no longer assumed. Hits closer than 1.75 mm are merged in a single one as they cannot be resolved reliably by the GEM detectors. This number was estimated using true cluster recorded by the GEM detectors during electron run in 2017: pure 1-particle clusters were merged offline at different distances to study the performance of the clusterization algorithm as a function of the distance between pure 1-particle cluster. At merging distance roughly equal to four strips the efficiency was found to rapidly go to zero. The hits defined in this procedure are used as input for the same reconstruction algorithm used for the data.

The reconstruction chain works as follows: first, the hits are grouped in track candidates by requiring the angle between the line connecting the hits recorded on the first and last pair of GEMs to have an angle smaller than 9 \si{mrad}. After that, track candidates are reconstructed using a Kalman filter implemented using the Genfit package \cite{genfit}. An example $\chi^2$ distribution of the fits to the track candidates can be seen in Fig. \ref{fig:dimuon:chisq2} where the algorithm is applied to reconstruct single tracks coming from MIP simulated in the 2018 setup. Finally, all track candidates are grouped in pairs to form possible vertex candidates. The exact position of the vertex is obtained by back-propagating the tracks at the point of minimal distance. Before being paired, track candidates are selected to have no common hit and are not considered if their minimal distance is larger than 3 mm. After all vertex candidates are recovered, a final set of cuts is applied to select only events that are compatible with the kinematics of the signal. These cuts will be discussed more in-depth in Sec.\ref{sec:methods:comparison}.


\begin{figure}[h!]
  \begin{center}
    \includegraphics[scale=0.8]{plots/GEM4_rel.pdf}    
  \end{center}
  \caption{Hit position recorded in last GEM before ECAL for MC simulated (Red curve) and data (Blue dots) $\mudecay$ events}
  \label{fig:dimuon:gemspectra}
\end{figure}

\begin{figure}[h!]
  \begin{center}
    \includegraphics[scale=0.3]{plots/chireco.pdf}    
  \end{center}
  \caption{$\chi^2$ obtained from reconstructed track candidates using simulated sample of $\pi^-$ in 2018 visible mode setup. In red a $\chi^2$ distribution for four degrees of freedom is shown.}
  \label{fig:dimuon:chisq2}
\end{figure}

\label{sec:methods:simulation}

\newpage

\subsection{$\mudecay$ events recorded in 2018 visible mode setup}
  

A dimuon sample was selected using all events collected during the visible mode of 2018. The beam quality was improved by requiring zero energy deposited in the Catcher placed off-beam upstream the WCAL and a reconstructed momentum in the range between 140 and 160 \si{GeV}. This was done to ensure a well-defined energy distribution not biased by the physical trigger employed in the experiment. A cut of 2 \si{GeV} $<$E$_{hcal} <$ 6.35 \si{GeV} for the first two HCAL modules placed after the ECAL and energy of less than 90 \si{GeV} in the WCAL selects dimuons events. The cuts on the HCAL modules is used to select a clean sample of dimuons leaving a double-MIP signature, where the third HCAL module is excluded to increase the acceptance and the additional cut on the WCAL is used to further suppress contribution coming from the region affected by the physical trigger. Finally,  an energy deposited of at least 1 MIP is required in the scintillator downstream the WCAL (S4), while at least 1.8 MIP is requested in the Veto behind the ECAL. The less strict cut on the S4 is justified by its limited thickness which makes it not suitable for precise energy measurement contrary to the 5 cm thick Veto used for the double-MIP requirement. This last cuts is used not only to increase the purity of dimuon in the sample, but also to select a sample with kinematics closer to the one expected from an hypothetical dark matter candidate. This makes the comparison with the Monte Carlo more significant for our search.

Although these cuts are expected to select mainly dimuon coming from $\gamma$ produced in the em-shower generated by the 150 \si{GeV} electron in the beam, a contribution is also expected from the hadron contamination in the beam, as $\pi^-$ have several channels to mimic such signature. The physical trigger employed in the experiment further increases such contribution, as the requirement of low energy deposit in the WCAL bias the beam composition to particles with high penetration power. To solve this issue a cut on the SRD detector and the pre-shower of WCAL is used. These cuts were used in the past for NA64 analysis and are expected to reject hadrons and muons at a level $<10^{-5}$ . To cross-check that the contamination is correctly removed, an independent method based on the beam profile is used. The beam profile significantly differs between electrons and hadrons as the H4 beamline is tuned for electrons. Both profiles are recovered from the data using a calibration run of electron/hadron respectively. The ratio between the two is estimated by mixing the two templates obtained from the calibration runs until the best agreement with the measured beam profile is reached using a  $\chi^2$-test. The result is summarized in Fig.\ref{fig:dimuon:profile}, where the beam profile of events selected with the dimuon-selection cuts discussed above are compared before and after the SRD criteria is applied. Before the SRD cut, the fits predict a contamination of roughly 50\% of hadron in the sample, this is again due to the bias added by the physical trigger. The same fit predicts contamination of 85\% if no selection for dimuon is applied (normal data acquired with physical trigger). After the cuts, the beam profile converges to the templates obtained in the calibration run, with the fit returning a value of contamination compatible with zero.

\begin{figure}[h!]
  \begin{center}
    \includegraphics[scale=0.8]{plots/beamspot.pdf}
  \end{center}
  \caption{Beam profile recorded by MM1(first Micromegas module upsteram) for hadron calibration run (blue dots), electron calibration run (red line), events selected with dimuons cuts from data collected with the physical trigger (black line) and those same events after SRD cut is applied (green square). Fits using the templates obtained from the calibration run show a level of contamination of $\sim$50\% in the dimuon sample. }
  \label{fig:dimuon:profile}
\end{figure}

\label{sec:methods:data}

\newpage

\subsection{Comparison}

To show that there are no significant differences in the tracking procedure between simulation and data the reconstructed angle and the energy deposited in the WCAL were used as figure of merit. The excellent agreement of the energy deposited in the WCAL in $\mudecay$ events was already shown in a previous analysis (Sec. \ref{placeholder}). If the tracking procedure affects differently data and MC, one would expect the agreement between the two distributions to diverge after cuts based on vertex reconstruction are applied to the sample. Following the procedure described in Sec.\ref{sec:methods:simulation}, a number of vertex candidates are selected for the comparison. As the interaction $\mudecay$ will have their vertex inside the WCAL, only vertex compatible with this assumption are selected for the comparison. In practice, a vertex is accepted if its position lies within 3$\sigma$ of the expected WCAL position, where $\sigma$ was fitted using a Gaussian from the distribution of pure dimuon pair selected from the simulation. After all such cuts are applied, the angle of the surviving vertex candidates is compared between simulation and data. Fig.\ref{fig:dimuon:wangle} shows the correlation between energy deposited and angle reconstructed for all vertex candidates selected. The energy deposition for data and simulation are in excellent agreement after all selection criteria are applied. The comparison with the angle is also reasonable, with the peak of the angle distribution being 2.9 \si{mrad} for simulation and 2.7 \si{mrad} for data with the two distribution matching to a good degree. It can be seen that a larger tail in the reconstructed angle is observed in the simulation. By integrating the number of events from 0 to 10 \si{mrad} one can quantify this difference to $\sim$6\%. As the typical dark matter scenario predicts an angle $<$2\si{mrad} and a vertex behind the WCAL this is not expected to affect our signal yield.


\begin{figure}[h!]
  \begin{center}
    \includegraphics[scale=0.8]{plots/wangle.pdf}    
  \end{center}
  \caption{Top: Energy deposited in the WCAL vs reconstructed angle of all vertex within 3$\sigma$ from the WCAL position for simulated $\mudecay$ (left) and a sample of data from 2018 visible mode run with selection criteria for dimuon (right). The two bottom plots show the comparison of the two projection for the two groups. Data are drawn with blue dots, simulation is plotted through a red line. }
  \label{fig:dimuon:wangle}
\end{figure}

This comparison shows that the distribution predicted by the MC does not change significantly when cuts over vertex position are applied. However when such cuts are applied, there still can be lower efficiency in the data compared to Monte Carlo. The reason for this are the inefficiencies of the GEM modules, fail of clusterization in some events or differences in the efficiency of the tracking procedure due to the simplifications used in the MC. The cuts applied to the sample are divided into four steps: i) at least two hits per GEM are required in the decay volume as a minimal condition for tracking. ii) events with a GEM module recording more than 5 hits are rejected as well from the analysis as incompatible with a single $\mudecay$ vertex in the decay volume. The chance of particles in the decay volume to emit enough secondary to reach the level of this cut was estimated to be 10$^{-3} <$ from the simulation. The MC predicts 68\% of $\mudecay$ to be within the GEM acceptance of the 2018 setup. This low acceptance is explained from the fact that the GEMs position is optimized to resolve very close two hits track coming from the decay of $X$ and is therefore less efficient for dimuon searches.  These first two cuts do not depend on the track-fitting procedure but instead on the clusterization performed by CORAL \cite{coral} and the efficiency of the GEMs modules, which are not present in the simulation. A factor of 0.8 is used to account for this as result of the comparison between data and Monte Carlo. iii) the tracking procedure is applied to the events that survived the two first requirements, only events with at least one vertex with minimal distance between tracks lower than 3 mm are selected. iv) the last cut requires the vertex position to be compatible with a vertex inside the decay volume, using the same cuts described above. The number of events surviving the requirement of at least one vertex in the decay volume was found to be slightly smaller in the data, notably the disagreement between the ratio of good vertex reconstructed inside the decay volume is $<$1\%. Accounting the smaller efficiency in the vertex-reconstruction in the data on the top of what was estimated for the hits reconstruction, a total factor of 0.77 less efficiency is estimated in the data within respect to the Monte Carlo. This factor is used for the correction of estimated signal yield in 2018 for the analysis involving tracking procedures. A summary of the efficiency can be found in Table \ref{tab:dimuon:efficiencies}.


\begin{table}
  \centering
  \begin{tabular}{|l|c|c|c|}
    \hline
    cut & efficiency MC & efficiency Data & MC / DATA \\
    \hline
    \textbf{Hits} & & &\\
    \hline
    hits per GEM $\geq$ 2 & 0.68 $\pm$ 0.01 & 0.58 $\pm$ 0.01 & 0.85 \\
    hits per GEM $\leq$ 5 & 0.68 $\pm$ 0.01 & 0.55 $\pm$ 0.01 & 0.80 \\
    \hline
    \textbf{tracking} & & &\\
    \hline
    Vertex distance $\leq$ 3 \si{mm} & 0.63 $\pm$ 0.01 & 0.49 $\pm$ 0.01 & 0.77  \\
    Vertex in decay volume & 0.62 $\pm$ 0.01 & 0.48 $\pm$ 0.01 & \textbf{0.77}\\
    \hline
    
  \end{tabular}
  \caption{efficiency of cuts based on tracking criteria for a clean sample of simulated $\mudecay$ and dimuon selected from the data for 2018. Hits cuts report efficiencies on cuts based exclusively on information coming from the single GEM modules. Tracking cuts are reporting efficiency after the tracking procedure is applied.}
  \label{tab:dimuon:efficiencies}
\end{table}

\label{sec:methods:comparison}




\section{Data analysis and selection criteria for dark matter searches}

\subsection{Background}

Background for tracking based approach could arise from particles punching through the WCAL and leaving a signature in the trackers downstream. Such contribution could arise either from large energy $\gamma$ punching through the WCAL and converting in the last few layers or by hadrons interacting in the dump.

In the case of hadrons, inelastic scattering in the WCAL produces a large shower of particles that can potentially create vertices in an event. Such events are expected to be suppressed by the many selection-criteria applied downstream for hadron rejection. Typically these are no energy in the HCAL/VETO and compatible shower profile in the ECAL. Furthermore, events able to mimic the pure electromagnetic signal of the decay $X\rightarrow e^+ e^-$ are often accompanied by a large transversal spread and are thus rejected by the requirement of energy conservation between WCAL and ECAL at a level of $< 10^{-5}$. This estimate was obtained by counting the number of events in the signal region with enough hits in the GEM modules to start the tracking-procedure without applying any rejection criteria for hadron in a MC simulation of 5$\times 10^{6}$ pions. Such an event should also pass the independent selection criteria applied upstream, namely large energy deposited in the SRD and in the WCAL pre-shower. In a sample up to $10^7$ EOTS, it was not possible to find an event with such signature even after excluding all selection criteria downstream. This is expected to reduce the background well below the level expected from the EOTS accumulated during 2018 for $\pi^-$ inelastic scattering.

To estimate the background for a larger number of EOTS, $K^0_S$ was used as benchmark process, as its pure-electromagnetic decay channels and short decay length are expected to be compatible with the one of the signal. A dedicated simulation was performed to estimate the contribution of such interaction for the statistic accumulated in 2018. The energy spectrum of $K^0_S$ was approximated using an exponential distribution fitted using a dedicated MC with an energy cut-off of 18 GeV, as $K^0_S$ with lower momentum would not be able to leave enough energy in the ECAL to enter in the signal region. By applying tracking-criteria over this sample it was estimated that a rejection of $10^{-2}$ can be conservatively achieved for this background, mainly through a cut on the reconstructed angle with some small correction due to the lower acceptance of the setup. This estimate however mostly depends on the main hadronic decay channel $K^0_S \rightarrow \pi^- + \pi^+$ which is further suppressed downstream by the many selection criteria used against hadrons mentioned above. The pure electromagnetic channel on the other hand, $K^0_S \rightarrow \pi^0 + \pi^0$ have a small chance to leave any signature in the GEM modules as no charged particle is typically emitted. Signal-like events can be produced either by pair conversion of the photon pair from a $\pi^0$ or in the decay chain $K^0_S \rightarrow \pi^0 + \pi^0, \pi^0 \rightarrow \gamma + e^- + e^+$, which is however further suppressed by the low branching ratio $\Gamma_i$/$\Gamma \approx $1\% \cite{pdg:meson}. A dedicated simulation performed with biased branching ratio showed that the rejection is further improved to $< 10^{-3}$ due to the larger emission angle of a three-body decay. Hence, a conservative rejection of $\sim 10^{-5}$ is reached due to both suppression. As no neutral events were found using the standard criteria of $E_{S4} < 0.5 MIP$ in 2018 data, it was estimated a number of background event of 0.006 for the counter analysis \ref{sec:mikhailanalysis}. By adding the additional suppression coming from the angle using the trackers, one can conservatively estimate the background contribution from K$_S$ to be at a level of $<0.001$.

For the case of electrons, the background is expected from high energetic $\gamma$ converting in the last few layers of the WCAL, suppressed by the large $X_0$ of the WCAL. No such background was observed in simulation up to $10^7$ EOTS. To estimate such contribution for a larger order of EOTS a data-driven method is used. A sample of 2$\cdot 10^7$ events was considered. This is roughly corresponding to $3 \cdot 10^{9}$ EOT ($\sim$10\% of 2018 data) due to the trigger reduction of the physical trigger. Events in the signal region with $E_{ECAL} < 105$ \si{GeV} were considered after the requirement of at least two hits in the each GEM module, only one event with such property was found. Assuming a suppression due to the angle and minimal vertex requirement of $10^{-3}$ this would put our background conservatively at a level $< 10^{-2}$. The analysis of the full 2018 data is compatible with this estimate: a total of three events were found with 2 two hits in the GEM modules. For none of these events it was possible to reconstruct a physical vertex.

Table \ref{tab:dm:background} summarizes the source of background expected for the combined counter+tracking analysis. The conclusion is that the background should be under control for the full dataset accumulated during 2018 visible mode.


\begin{table}
  \centering
  \begin{tabular}[]{|l|r|}
    \hline
    background source & estimated background \\
    \hline
    $\gamma$ punchtrough from em-shower &  $<$0.01 \\
    $\pi^-$ punchtrough & $<$0.001 \\
    $K^0_S \rightarrow \pi^- + \pi^+$  &  $<$0.001 \\
    $K^0_S \rightarrow \pi^0 + \pi^0$,$\pi^0 \rightarrow \gamma + e^- + e^+$   & $<$0.001\\
    \hline
  \end{tabular}
  \caption{background sources for NA64 visible mode tracking analysis}
  \label{tab:dm:background}
\end{table}

\subsection{Analysis of 2018 collected data}

One of the main limitations of previous analysis in the context of NA64 visible mode setup was a strict cut on the Veto behind the WCAL strongly reducing the efficiency for low energy $X$ produced during the late stages of the em-shower. Although one would expect from a naive approach based on William-Weizsacker cross section most of $X$ to be produced at high energy at the beginning of the shower, a large fraction of X bosons with energies between 30 and 50 GeV  is expected because of the high multiplicity of the shower at later stages. These $X$ are rejected in an analysis based exclusively on the requirement of low energy deposited in the Veto at the end of the dump. There are two main reasons for this: short-lived particle have a high chance of decaying before the end of the dump if the decay length is not boosted significantly, hence they have a higher chance of being rejected by the Veto. Secondly, when $X$ is produced at the late stages of the shower, the chance for the remaining particles in the dump to leave a large signal in the Veto is high. On the other hand, if $X$ is produced at the beginning of the WCAL most of the energy of the shower is lost and the remaining e$^-$ does not have enough momentum to reach the last layer of the WCAL and thus the event passes the selection.  Fig.\ref{fig:dm:wveto} shows the response of the Veto employed in 2018 visible mode to an em-shower initiated by a 150 \si{GeV} $e^-$ compared to the cut used as selection criteria for this detector. One can see that a large quantity of $X$ is rejected by the current analysis as their late production in the shower is causing large energy deposited in the Veto exceeding the currently used selection criteria. 

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=\textwidth,height=0.65\textwidth]{plots/wveto.png}    
  \end{center}
  \caption{Energy deposited in the 6 \si{mm} thick Veto placed behind the WCAL in 2018 NA64 visible mode setup. In red the distribution for standard em-shower produced by 150 \si{GeV} electron is drawn. In black the same distribution is shown for events where $X$ is produced in the shower. The cut applied to this distribution in 2018 visible mode analysis is shown by blue arrow. }
  \label{fig:dm:wveto}
\end{figure}

The disadvantage of this analysis is overcome using the tracker instead: the presence of a vertex with a small angle in the decay volume substitute the criteria of no energy in the WCAL and $X$ is able to pass the selection criteria even if its energy is low. On the other hand, the analysis has shortcomings for dark matter produced with high boost: very high energetic dark matter will have a very small aperture angle that is often not resolvable by the GEM modules, which as stated in Sec.\ref{sec:methods:simulation} was estimated to be 1.75 \si{mm}.

\begin{figure}[h!]
  \begin{center}
    % \includegraphics[scale=0.8]{plots/tvsc.pdf}
    \includegraphics[scale=0.6]{plots/prova_wecal.png} 
  \end{center}
  \caption{Simulated events with $X$production inside the WCAL in 2018 visible mode setup. Events passing selection criteria based on tracking are plotted in red, while events surviving selection criteria based on Veto and S4 cut are plotted with blue square. Estimated optimal threshold to switch between selection criteria is shown with a brown line.}
  \label{fig:dm:tvsc}
\end{figure}

Fig. \ref{fig:dm:tvsc} summarizes this claim by comparing a sample of simulated $X$ using these two different selection criteria. The plot shows that two population of $X$ exist in a beam dump experiment and they can be distinguished using the energy deposited in the ECAL downstream the dump. Due to their different kinematics, it is convenient to treat the two population separately and apply different final selection criteria. An analysis that combines the two approaches is structured as follows: first standard cuts are used to purify the sample from a large number of hadrons selected by the physical trigger. These cuts are the same employed in previous searches as they are independent of the exact signal scenario. After that, a signal region is defined by asking energy in the WCAL smaller than 105 \si{GeV}, and total energy deposited between WCAL and ECAL to be equal to the nominal beam energy with an uncertainty estimated at 10\%. Finally, one looks at the energy recorded in ECAL to decide the best selection criteria to define if the event is compatible with the production of dark matter. If $E_{ECAL} \geq E_{thr}$ one uses no energy deposited in the Veto and a signal larger than 0.8 MIP in S4 as the final requirement, else the final selection criteria will be the presence of a vertex in the decay volume with a reconstructed angle smaller than 3 \si{mrad}. The optimal value of $E_{thr}$ to maximize the signal is in general dependent on the exact dark matter scenario taken into account. It was found that a value $E_{thr} = 75$ \si{GeV} is optimal for various interesting models in the current range of 2018 visible mode. More in general this threshold achieve similar result to analysis based exclusively on the counter also for $X$ with smaller $\epsilon$, as long as the expected decay length of $X$ does not exceed the $\sim$6\si{m} extension of the dump. This range is mainly dominated by the low cross section of $X$ and hence by the statistics accumulated. Table \ref{tab:dm:efftable} compares the expected number of $X$ for 10$^{10}$ EOTS using the counter-based approach compared to the new one for different $X$ couplings. Although for some scenario the boost can be significant, for $X$ with very small decay time the weight of such low energetic $X$ is so small that the two approaches ultimately reach very close results.  

\begin{table}
  \centering
  \begin{tabular}[]{|c|c|c|c|c|}
    \hline
    M$_X$ [GeV]& $\epsilon$ & N$_X$ (counter analysis) & N$_X$ (counter+tracking analysis) \\    
    \hline
    0.0167 & 0.000316 & 1.56173 & 1.84574 \\
    0.0167 & 0.000316 & 1.66155 & 1.78013 \\
    0.0167 & 0.0006 & 1.25665 & 1.2707 \\
    0.0167 & 0.0001 & 0.232149 & 0.275633 \\
    0.0167 & 0.00018 & 0.908161 & 0.935062 \\
    0.0167 & 0.0007 & 0.783965 & 0.779922 \\
    0.01 & 0.003 & 0.000112911 & 0.00011291\\
    0.01 & 0.0015 & 2.08833 & 2.0455 \\
    0.005 & 0.004 & 2.11567 & 2.11111 \\
    0.0167 & 0.00018 & 0.858371 & 1.01707 \\
    0.022 & 0.000316 & 0.652377 & 0.788452 \\    
    \hline
  \end{tabular}
  \caption{$N_{X}$ expected for 10$^{10}$ EOT for different signal scenarios. Different types of analysis are compared: the counter analysis uses only the veto behind Tungsten ECAL for the final discrimination. Counter + tracking uses instead cuts based on GEMs as described in Sec.\ref{sec:methods:comparison} depending on the energy detected by the downstream ECAL using $E_{thr} = 75$ \si{GeV} as threshold.}
  \label{tab:dm:efftable}
\end{table}

\clearpage
\newpage

The analysis defined on the selection criteria described above was tested on the full data sample collected during 2018, amounting to $n_{eots} \approx 3 \times 10^{10}$. No events were found passing all the selection criteria chosen for the analysis. Three events were observed with energy very close to the minimal allowed by the cut $E_{WCAL} < 105$ GeV (energy between 25 GeV and 40 GeV approximately) compatible with the presence of two particles in the decay volume. However, no reliable vertex could be reconstructed from such events, leading to the conclusion that those events were caused by a small leak from the WCAL with no physical vertex. This result is compatible with the one obtained by the counter analysis, and shows the complementarity of the two approaches.

As final checks, the same criteria were applied over the whole sample to check the reliability of the tracking selection algorithm. As seen in Fig.\ref{fig:dm:wehcal_vertex}, the tracking procedure alone is able to reliably select $\mudecay$ events without the need of any additional cut based on calorimeters. The additional events in the sample are caused by inelastic scattering of hadrons in the dump which are predicted to a good degree by the MC simulation, strong suppression of such events can be achieved using SRD cuts as shown in the right plot.


\begin{figure}[h!]
  \begin{center}
    % \includegraphics[scale=0.8]{plots/tvsc.pdf}
    \includegraphics[scale=0.4]{plots/wehcal_vertex.pdf}
    \includegraphics[scale=0.44]{plots/wehcal_vertex_epure2.pdf}
  \end{center}
  \caption{WCAL vs ECAL+HCAL plot for a sample of 2$\cdot 10^7$ events acquired with physical trigger after a cut requiring at least one valid vertex in the decay volume is applied. Left: events have no additional selection criteria, on the right only events which passed SRD and WCAL pre-shower cuts are considered.}
  \label{fig:dm:wehcal_vertex}
\end{figure}


\begin{thebibliography}{99}
\bibitem{pdg:meson}
  http://pdg.lbl.gov/
\bibitem{genfit}
  https://github.com/GenFit/GenFit
\bibitem{feng1} J. Feng 
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo, 
Phys. Rev. Lett. 117, 071803 (2016) 
\bibitem{feng2}
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo,
 Phys. Rev. D 95, 035017 (2017)
\bibitem{DarkPhotons}
S.N. Gninenko, N.V. Krasnikov, M.M. Kirsanov, D.V. Kirpichnikov, Missing energy signature from
invisible decays of dark photons at the CERN SPS, Phys. Rev. D 94, 095025 (2016), arXiV:1604.08432 [hep-ph]
\bibitem{coral}
  P.~Abbon {\it et al.} [COMPASS Collaboration],
  %``The COMPASS experiment at CERN,''
  Nucl.\ Instrum.\ Meth.\ A {\bf 577} (2007) 455
  doi:10.1016/j.nima.2007.03.026
  [hep-ex/0703049].
  %%CITATION = doi:10.1016/j.nima.2007.03.026;%%
  %524 citations counted in INSPIRE as of 28 May 2019


\end{thebibliography}

\end{document}  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
