{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_160_nokill.root");
  TFile myfile1("hist_160_nokill_muid.root");
  TFile myfile2("hist_sign_160.root");
  TFile myfile3("hist_sign_160_muid.root");

  TH1D* hB = (TH1D*)myfile.Get("115");
  TH1D* hB1 = (TH1D*)myfile1.Get("115");
  TH1D* hS = (TH1D*)myfile2.Get("116");
  TH1D* hS1 = (TH1D*)myfile3.Get("116");

  c1 = new TCanvas("trigger160"," ",200,10,900,600);
  pad1 = new TPad("pad1","This is pad1",0.,0.5,0.5,1.,0);
  pad1->SetFillColor(0);
  pad1->SetBorderMode(0);
  pad1->SetBorderSize(0);
  pad1->Draw();

  pad2 = new TPad("pad1","This is pad1",0.5,0.5,1.,1.,0);
  pad2->SetFillColor(0);
  pad2->SetBorderMode(0);
  pad2->SetBorderSize(0);
  pad2->Draw();

  pad3 = new TPad("pad1","This is pad1",0.,0.,0.5,0.5,0);
  pad3->SetFillColor(0);
  pad3->SetBorderMode(0);
  pad3->SetBorderSize(0);
  pad3->Draw();

  pad4 = new TPad("pad1","This is pad1",0.5,0.,1.,0.5,0);
  pad4->SetFillColor(0);
  pad4->SetBorderMode(0);
  pad4->SetBorderSize(0);
  pad4->Draw();

  pad1->cd();
  hB->SetStats(false);
  hB->SetTitle("");
  hB->SetXTitle("Muon X at HCAL [mm]");
  hB->SetYTitle("");
  //hC->SetAxisRange(0., 100., "X");
  hB->Draw();

  pad2->cd();
  hB1->SetStats(false);
  hB1->SetTitle("Muon ID counter in trigger");
  hB1->SetXTitle("Muon X at HCAL [mm]");
  hB1->SetYTitle("");
  hB1->Draw();

  pad3->cd();
  hS->SetStats(false);
  hS->SetTitle("");
  hS->SetXTitle("Muon X at HCAL [mm]");
  hS->SetYTitle("");
  hS->Draw();

  pad4->cd();
  hS1->SetStats(false);
  hS1->SetTitle("Muon ID counter in trigger");
  hS1->SetXTitle("Muon X at HCAL [mm]");
  hS1->SetYTitle("");
  hS1->Draw();

}
