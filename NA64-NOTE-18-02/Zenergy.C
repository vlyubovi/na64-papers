{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_sign_160_muid.root");

  TH1D* hE = (TH1D*)myfile.Get("402");

  c1 = new TCanvas("Zenergy"," ",200,10,900,600);

  hE->Rebin();
  hE->SetStats(false);
  hE->SetTitle("");
  hE->SetXTitle("Z_{#mu} energy [GeV]");
  hE->SetYTitle("");
  hE->Draw();

}
