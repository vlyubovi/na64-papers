{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  //TFile myfile("hist_160_muid_ecut.root");
  //TFile myfile("hist_160_muid_ecut_vetomagnet.root");
  TFile myfile("hist60.root");

  TH2D* hB = (TH2D*)myfile.Get("202");

  c1 = new TCanvas("BGbiplot_LEGO"," ",800,800);

  pad1 = new TPad("pad1","This is pad1",0.,0.,1.,1.,0);
  pad1->SetFillColor(0);
  pad1->SetBorderMode(0);
  pad1->SetBorderSize(0);
  pad1->SetRightMargin(0.12);
  pad1->Draw();
  pad1->cd();

  hB->Rebin2D(2,2);

  hB->SetStats(false);
  hB->SetTitle("");
  hB->SetXTitle("muon energy after ECAL [GeV]");
  hB->SetYTitle("Energy in calorimeters [GeV]");
  hB->SetTitleOffset(1.9, "X");
  hB->SetTitleOffset(1.9, "Y");
//  hB->Draw("colz");
  hB->Draw("LEGO2");

  TLine* line = new TLine(80., 0., 80., 20.);
  line->SetLineColor(kRed);
  line->SetLineWidth(2.);
  line->Draw();
  TLine* line1 = new TLine(80., 20., 0., 20.);
  line1->SetLineColor(kRed);
  line1->SetLineWidth(2.);
  line1->Draw();

//  TLine* line2 = new TLine(160., 0., 0., 160.);
//  line2->SetLineColor(kRed);
//  line2->SetLineWidth(2.);
//  line2->Draw();

}
