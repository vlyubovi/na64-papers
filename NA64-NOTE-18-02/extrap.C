{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  //TFile myfile("hist_160_muid_ecut_vetomagnet.root");
  //TFile myfile("hist_160_muid_ecut.root");
  //TFile myfile("hist_160_muid_ecut_vetomagnet1.root");
  TFile myfile("hist70.root");

  TH2D* hB = (TH2D*)myfile.Get("202");

  c1 = new TCanvas("extrap"," ",200,10,900,600);

  cout << "signal box dist = " << 60./sqrt(2.) << endl;

  TH1D* hdist = new TH1D("hdist", "dist from line", 28, 0., 112.);

  for(int ibin=1; ibin<16; ibin++) {
    double dmin = 4.*((double)(ibin-1));
    double dmax = 4.*((double)ibin);
    double xnev = 0.;
    for(int ix=1; ix<=110; ix++) {
      for(int iy=1; iy<=110; iy++) {
        double xbin = 2.*((double)ix) -1.;
        double ybin = 2.*((double)iy) -1.;
        double y0 = xbin + ybin;
        double ydist = 160. - y0;
        double distline = ydist/sqrt(2.);
        if(xbin < 140. && xbin > ybin && distline > dmin && distline < dmax) xnev += hB->GetBinContent(ix, iy);
      }
    }
    hdist->SetBinContent(ibin, xnev);
    cout << "bin " << ibin << " nev = " << xnev << endl;
  }
  hdist->SetStats(false);
  //hdist->SetTitle("");
  hdist->SetXTitle("Distance from line [GeV]");
  hdist->SetYTitle("N events");
  hdist->SetMinimum(0.000001);
  hdist->Draw("E");

  TLine* line = new TLine(42.42,0.000001, 42.42, 70.);
  line->SetLineColor(kRed);
  line->SetLineWidth(2.);
  line->Draw();
}
