\documentclass[pdftex,12pt,a4paper]{article}
%\usepackage[affil-it]{authblk}
\usepackage[margin=0.7in]{geometry}
\usepackage{multicol}
\usepackage{enumerate}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd}
\usepackage[pdftex]{graphicx}
\usepackage[font={small,it}]{caption}
\usepackage[margin=8pt]{subfig}
%\usepackage{multirow}
\usepackage{longtable}
\usepackage{float}
%\usepackage{hyperref}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand\g{\gamma}
\newcommand\ee{e^+e^-}
\newcommand\aee{X\to e^+e^-}
\newcommand\ainv{X\to invisible}
\newcommand\xdecay{X \rightarrow e^+  e^-}
\newcommand\pair{e^+ e^-}
\usepackage{changepage}

\begin{document} 

\title{\vspace{-1.5cm}{
\begin{center}
%\resizebox{7cm}
\includegraphics[scale=0.47]{na64-logo-pc.pdf}
\end{center}
\vskip1.cm
%%%%%%%%%%%%%%%%%%  Put here the correct number for your Note %%%%%%%%%%%%%%%%%%%
\begin{flushright}
\normalsize{\bf
NA64-NOTE-19-09~~~ \\
\today}
%March 8 2017}
\end{flushright}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Put here the Title, Author list and the Abstract 
\vskip3.5cm
\Large{\bf Sensitivity studies of visible mode with varying length of the calorimeter}}
%\author{NA64 Collaboration}
\author{E. Depero}
\date{\vspace{-5ex}}
\maketitle
\abstract{In this note we study possible new design if the Tungsten calorimeter (WCAL) in the visible mode of NA64. Small sample of signal events are simulated and the signal yield is calculated for each one of them assuming zero background. The study aims to probe a new setup able to cover the so called Beryllium anomaly, to this aim the Dark matter is simulated to have a mass of 16.7 MeV and a coupling $\epsilon sim$10$^{-3}$.}

\newpage

  
 
\section{Methodology}

The simulation is performed using the setup visible mode in 2018. The face of the WCAL is left in its original position and the only thing that is changed is the thickness of the convert and counter layer as well as its number. Most of the design tested has an equivalent radiation length to the one used in 2018 visible mode, meaning that the background due to punch-through should be equivalent to the one estimated for visible mode 2018. A small difference in the background to be estimated could be caused by long-lived neutral particles able to penetrate through the WCAL and the Veto installed immediately after. The Veto was taken with a thickness of 6 mm.

Since most of the design has a different converter thickness, a new calibration was needed for all tests. This is expected to worsen the energy resolution by a factor of at least $\sqrt{2}$, which is taken into account in the simulation automatically. On top of this, the energy resolution due to the electronic is taken into account in the study estimated from the data of 2018.

The number of EOTS required to cover a specific dark matter hypothesis is calculated from the weight of the signal detected in the setup. The weight is defined as the integral of the decaying exponential integrated into the region of the acceptance, which can be viewed as the probability for a specific X-boson produced at certain energy to be detected in the NA64 setup. As the X-boson has an extremely low decay time in the order of ns when produced at rest, the boost of the decay length due to its high energy is necessary to give the boson a reasonable chance to be detected after its production. This means that only produced in the first few layers of the WCAL are relevant due to their high energy. The many X-boson produced in the late stages of the shower on the other hand although large decay immediately in the WCAL causing no observable difference in the shower signature.

To calculate the necessary number of EOTS to cover the anomaly the following formula is used:

\[
  N_{EOT} = \frac{2.3}{\sum \omega_i(m_{X}, \epsilon_{Xmax}) \times N_{norm}(m_{X}, \epsilon_{Xmax}, bias)}
\]

where:

\begin{itemize}
\item $m_{X}$ is the mass of the X-boson, set to be 16.7 MeV for this study.
\item $\epsilon_{Xmax}$ is the maximum $\epsilon$ that would justify the Beryllium anomaly hypothesis \cite{feng1}, this is set to be $10^{-3}$
\item $\sum \omega_i$ is the sum over all the weight of the X-boson that passed all selection criteria
\item $N_{norm}$ a normalization constant computed to correct for the bias in the cross-section given in the simulation, it is a function of the X-boson parameter and the bias chosen in the simulation
\end{itemize}

The error for this number is calculated assuming Poisson error for the weight of the X-bosons.

Regarding the selection criteria, all the cuts used in the visible mode analysis of 2018 were faithfully replicated for this study, to have a consistent projection of the sensitivity. For more information about these cuts, one can see \cite{na64-paper:19-02}.
\section{Results}

Two different designs were explored for the tungsten calorimeter. The first design arranges the tungsten layer in pairs of two instead of one, which would make the single WCAL layer consist of 6 mm of tungsten and 3 mm of Scintillating material. The advantage of this design is to be 3 cm shorter without changing the total radiation length that the calorimeter had in 2018 (~40$X_0$). However, the larger converter is expected to worsen the energy resolution of the detector by a factor $\sqrt{2}$ as mentioned before. The second design, on the other hand, use a calorimeter in two different part, the first one with the same structure used in 2018, i.e. 3 mm of Tungsten and 2 mm of scintillator for a total of 25 layer, while a second part consisting of only Tungsten layer is used to shield for neutral punch-trough for a total of 9 layers consisting of 3 mm Tungsten each. This solution is expected to have a good resolution for the beam on average as the ratio between converter and scintillator remains optimal. However, it is slightly longer, which can make an important difference in particular short-lived X. Additionally, a tail is expected in the energy distribution of the WCAL due to shower fluctuations.

The results of the simulation are summarized in Table.\ref{tab:results}. The third entry of the table shows the results for the setup used in 2018 for comparison. The most promising results are given by the first design tested, where the layer is selected to be 6mm of tungsten and only half of the scintillator is used. In this setup the beryllium anomaly hypothesis\cite{feng1} could be covered in 10 days assuming a similar rate recorded in 2018. The energy decrease of energy resolution is expected to have a very small impact on the X-boson efficiency. This is explained by the fact that most of the dark matter contributing to the signal is produced in the very first layers of the WCAL and bring most of the energy of the beam outside of the WCAL, hence most of the energy will be recorded on the downstream ECAL. More information about this is given in the next session.

\begin{center}
  % \begin{adjustwidth}{-2cm}{}
\begin{table}
\begin{tabular}{lrrrrr}
WCAL structure [mm](layers) & WCAL length  [mm] & epsilon & EOTS [$10^{10}$] & Day needed\\
\hline
ECAL1:6+2(17)ECAL2:-- & 142 & 0.0012  & 37(37) & 44(45)\\
ECAL1:6+2(17)ECAL2:-- & 142 & 0.001  & 8(2) & 10(3)\\
ECAL1:3+2(34)ECAL2:-- & 173 & 0.001  & 22(8) & 26(10)\\
ECAL1:3+2(25)ECAL2:3+0(9) & 153 & 0.001  & 15(3) & 18(3)\\
ECAL1:3+2(25)ECAL2:3+0(9) & 153 & 0.001 & 16(4) & 19(5)\\
\hline
\end{tabular}
\label{tab:results}
\caption{Number of EOTS required to cover an X-boson with mass of 16.7 MeV using different design of the WCAL in visible mode setup of 2018. The first entry describe the structure using the convention:
[ECAL]:[converter-dept]+[counter-depth](number-of-layers).}
\end{table}
%\end{adjustwidth}
\end{center}
  
\section{Conclusion}

\vskip1.5cm
{\large \bf Acknowledgements}\\

We should like to thank X,Y,Z for useful comments and discussions.


\begin{thebibliography}{99}
\bibitem{feng1} J. Feng 
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo, 
Phys. Rev. Lett. 117, 071803 (2016) 
\bibitem{feng2}
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo,
 Phys. Rev. D 95, 035017 (2017)
\bibitem{DarkPhotons}
S.N. Gninenko, N.V. Krasnikov, M.M. Kirsanov, D.V. Kirpichnikov, Missing energy signature from
invisible decays of dark photons at the CERN SPS, Phys. Rev. D 94, 095025 (2016), arXiV:1604.08432 [hep-ph]


\end{thebibliography}

\end{document}  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
