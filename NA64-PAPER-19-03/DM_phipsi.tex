%%%%%%%%%%%%%%%%%%%%%%% file template.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is a template file for Web of Conferences Journal
%
% Copy it to a new file with a new name and use it as the basis
% for your article
%
%%%%%%%%%%%%%%%%%%%%%%%%%% EDP Science %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%\documentclass[option]{webofc}
%%% "twocolumn" for typesetting an article in two columns format (default one column)
%
\documentclass{webofc}
\usepackage[varg]{txfonts}   % Web of Conferences font
%
% Put here some packages required or/and some personnal commands
%
\newcommand\ma{m_{A'}}
\newcommand\pair{e^+e^-}
\newcommand\ee{e^+e^-}
\newcommand\xee{X \to e^+e^-}
\newcommand\aee{A' \to e^+e^-}
\newcommand\xdecay{X \rightarrow e^+  e^-}
\newcommand\ainv{A'\to invisible}
\newcommand\kospio{K^0_S \to \pi^+ \pi^-}

\begin{document}
%
\title{Recent results of the NA64 experiment at the CERN SPS}
%
% subtitle is optionnal
%
%%%\subtitle{Do you have a subtitle?\\ If so, write it here}

\author{\firstname{Mikhail} \lastname{Kirsanov}\inst{1}\fnsep\thanks{\email{Mikhail.Kirsanov@cern.ch}}
}

\institute{INR RAS Moscow}

\abstract{
  We report on the results of the search for a new sub-GeV vector boson ($A'$) mediated production of Dark Matter ($\chi$) in the fixed-target
experiment, NA64, at the CERN SPS.  The $A'$, called dark photon, could be  generated  in  the    reaction $ e^- Z \to e^- Z A'$
of 100 GeV electrons dumped against an active target which is followed  by the prompt invisible decay $A' \to \chi \overline{\chi}$.   
The experimental signature of this process  would be an event with an isolated electron and large missing energy in the detector.    
From the analysis of the data sample collected in 2016 corresponding to $4.3\times10^{10}$ electrons on target no evidence of such a process  
has been found. The constraints on the $A'$ mixing strength with photons, $10^{-5}\lesssim \epsilon \lesssim 10^{-2}$, for the $A'$ mass range
$\ma \lesssim 1$ GeV are derived. Significantly more data were taken in 2017 and 2018. The analysis of these data is not yet finished, although
the sensitivity is already estimated as about 3 times better than in the previously published result on the 2016 data. 

  For models considering scalar and fermionic thermal Dark Matter interacting with the visible sector  
through the vector portal the experiment starts to be sensitive to the regions of the dark-matter parameter $y = \epsilon^2 \alpha_D 
(\frac{m_\chi}{m_{A'}})^4$ predicted from the cosmological observations of Dark Matter.

  We also report the improved results on a direct search for a new 16.7 MeV boson ($X$) which could explain the anomalous excess of $\ee$ pairs
observed in the excited $^8$Be$^*$ nucleus decays. Due to its coupling to electrons, the X could be produced in the bremsstrahlung
reaction $e^- Z \to e^- Z  X$ by a beam of electrons incident on an active target in the NA64 experiment at the CERN SPS and
observed through the subsequent decay into a $\ee$ pair.
With $8.4\times 10^{10}$ electrons on target no evidence for such decays was found, allowing to set limits on the $X-e^-$
coupling in the range $1 \times 10^{-4}\lesssim \epsilon_e \lesssim 6\times 10^{-4}$ excluding part of the allowed
parameter space. We also set new bounds on the mixing strength of photons with dark photons ($A'$) from non-observation of
the decay $\aee$ of the bremsstrahlung $A'$ with a mass $\lesssim 23$ MeV.

  Future plans of the NA64 experiment are presented.
}
%
\maketitle
%


\section{Introduction}
\label{intro}


 The NA64 experiment is primarily designed for the search of dark photons $A'$ \cite{Okun:1982xi,Galison:1983pa,Holdom:1985ag} through the
missing energy signature. If the $A'$ exists it could be produced via the kinetic mixing with bremsstrahlung photons in the reaction
of high-energy electrons absorbed in an active beam dump (target) followed by the prompt $\ainv$ decay into DM particles in a hermetic detector
\cite{Gninenko:2013rka,Andreas:2013lya}. The first results of these search were published in \cite{NA64invis}. They were based on the
statistics corresponding to $4.3\times10^{10}$ electrons on target (EOT). In the runs of 2017 and 2018 a significantly more data
were collected. These data correspond in total to about $3\times10^{11}$ EOT. With this statistics the NA64 experiment starts to be
sensitive to Dark Matter mixing parameters predicted by some thermal Dark Matter (DM) models \cite{NA64models}.


\section{Sensitivity of NA64 to A' in the invisible mode search}


 The NA64 experiment employs the optimized 100 GeV electron beam from the H4 beam line at the North Area (NA) of the CERN SPS described in details in
Ref.\cite{beam}. The H4 provides an essentially pure $e^-$ beam for fixed-target experiments. The beam is designed to transport the electrons with
the maximal intensity up to $\simeq 10^7$ per SPS spill of 4.8 s in the momentum range between 50 and 150 GeV/c produced by the primary
proton beam of 400 GeV/c on a beryllium target. The main contribution to the $e^-$ yield from
the target is the production of $\pi^0$ followed by a process $\pi^0 \to \gamma\gamma \to \ee$. The short-lived $\pi^0$ decay inside the target, and
the electrons are produced through the conversion of the decay photons in a separate converter \cite{h4beam}.

 The  NA64 detector, which is located at about 500 m from the proton target, is schematically shown in Fig.~\ref{setup}.
The setup utilises the beam defining scintillator (Sc) counters S1-S3 and veto V1, and the spectrometer consisting of two successive dipole
magnets with the integral  magnetic field of $\simeq$7 T$\cdot$m  and a low-material-budget tracker.
The tracker is a set of two upstream Micromegas chambers (MM1,2) and two downstream MM3,4 and GEM1,2 stations, allowing the measurements
of $e^-$ momenta $P_e$ with the precision $\delta P_e/P_e \simeq 1\%$ \cite{Banerjee:2015eno}. The magnets also serve as an effective filter
rejecting the low energy electrons present in  the beam.
To improve the high energy electrons selection and suppress background from a possible admixture of low energy electrons, a tagging system 
utilizing the synchrotron radiation (SR) from high energy electrons in the magnetic field is used, as schematically shown in Fig.~\ref{setup}. 
The basic idea is that, since the amount SR  energy per revolution emitted by a particle with the mass $m$ and energy $E_0$ is
$<E_{SR}> \propto E_0^3/m^4$, the low energy electrons and hadrons in the beam could be effectively rejected by using the cut on the energy 
deposited in the SR detector (SRD) \cite{Gninenko:2013rka,na64srd}.
A 15 m long vacuum vessel is installed between the magnets and the ECAL to minimize absorption of the SR photons detected immediately 
at the downstream end of the vessel by a SRD, which is an array of PbSc sandwich calorimeter with a fine sampling.
The SRD is also segmented  transversely in three SRD counters, each of $60\times 80 $mm$^2$
in lateral size assembled from $80-100~\mu$m Pb and 1 mm Sc plates with wave length shifting (WLS) fiber read-out. This allowed to additionally
suppress misidentified hadrons that could knock electrons off the output vacuum window of the vessel producing a fake $e^-$ SRD tag,
by about two  orders of magnitude \cite{na64srd}.
One of the main components of the experiment is the active target, a hodoscopic electromagnetic calorimeter (ECAL) for the measurement of the
electron energy deposition, $E_{ECAL}$, with the accuracy $\delta E_{ECAL}/E_{ECAL} \simeq 0.1/\sqrt{E_{ECAL}[{\rm GeV}]}$ as well as the $X,~ Y$
coordinates of the incoming electrons by using the transverse electromagnetic (e-m) shower profile.
The ECAL is a matrix of $6\times 6$ Shashlik-type counters assembled from Pb and Sc plates with WLS fiber read-out. Each module is $\simeq 40$
radiation lengths ($X_0$) and has an initial part of $\simeq 4~X$ read separately. By requiring the presence of the in-time SR signals in all
three SRD counters and using information on the longitudinal and lateral shower development in the ECAL, the initial level of the hadron
contamination in the beam $\pi/e^- \lesssim 10^{-2}$ is suppressed by more than 4 orders of magnitudes, while keeping the electron ID efficiency
at the level of $\gtrsim 95\%$ \cite{na64srd}. A high-efficiency veto counter VETO and a massive hadronic calorimeter (HCAL) of $\simeq 30$
nuclear interaction lengths ($\lambda_{int}$) are positioned just after the ECAL. The VETO is a plane of scintillation counters used to veto
charged secondaries incident on the HCAL detector from upstream $e^-$ interactions.

\begin{figure*}[tbh!!]
\includegraphics[width=1.\textwidth,height=.45\textwidth]{NA64_top_2016.png}
\vskip-0.cm{\caption{Schematic illustration of the setup to search for $\ainv$ decays of the bremsstrahlung $A'$s
produced in the reaction  $eZ\rightarrow eZ A'$ of 100 GeV e$^-$ incident on the active ECAL target.
\label{setup}}}
\end{figure*}

 In order to decrease the number of events recorded by DAQ the trigger including the requirement $E_{ECAL} < E_{threshold}$ is used in the main
part of the runs ("physical runs", unlike "calibration runs"). The threshold is usually chosen to be about 80 GeV for the 100 GeV beam.

 For the cuts selection, calculation of various efficiencies and background estimation the package for the detailed full simulation of the
experiment based on Geant4 \cite{Geant4-2002, Geant4-2006} is developed. It contains the subpackage for the simulation of various types of
Dark Matter particles based on the exact tree-level calculation of cross sections \cite{DMsimulation}.

 The signature of signal events is the incoming electron with measured momentum close to 100 GeV, identified by requiring energy above the cut
in the SRD detectors and the shower profile compatible with electron in the ECAL that deposits less than 50 GeV in the ECAL. Energy deposition
compatible with noise is required in the HCAL and veto counters. The cut 50 GeV was chosen taking into account the expected energy distributions
from signal events and backgrounds. The two-dimensional distribution of events in the physical run is shown in Fig.~\ref{invis_two_dim}.

\begin{figure*}[tbh!!]
\includegraphics[width=0.6\textwidth,height=.45\textwidth]{HCvsEC-2441-2457-upper-nostats.pdf}
\vskip-0.cm{\caption{Two-dimensional distribution of the events in the physical run after the tracker and veto cuts.
\label{invis_two_dim}}}
\end{figure*}

 While some backgrounds were estimated with MC, such as various decays in flight, and turned out to be very small, there are background that are
more difficult to estimate. They could come from the upstream interactions on the beam elements and tracker, small non-hermeticity of the detector
for rare events with extremely big transverse momentum, and can be complicated by the pile-up effects. For this reason another method of background
estimation was used. We performed the extrapolation from the side bins as it is illustrated in Fig.~\ref{invis_extrap}.

\begin{figure*}[tbh!!]
\includegraphics[width=0.6\textwidth,height=.45\textwidth]{r2366-2436-4.png}
\vskip-0.cm{\caption{Invisible mode background extrapolation method.
\label{invis_extrap}}}
\end{figure*}

 The analysis of all the data collected in 2016 - 2018 at this moment is not yet finished, although the sensitivity to DarkPhotons is already
calculated. It is shown in Fig.~\ref{invis_sens}.

%\begin{figure*}[tbh!!]
%\includegraphics[width=1.\textwidth,height=.45\textwidth]{Invisnew.png}
%\vskip-0.cm{\caption{The sensitivity of NA64 to DarkPhotons with the full statistics collected in 2016 - 2018. 
%\label{invis_sens}}}
%\end{figure*}

%\begin{figure*}[tbh!!]
%\includegraphics[width=1.\textwidth,height=.45\textwidth]{Invis-y.png}
%\vskip-0.cm{\caption{The sensitivity of NA64 to DarkPhotons in terms of the variable $y$, shown together with the predictions of
%some popular thermal DarkMatter models.
%\label{invis_sens_y}}}
%\end{figure*}

\begin{figure*}[tbh!!]
\includegraphics[width=0.5\textwidth]{Invisnew.png}
\hspace{-0.cm}{\includegraphics[width=0.5\textwidth]{Invis-y.png}}
\caption{The sensitivity of NA64 to DarkPhotons with the full statistics collected in 2016 - 2018.
Left plot: in terms of the mixing strength $\epsilon$. Right plot: in terms of the variable $y$, assuming $\alpha_D=0.1$ and
$m_{A'}=3m_{\chi}$, shown together with the predictions of some popular thermal Dark Matter models.}
\label{invis_sens}
\end{figure*}


\section{Search for a new $X(16.7)$ boson and dark photons decaying to $e^+e^-$.}


 The ATOMKI experiment of Krasznahorkay et al. \cite{be8} has reported the observation of a 6.8$\sigma$ excess of events
in the invariant mass distributions of $\pair$ pairs produced in the nuclear transitions of excited  $^8Be^*$ to its ground state via
internal pair creation. It has been shown that the anomaly can be interpreted as the emission of a new
protophobic gauge $X$ boson with a mass of 16.7 MeV followed by its $\xdecay$ decay \cite{feng1,feng2}.
This explanation of the anomaly was found to be consistent with all existing constraints assuming that the $X$ has
a non-universal coupling to quarks and coupling to electrons in the range $2\times 10^{-4} \lesssim \epsilon_e \lesssim 1.4\times 10^{-3}$.
The search for such particles was performed by the NA64 experiment modified for the visible mode of decay to $\ee$ in 2017 \cite{NA64Be2017}.
We report here the improved results from the NA64 experiment obtained using the data collected in 2018 in the new run at the CERN SPS
performed after optimization of the experiment configuration and parameters.
 
 In order to search for visible decays the NA64 experiment uses another active target, the compact electromagnetic calorimeter WCAL
made as short as possible to maximize the sensitivity to short lifetimes while keeping the leakage of particles at a small level.
The WCAL(ECAL) is assembled from the tungsten(lead) and plastic scintillator plates with wave lengths shifting fiber read-out.
Immediately after WCAL there is a veto counter V2, the tracking detectors, the signal counter S4. They are followed by the
ECAL that was used in the invisible mode and the same detectors downstream of it (VETO and HCAL). The energy of the $\ee$ pair
is measured by the ECAL.

 The candidate events were selected with the following criteria chosen to maximize the acceptance of signal events and
to minimize the number of background events, using both MC simulation and data:
(i) No  energy deposition in the V2 counter exceeding about half of the energy deposited by the  minimum ionizing particle (MIP);
(ii) The signal in the decay counter S4 is consistent with two MIPs;
(iii) The sum of energies deposited in the WCAL+ECAL is equal to the beam energy within the energy resolution of these detectors.
 At least 25\% of the total energy should be deposited in the ECAL;
(iv) The shower in the WCAL should start to develop within a few first $X_0$, which is ensured by the preshower part energy cut;
(v) The cell with maximal energy deposition in the ECAL should be (3,3)
(vi) The lateral and longitudinal shape of the shower in the ECAL are consistent with a single e-m one. This requirement
does not decrease the efficiency to signal events because the distance between $e^-$ and $e^+$ in the ECAL is very small.
The rejection of events with hadrons in the final state was based on the VETO and/or the energy
deposited in the HCAL.

 In order to check various efficiencies and the reliability of the MC simulations, we selected a clean sample of
$\simeq 10^5$ $\mu^+ \mu^-$ events with $E_{WCAL} < 0.6E_{beam}$ originated from the QED dimuon production
in the dump. This rare process is dominated by the reaction $e^- Z \to e^- Z \gamma; \gamma \to \mu^+ \mu^-$
of a hard bremsstrahlung photon conversion into the dimuon pair on a dump nucleus.
We performed various comparisons between these events and the corresponding MC simulated sample, and applied the
estimated efficiency corrections to the MC events. These corrections do not exceed 20\%.

 In order to further increase the sensitivity to short-living X bosons (higher $\epsilon$) the following optimization steps
were performed before the 2018 run: (i) Beam energy increased to 150 GeV (ii) Thinner counter V2 was prepared and installed
immediately after the last tungsten plate inside the WCAL box. In addition, the vacuum pipe was installed immediately after
the WCAL, the distance between the WCAL and ECAL was increased.

 It was found that the main background comes from the $\kospio$ events from $K_0$ mainly produced by hadrons misidentified
as electrons. We estimated this background using both simulation and data. For this, we selected the sample of neutral
events changing the cut (ii) to $E_{S_4}<0.5MIP$. This sample has 3 events in the 2017 data and one event in the 2018 data.
The smaller number of such events in the 2018 data is expected, because due to the higher distance between WCAL and ECAL less
$K^0_S$ events pass the criteria (v) and (vi). The MC sample of $K^0_S$ was simulated according to distributions predicted
for the hadron interactions in WCAL. With this sample we calculated the number of neutral and signal-like events passing
the criteria. This gives us the prediction of the number of background events: 0.06 for 2017 data and 0.006 for the 2018
data. Other backgrounds are significantly smaller.

 After opening the signal box we found no events. We performed the combined statistical analysis of the 2017 and 2018
data. The result is shown in Fig.~\ref{new-excl-Be}.

\begin{figure*}[tbh!!]
\includegraphics[width=0.6\textwidth,height=.45\textwidth]{new-excl-Be.pdf}
\vskip-0.cm{\caption{The regions of $X \rightarrow e^+e^-$ parameters excluded by the NA64 experiment with the 2017 data
(blue shadowed region) and with 2017+2018 data (dashed line, preliminary). The red vertical line is the region of parameters
that could explain the $^8Be$ anomaly.
\label{new-excl-Be}}}
\end{figure*}

 We are now developing the analysis of the data using the track detectors. The idea is to reconstruct the two tracks and
the vertex of decay. This is possible if the total energy of the $e^+e^-$ pair is not very high, otherwise the two tracks
are difficult to resolve. We expect that this will improve the sensitivity to big $\epsilon$.

 Two more analyses devoted to the search for exotic particles are being performed now using the data collected in 2018 in the visible
mode configuration.


\section{Future plans of the NA64 experiment.}

 
 Running of the NA64 experiment on the electron beam after the SPS - LHC Long Stop 2 (LS2), i.e. in 2021, is approved by
the SPSC. The plan is to collect data in the invisible mode corresponding to $5 \times 10^{12}$ EOT. This will require the
modernization of the apparatus: better hermeticity of the detector, smaller material budget of the tracker, faster electronics.
This will allow to probe already most of the thermal DM models \cite{NA64models}.

 The collaboration is now working on the project to extend the searches by running in the SPS muon beam M2. The corresponding
project is called NA64$\mu$. With this experiment it will be possible to search for $Z_{\mu}$ mediator to Dark Matter of the
$L_{\mu} - L_{\tau}$ model \cite{NA64models}. This model can provide explanation of the (g-2) anomaly. The signature of the
$Z_{\mu}$ production event would be an incoming muon with well measured momentum that loses more than half of its energy
in the active target without significant energy deposition in it and in HCAL. This experiment will also allow to search for
A', having the advantage as compared to the electron beam of smaller cross section suppression at the masses of A' of a few
hundred MeV \cite{NA64models}. In the muon beam it is also possible to search for $\mu - \tau$ conversion \cite{NA64mutau}.


\section{Conclusion.}


 The NA64 experiment at the CERN SPS is performing searches for Dark Matter particles since 2016. No signal of such particles
are found yet. New stringent limits on the parameters of Dark Matter models are obtained. The NA64 starts to be
sensitive to predictions of some popular thermal Dark Matter models \cite{NA64models}. There are plans to continue the experiment,
significantly modernized, after the LS2 of the SPS-LHC, in 2021. After a few year of running it will be possible to
probe already most of the existing thermal Dark Matter models. The searches will be continued in the muon beam of the
CERN SPS with modified and additional detectors. The NA64 experiment has a rich physical program to search for various
other exotic partictes and new phenomena.


\begin{thebibliography}{}

\bibitem{Okun:1982xi}
L.~B.~Okun, ``Limits Of Electrodynamics: Paraphotons?,'' Sov.\ Phys.\ JETP {\bf 56}, 502 (1982)

\bibitem{Galison:1983pa}
P.~Galison and A.~Manohar, ``Two $Z'$s or not  two $Z'$s ?'', Phys. Lett. B {\bf 136}, 279 (1984).

\bibitem{Holdom:1985ag}
B.~Holdom,``Two U(1)'s and Epsilon Charge Shifts,'' Phys.\ Lett.\ B {\bf 166}, 196 (1986).

\bibitem{Gninenko:2013rka}
S.~N.~Gninenko,``Search for MeV dark photons in a light-shining-through-walls experiment at CERN,'' Phys.\ Rev.\ D {\bf 89},  075008 (2014).

\bibitem{Andreas:2013lya}
S.~Andreas {\it et al.},``Proposal for an Experiment to Search for Light Dark Matter at the SPS,'' arXiv:1312.3309 [hep-ex].

\bibitem{NA64invis}
NA64 Collaboration, ``Search for vector mediator of Dark Matter production in invisible decay mode,'' Phys. Rev. D {\bf 97}, 072002 (2018),
arXiV:1710.00971 [hep-ph]

\bibitem{NA64models}
S.N.~Gninenko, D.V.Kirpichnikov, M.M.Kirsanov and N.V.~Krasnikov,
``Combined search for light dark matter  with electron and muon beams at NA64,'' arXiv:1903.07899 [hep-ph]

\bibitem{beam}
See, for example, http://sba.web.cern.ch/sba/

\bibitem{h4beam}
H.W. Atherton, P. Coet, N. Doble, D.E. Plane, CERN/SPS 85-43 (1985).

\bibitem{Banerjee:2015eno}
D.~Banerjee, P.~Crivelli and A.~Rubbia,``Beam Purity for Light Dark Matter Search in Beam Dump Experiments,''
Adv.\ High Energy Phys.\ {\bf 2015}, 105730 (2015).

\bibitem{na64srd}
E.~Depero {\it et al.},``High purity 100 GeV electron identification with synchrotron radiation,'' Nucl.\ Instrum.\ Meth.\ A {\bf 866}, 196 (2017).

\bibitem{Geant4-2002}
S.~Agostinelli {\it et al.} [GEANT4 Collaboration], ``GEANT4: A Simulation toolkit,''
Nucl.\ Instrum.\ Meth.\ A {\bf 506}, 250  (2003).

\bibitem{Geant4-2006}
J.~Allison {\it et al.}, ``Geant4 developments and applications,''
IEEE Trans.\ Nucl.\ Sci.\  {\bf 53}, 270 (2006).

\bibitem{DMsimulation}
S.N. Gninenko, D.V. Kirpichnikov, M.M. Kirsanov and N.V. Krasnikov,
``The exact tree-level calculation of the dark photon production in high-energy electron scattering at the CERN SPS,''
Phys.Lett. B {\bf 782} (2018) 406-411, arXiV:1712.05706 [hep-ph].

\bibitem{be8}
A. Krasznahorkay et al., Phys. Rev. Lett. {\bf 116}, 042501 (2016).

\bibitem{feng1} J. Feng
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo,  Phys. Rev. Lett. {\bf 117}, 071803 (2016).

\bibitem{feng2}
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo,  Phys. Rev. D {\bf 95}, 035017 (2017).

\bibitem{NA64Be2017}
NA64 Collaboration, ``Search for a Hypothetical 16.7 MeV Gauge Boson and Dark Photons in the NA64 Experiment at CERN,'',
Phys. Rev. Lett. 120, 231802 (2018), arXiV:1803.07748 [hep-ph].

\bibitem{NA64mutau}
S. Gninenko et al., ``Deep inelastic e-tau and mu-tau conversion in the NA64 experiment at the CERN SPS,''
Phys. Rev. D 98, 015007, arXiV:1804.05550 [hep-ph].


\end{thebibliography}

\end{document}
