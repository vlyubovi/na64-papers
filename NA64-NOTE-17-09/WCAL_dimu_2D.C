{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_W_dimu_3387_3389.root");

  TH2D* hDATA = (TH2D*)myfile.Get("ehplot");

  c1 = new TCanvas("WCAL_dimu_2D"," ",200,10,900,600);
  hDATA->SetStats(false);
  hDATA->SetTitle("");
  hDATA->SetXTitle("Energy in WCAL [GeV]");
  hDATA->SetYTitle("Total energy in HCAL modules 0 - 2 [GeV]");

  hDATA->SetAxisRange(0., 100., "X");
  hDATA->Draw("colz");
}
