\documentclass[pdftex,12pt,a4paper]{article}
%\usepackage[affil-it]{authblk}
\usepackage[margin=0.7in]{geometry}
\usepackage{multicol}
\usepackage{enumerate}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd}
\usepackage[pdftex]{graphicx}
\usepackage[font={small,it}]{caption}
\usepackage[margin=8pt]{subfig}
%\usepackage{multirow}
\usepackage{longtable}
\usepackage{float}
%\usepackage{hyperref}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand\g{\gamma}
\newcommand\ee{e^+e^-}
\newcommand\aee{X\to e^+e^-}
\newcommand\ainv{X\to invisible}
\newcommand\xdecay{X \rightarrow e^+  e^-}
\newcommand\pair{e^+ e^-}

\begin{document}

\title{\vspace{-1.5cm}{
\begin{center}
%\resizebox{7cm}
\includegraphics[scale=0.47]{na64-logo-pc.pdf}
\end{center}
\vskip1.cm
\begin{flushright}
\normalsize{\bf
NA64-NOTE-17-09~~~ \\
\today}
%March 8 2017}
\end{flushright}}
\vskip3.5cm
\Large{\bf Search for dark photons decaying to electron-positron pairs $e N \rightarrow A' \rightarrow e^+ e^-$ in NA64 using the 2017 data.}}
%\author{NA64 Collaboration}
\author{M.M. Kirsanov}
\date{\vspace{-5ex}}
\maketitle
\abstract{The search for $\xdecay$ decays of a new short-lived neutral boson $X$ with a mass $m_X =16.7$ MeV and coupling to
electrons in the range $ 10^{-4}\lesssim \epsilon_e \lesssim 10^{-3}$ in the NA64 experiment is reported. The analysis uses
the data corresponding to $5.4 \times 10^{10}$ electrons on the active target (EOT) collected in the second half of the
2017 run. It consisted of two parts, first with 40X WCAL configuration (with catcher behind), with $2.4 \times 10^{10}$ EOT
and second with 30X WCAL configuration. No signal events were found after applying all cuts.}


\section{Introduction}


 The experiment NA64 is designed primarily to search for dark photons A' through missing
energy signatures in the SPS beams of electrons. The run in the visible decay mode
configuration was also performed in 2017. This configuration consisted of two calorimeters,
WCAL and ECAL, the veto counter V2 16x16x2 $cm^3$ after WCAL, the $e+e-$ detection counter S4
before the ECAL and other usual detectors, as in the invisible mode configuration.


\section{Simulation of the dark photons production}
 

 The production of dark photons A' off nuclei, which is the signal that we search for, was
performed by the code described in \cite{DarkPhotons}, compiled as a part of the Geant4
application. We assumed that both electrons and positrons of the electromagnetic shower initiated
by the beam electron in the tungsten calorimeter WCAL can produce A' with the same cross section.
For the visible mode configuration the subsequent decay of A' to $e^+e^-$ was simulated.
The resulting electron - positron pair was traced by Geant4 in the same way as
all other particles. The life time of A' depends on its mass and on the mixing constant $\epsilon$.
We used the mass 16.7 MeV and $\epsilon = 0.000316$ as a reference point.


\section{Checks of calibration}


 The WCAL calibration check in the electron calibration run 3400 is shown in Fig.~\ref{Wcalib}. It was corrected w.r.t.
the Donskov code calibration by 3.9\% up. In the 30X geometry data it was corrected by 6\% down.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{Wcalib.pdf}}
\end{center}
\caption{Energy deposition in WCAL in the electron calibration run 3400}
  \label{Wcalib}
\end{figure}

 As usual gamma conversion to muons is a very important reference process. Such events were selected by the upper cut
on the WCAL energy at 70 GeV and by requiring the energy in HCAL0, HCAL2 to be between 2.5 and 6.25 GeV
(upper cut 6.35 GeV in data instead of 6.25 GeV in MC, the same result can be obtained by smearing  the HCAL energy in MC).
The WCAL - HCAL 2D plot is shown in Fig.~\ref{WCAL_dimu_2D}. The WCAL energy deposition is shown in Fig.~\ref{WCAL_dimu}.
The number of selected events in data is 0.78 of the MC prediction.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{WCAL_dimu_2D.pdf}}
\end{center}
\caption{2D plot WCAL - HCAL (modules 0 - 2) for the selected dimuon events}
  \label{WCAL_dimu_2D}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{WCAL_dimu.pdf}}
\end{center}
\caption{Energy deposition in WCAL for the selected dimuon events}
  \label{WCAL_dimu}
\end{figure}

 The calibrations of counters V2 and S4 (the thin counter before the ECAL) were checked with dimuon events. 
The V2 and S4 energy distributions are shown in Fig.~\ref{V2_dimu} and  Fig.~\ref{S4_dimu}. Unfortunately the
V2 counter worked worse in the second half of the run, in 30X geometry, probably it was partly damaged.
This can be seen in Fig.~\ref{V2_dimu_30X}.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{V2_dimu.pdf}}
\end{center}
\caption{Energy deposition in V2 for the selected dimuon events}
  \label{V2_dimu}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{V2_dimu_30X.pdf}}
\end{center}
\caption{Energy deposition in V2 for the selected dimuon events in 30X geometry}
  \label{V2_dimu_30X}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{S4_dimu.pdf}}
\end{center}
\caption{Energy deposition in S4 for the selected dimuon events}
  \label{S4_dimu}
\end{figure}

 In the first part of the run a catcher was installed immediately after WCAL. It is a short calorimeter similar
to WCAL with 11 layers. The upper cut on the energy in it was used against the backgrounds from $\pi$ charge
exchange reaction an other reactions with hadrons. The signal in it for dimuon events is compared to MC in
Fig.~\ref{Catcher_dimu}.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{Catcher_dimu.pdf}}
\end{center}
\caption{Energy deposition in the catcher for the selected dimuon events}
  \label{Catcher_dimu}
\end{figure}


\section{Choosing cuts for the signal selection}


 The signal event candidates were selected with the upper cut in V2 at 0.0025 GeV (about 0.6 MIP) and lower
cut in S4 at 0.0003 GeV (about 1.5 MIP). As can be seen in Fig.~\ref{V2_noise}, in the calibration run about 8\% of
events have energy higher than 0.0025 GeV in V2. This tail is well reproduced by MC, for this reason it can be
attributed to the tails of electromagnetic showers in WCAL.

 Three additional criteria were used for the signal selection. The first is the requirement that the ECAL cell with
the maximal energy deposition is the cell (3,3), the cell where the beam enters the ECAL when WCAL is not installed.
The signal simulation showed that the efficiency of this requirement for the signal is higher than 99\%. The second
requirement is the requirement of no significant energy ($ < 1 MIP$) in VETO, it ensures that there are no hadrons after
the WCAL. Finally, the energy in the HCAL module 0 should be less than 1.5 GeV

 The reduction flow for the signal and some backgrounds is in the Table~\ref{table:reduction1}. The signal was simulated
with the internal lower cut on the energy of electrons/positrons that emit A' at 18 GeV, so that the trigger cut on the WCAL
energy has not too low efficiancy. A' was forced to decay in the region 2 m long starting after the last converter of WCAL.
The corresponding weight was calculated and stored for each event.

\begin{table}[h]
\caption{MC reduction table for the signal, $e^-$, $\pi^-$ and $K^0_S$. SRD selection was not applied to $\pi^-$ and $K^0_S$.}
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
Cut                          & $N_{accepted}$ signal & $N_{accepted}$ MC $e^-$  & $N_{accepted}$ MC $\pi^-$ & $N_{accepted}$ MC $K^0_S$ \\
\hline
Initial                      &   -                  &   100000               & 50000                        & 100000 \\
\hline
1. $WCAL < 70$, $ECAL > 1.1$ &  267                 &   0                    & 31000                        &  57000 \\
\hline
2. $V2 < 0.6 MIP$, $W2 < 0.1$&  213                 &                        & 2                            &  33500 \\
\hline
3. $S4 > 1.5 MIP$            &  203                 &                        & 0                            &  5500  \\
\hline
4. $VETO < 1 MIP$            &  199                 &                        & 0                            &  432   \\
\hline
5. $HCAL0 < 1.5$ GeV         &  199                 &                        & 0                            &  387   \\
\hline
6. ECAL max in (3,3)         &  199                 &                        & 0                            &  338   \\
\hline
7. $WCAL+ECAL > 90$ GeV      &  199                 &                        & 0                            &  not made \\
\hline
$\chi^2 < 10.$               &  196                 &                        &                              &  28    \\
\hline
\end{tabular}
\end{center}
\label{table:reduction1}
\end{table}


\section{Using shower profile}


 The shape of the electromagnetic shower in ECAL was parameterized using electrons from the calibration runs
in the invisible mode geometry and MC simulation of electrons. For the signal events that we are looking for
there are two electrong in the ECAL, but usually they are very close to each other. For this reason the
shower shape analysis can still be efficient to reject background.

 The calculation of the shower shape $\chi^2$ requires the coordinates of entry of electron into the ECAL.
The method predicts energy deposition in the ECAL cells and compared them with the actually measured ones.
At the first step of the analysis these coordinated were estimated from the ECAL data. The "center of gravity"
of the energy depositions in the ECAL cells was calculated (3X3 matrix around the the cell with maximal
energy deposition was used). From its coordinates the entry point coordinated were calculated using the
correction function that was obtaited by fitting the dependence observed in MC simulated events.

 The distribution of $\chi^2$ for single electrons in MC and data is shown in Fig.~\ref{chi2_MC} and
Fig.~\ref{chi2_data}. The expected distribution for the signal is shown in Fig.~\ref{chi2_signal}.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{chi2_MC.pdf}}
\end{center}
\caption{ECAL shower profile $\chi^2$ for MC simulated 100 GeV electrons}
  \label{chi2_MC}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{chi2_data.pdf}}
\end{center}
\caption{ECAL shower profile $\chi^2$ for the 100 GeV electron calibration run (invisible mode)}
  \label{chi2_data}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{chi2_signal.pdf}}
\end{center}
\caption{ECAL shower profile $\chi^2$ for the signal events}
  \label{chi2_signal}
\end{figure}

 Finally, to check the shower profile performance in the visible mode conditions, we selected muons with hard
delta electrons emitted in ECAL. We required more than 25 GeV in ECAL and additional purity cuts in the catcher
and V2 in order to select clean muons (without significant delta electron activity before ECAL). Also, in order
to select early emission in ECAL we required small energy deposition in the VETO. In the hadron calibration run
3399 only 7 events remained after selection, all of them have $\chi^2$ less than 10 (Fig.~\ref{chi2_mu_delta_e}).

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{chi2_mu_delta_e.pdf}}
\end{center}
\caption{ECAL shower profile $\chi^2$ for the signal events}
  \label{chi2_mu_delta_e}
\end{figure}

 The conclusion is that it is safe to cut the shower profile $\chi^2$ at 10 to select signal events.


\section{Comparison with MC}


 The efficiency of the criteria described above to signal can be correctly estimated only if the subtetectors are
well calibrated and the distributions of energy deposition in WCAL, V2, S4 and VETO are correctly simulated.
This is checked using the calibration runs, where there is no upper cut on the energy deposition in calorimeters.

 As mentioned above, the upper cut on the energy deposition in the catcher was used. The distribution of this quantity
in the calibration run is compared with MC in Fig.~\ref{Catcher_calib}.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{Catcher_calib.pdf}}
\end{center}
\caption{Energy deposition in the catcher in the electron calibration run 3400}
  \label{Catcher_calib}
\end{figure}

 The V2 counter (immediately after WCAL) energy deposition distribution is compared to MC in Fig.~\ref{V2_noise}.
The agreement is reasonable (better than in 2016). In data, about 8\% of events have energy higher than 0.0025 GeV in V2.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{V2_calib.pdf}}
\end{center}
\caption{Energy deposition in V2 in the electron calibration run 3400}
  \label{V2_noise}
\end{figure}

 The same comparison is made for the 30X geometry in Fig.~\ref{V2_noise_30X}. The tail is much bigger. However, the
shower for signal events is smaller and shorter.

 The problem with V2 in the second half of the run can be seen also in the calibration runs. Sometimes the signal
dropped by a factor of 2 - 2.5, as for example in the calibration run 3466.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{V2_calib_30X.pdf}}
\end{center}
\caption{Energy deposition in V2 in the electron calibration run 3521 (30X geometry)}
  \label{V2_noise_30X}
\end{figure}


\section{Background estimation}


 It is very difficult to calculate the background by simulating the full statistics of the
2017 run. Several studies on data and MC were performed in order to have some idea about
the background.

 The first study is the search for gammas and other neutral particles escaping from WCAL. If such events are found,
it is possible to get the idea about the background multiplying their number by the small probability of
conversion in the air and in the counters between WCAL and ECAL. The search for gammas required that the
maximal energy deposition in ECAL is in (3,3), required no energy in VETO (less than 1 MIP), less than 0.6 MIP in V2,
less than 0.6 MIP in catcher, less than 0.75 MIP in S4, less than 1 GeV in HCAL0. The resulting distribution
before applying the shower profile cut is shown in Fig.~\ref{neutrals}. Requirement that the sum of energies in WCAL
and ECAL is in the range from 85 to 120 GeV leaves three events.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{W0.pdf}}
\end{center}
\caption{Biplot for neutrals, shower profile cut not applied.}
  \label{neutrals}
\end{figure}

 The most probable source of both signal-like events and neutrals are $K^0_S$ produced in the WCAL and decayed
to $\pi^0 \pi^0$. We performed the MC study of this source. First of all, we simulated $pi^-$ incident on the WCAL
and calculated the number of produced hard $K^0_S$. The probability to produce $K^0_S$ with energy higher than
50 GeV is approximately 0.002. The upper limit on this probabilty for electrons is $10^{-5}$. MC simulation with
biased cross section is needed to estimate it.

 The second exercise was to simulate 50 GeV $K^0_S$ inside WCAL along the line of the initial beam deflected by
the magnets and leaving them decay or interact in the subdetectors. Some of such events look like signal events,
some of them look like neutral events described above. The ratio between them allows to estimate the background
multiplying it by the number of neutral events found in data. This gives the backgroud estimate os about 0.06
for the full 2017 visible mode statistics.


\section{Results}


 The result of the search for signal events is shown in Fig.~\ref{result}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{W.pdf}}
\end{center}
\caption{WCAL - ECAL plot of signal - like events.}
  \label{result}
\end{figure}

 The data reduction flow is in the Table~\ref{table:reduction2}. Initial number of data events is counted after the SRD selection.

\begin{table}[h]
\caption{Data reduction table}
\begin{center}
\begin{tabular}{|c|c|}
\hline
Cut                    & $N_{accepted}$       \\
\hline
Initial                &  $5.75 \times 10^7$  \\
\hline
$WCAL < 70$ GeV        &  $3.2 \times 10^7$   \\
\hline
$V2 < 0.6 MIP$         &  $5   \times 10^5$   \\
\hline
$S4 > 1.5 MIP$         &  2400                \\
\hline
$VETO < 1 MIP$         &  8                   \\
\hline
$HCAL0 < 1.5$ GeV      &  2                   \\
\hline
ECAL max in (3,3)      &  2                   \\
\hline
$WCAL+ECAL > 90$ GeV   &  0                   \\
\hline
\end{tabular}
\end{center}
\label{table:reduction2}
\end{table}

 After this first processing the requirement of less than 0.6 MIP in the catcher was added, which killed one candidate event.

 There is 1 candidate events in data, to be analysed in details, using shower profile, tracker etc.

 The signal yield estimation is in Table~\ref{table:signal1} and Table~\ref{table:signal2}.

\begin{table}[h]
\caption{Signal yield estimation for the Berillium anomaly ($M_X = 16.7 MeV$), 40X geometry}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\epsilon$             &  Normalization      & Reco acepted  & $N_{sign}$ per $10^10$ EOT \\
\hline
0.000316               &  $1.9*10^{-8}$      & 216/44000     & 0.934                      \\
\hline
0.0002                 &  $0.76*10^{-8}$     & 579/44000     & 1.                         \\
\hline
0.0001                 &  $0.19*10^{-8}$     & 660/44000     & 0.285                      \\
\hline
0.0004                 &  $2.77*10^{-8}$     & 100/44000     & 0.582                      \\
\hline
0.0005                 &  $4.09*10^{-8}$     & 28.5/44000    & 0.265                      \\
\hline
0.0006 small V2        &  $5.89*10^{-8}$     & 5.2/44164     & 0.069                      \\
\end{tabular}
\end{center}
\label{table:signal1}
\end{table}


\begin{table}[h]
\caption{Signal yield estimation for the Berillium anomaly ($M_X = 16.7 MeV$), 30X geometry}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\epsilon$             &  Normalization      & Reco acepted  & $N_{sign}$ per $10^10$ EOT \\
\hline
0.000316               &  $1.73*10^{-8}$     & 220/44000     & 0.865                      \\
\hline
0.0002                 &  $0.69*10^{-8}$     & 427/44000     & 0.67                       \\
\hline
0.0004                 &  $2.62*10^{-8}$     & 10/4400       & 0.59                       \\
\hline
0.0006                 &  $5.89*10^{-8}$     & 19.3/44172    & 0.257                      \\
\hline
0.0006 small V2        &  $5.89*10^{-8}$     & 22/44158      & 0.293                      \\
\end{tabular}
\end{center}
\label{table:signal2}
\end{table}

 We have a maximal sensitivity at $\epsilon ~0.0002$. The expected limit here is $??? \times BeA$, where $BeA$ is what expected
from the Berillium anomaly X boson with Dark Photon cross sections and 100\% branching to $e^+e^-$.

 After opening the signal box for the full statistics we found 2 candidates in the 30X geometry. However, they did not pass
the very first test: removing the timing cut. It turned out that both candidates disappear if the timing cut is relaxed from
4 to 5 sigma because in this case in V2 there is signal above the cut.

 With no candidates remaining we exclude the range of mixing strengths of the X boson $0.00011 < \epsilon < 0.00042$

 We proceeded then with setting limits on the A' decaying to $e^+e^-$ as a function of the A' mass. The yields of signal events
are shown in the Tables \ref{table:signal3} - \ref{table:signal9}. Some of yields are calculated for the future run, for example
with a small V2 (cylinder counter with a diameter of 3 cm). It is possible that some data will be taken with the beam energy of
150 GeV. The corresponding yields are in the Table \ref{table:signal10} and beyond.

\begin{table}[h]
\caption{Signal yield estimation for the X ($M_X = 10 MeV$), 40X geometry}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\epsilon$             &  Normalization      & Reco acepted  & $N_{sign}$ per $10^10$ EOT \\
\hline
0.000316               &  $4.08*10^{-8}$     & 751/44000     & 6.96                       \\
\hline
0.0006                 &  $14.7*10^{-8}$     & 174/44000     & 5.8                        \\
\hline
0.001                  &  $40.9*10^{-8}$     & 8.6/44000     & 0.8                        \\
\hline
0.0001                 &  $0.409*10^{-8}$    & 428/44000     & 0.398                      \\
\hline
0.0012                 &  $58.9*10^{-8}$     & 1.1/44000     & 0.147                      \\
\end{tabular}
\end{center}
\label{table:signal3}
\end{table}


\begin{table}[h]
\caption{Signal yield estimation for the X ($M_X = 5 MeV$), 40X geometry}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\epsilon$             &  Normalization      & Reco acepted  & $N_{sign}$ per $10^10$ EOT \\
\hline
0.000316               &  $13.2*10^{-8}$     & 675/44000     & 20.3                       \\
\hline
0.0016                 &  $340*10^{-8}$      & 37/44000      & 28.6                       \\
\hline
0.0025                 &  $832*10^{-8}$      & 0.0265/44000  & 0.05                       \\
\hline
0.002                  &  $532*10^{-8}$      & 3./17664      & 9                          \\
\hline
0.0001                 &  $1.33*10^{-8}$     & 55/17674      & 0.41                       \\
\hline
0.00005                &  $0.333*10^{-8}$    & 12/17682      & 0.02                       \\
\hline
0.00009                &  $1.078*10^{-8}$    & 44/17598      & 0.269                      \\
\hline
\end{tabular}
\end{center}
\label{table:signal4}
\end{table}


\begin{table}[h]
\caption{Signal yield estimation for the X ($M_X = 25 MeV$), 30X geometry}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\epsilon$             &  Normalization      & Reco acepted  & $N_{sign}$ per $10^10$ EOT \\
\hline
0.0002                 &  $0.25*10^{-8}$     & 120/17612     & 0.17                       \\
\hline
0.000316               &  $0.635*10^{-8}$    & 27/17659      &                            \\
\end{tabular}
\end{center}
\label{table:signal5}
\end{table}


\begin{table}[h]
\caption{Signal yield estimation for the X ($M_X = 22 MeV$), 40X geometry}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\epsilon$             &  Normalization      & Reco acepted  & $N_{sign}$ per $10^10$ EOT \\
\hline
0.000316               &  $0.816*10^{-8}$    & 37/17675      & 0.171                      \\
\hline
0.00025                &  $0.511*10^{-8}$    & 85/17705      & 0.245                      \\
\hline
0.00022                &  $0.396*10^{-8}$    & 135/17522     & 0.305                      \\
\hline
0.00021                &  $0.361*10^{-8}$    & 146/17552     & 0.3                        \\
\hline
0.0002                 &  $0.327*10^{-8}$    & 158/17718     & 0.292                      \\
\hline
0.00023                &  $0.432*10^{-8}$    & 110/17587     & 0.27                       \\
\hline
\end{tabular}
\end{center}
\label{table:signal6}
\end{table}


\begin{table}[h]
\caption{Signal yield estimation for the X ($M_X = 22 MeV$), 30X geometry}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\epsilon$             &  Normalization      & Reco acepted  & $N_{sign}$ per $10^10$ EOT \\
\hline
0.0002                 &  $0.327*10^{-8}$    & 123/17612     & 0.228                      \\
\hline
0.00015                &  $0.184*10^{-8}$    & 167/17660     & 0.174                      \\
\hline
\end{tabular}
\end{center}
\label{table:signal7}
\end{table}


\begin{table}[h]
\caption{Signal yield estimation for the X ($M_X = 20 MeV$), 40X geometry}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\epsilon$             &  Normalization      & Reco acepted  & $N_{sign}$ per $10^10$ EOT \\
\hline
0.000316               &  $0.953*10^{-8}$    & 63/17685      & 0.339                      \\
\hline
\end{tabular}
\end{center}
\label{table:signal8}
\end{table}


\begin{table}[h]
\caption{Signal yield estimation for the X ($M_X = 20 MeV$), 30X geometry}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\epsilon$             &  Normalization      & Reco acepted  & $N_{sign}$ per $10^10$ EOT \\
\hline
0.0003                 &  $0.859*10^{-8}$    & 81/17633      & 0.329                      \\
\hline
\end{tabular}
\end{center}
\label{table:signal9}
\end{table}


\begin{table}[h]
\caption{Signal yield estimation for the X ($M_X = 16.7 MeV$), 30X geometry, 150 GeV}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\epsilon$             &  Normalization      & Reco acepted  & $N_{sign}$ per $10^10$ EOT \\
\hline
0.0006 V2 D=3 thin     &  $6.87*10^{-8}$     & 46/43958      & 0.719                      \\
\hline
0.0006 V2 D=3          &  $6.87*10^{-8}$     & 33/44052      & 0.515                      \\
\hline
0.0006 V2 12x12 inside &  $6.87*10^{-8}$     & 33/44055      & 0.514                      \\
\hline
0.0006 V2 6x6 inside   &  $6.87*10^{-8}$     & 43/44051      & 0.670                      \\
\hline
0.0006 V2 6x12 inside  &  $6.87*10^{-8}$     & 39.6/45873    & 0.593                      \\
\hline
0.0006 V2 6x12 after W &  $6.87*10^{-8}$     & 36.2/45813    & 0.543                      \\
\hline
0.0006 V2 6x12 after Al&  $6.87*10^{-8}$     & 44.2/45863    & 0.662                      \\
\end{tabular}
\end{center}
\label{table:signal10}
\end{table}


\begin{table}[h]
\caption{Signal yield estimation for the X ($M_X = 16.7 MeV$), 40X geometry, 150 GeV}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\epsilon$             &  Normalization      & Reco acepted  & $N_{sign}$ per $10^10$ EOT \\
\hline
0.0006 V2 D=3          &  $6.87*10^{-8}$     & 21/44031      & 0.327                      \\
\hline
\end{tabular}
\end{center}
\label{table:signal11}
\end{table}

	
\begin{thebibliography}{299}
		
\bibitem{DarkPhotons}
S.N. Gninenko, N.V. Krasnikov, M.M. Kirsanov, D.V. Kirpichnikov, Missing energy signature from invisible decays of dark photons at the CERN SPS, Phys. Rev. D 94, 095025 (2016), arXiV:1604.08432 [hep-ph]
\end{thebibliography}
	
\end{document}
