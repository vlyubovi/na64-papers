{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_W_e100.root");
  TFile myfile1("hist_Wcalib_3400.root");

  TH1D* hMC = (TH1D*)myfile.Get("ev2");
  TH1D* hDATA = (TH1D*)myfile1.Get("ev2");

  c1 = new TCanvas("V2_calib"," ",200,10,900,600);
  hMC->Rebin(4);
  hDATA->Rebin(4);
  hMC->SetStats(false);
  hMC->SetTitle("");
  hMC->SetXTitle("Energy in V2 [GeV]");
  hMC->SetYTitle("Events");
  hDATA->SetXTitle("Energy in V2 [GeV]");
  hDATA->SetYTitle("Events");

  hMC->Scale(hDATA->Integral()/hMC->Integral());

  hMC->Draw("HIST");
  hDATA->Draw("ESAME");
}
