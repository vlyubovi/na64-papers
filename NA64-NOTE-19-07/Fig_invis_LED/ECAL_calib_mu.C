{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  //gStyle->SetMarkerSize(0.5);

  TFile myfile("hist_calib_mu_MC.root");
  TFile myfile1("hist_calib_mu_3922_nopile.root");

  TH1D* hMC = (TH1D*)myfile.Get("eecalcentral");
  TH1D* hDATA = (TH1D*)myfile1.Get("eecalcentral");

  c1 = new TCanvas("ECAL_calib_mu"," ",200,10,900,600);
  hMC->SetStats(false);
  hMC->SetTitle("");
  hMC->SetXTitle("Energy in ECAL [GeV]");
  hMC->SetYTitle("Events");
  hMC->SetLineColor(kRed);
  hDATA->SetXTitle("Energy in ECAL [GeV]");
  hDATA->SetYTitle("Events");
  hDATA->SetMarkerSize(0.5);

  hMC->Scale(hDATA->Integral()/hMC->Integral());

  hMC->Draw("HIST");
  hDATA->Draw("ESAME");
}
