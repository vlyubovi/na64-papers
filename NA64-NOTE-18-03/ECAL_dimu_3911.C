{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_dimu_MC_hcal01cut_12f.root");
  TFile myfile1("hist_dimu_hcal01cut_3911.root");

  TH1D* hMC = (TH1D*)myfile.Get("eecal");
  TH1D* hDATA = (TH1D*)myfile1.Get("eecal");

  c1 = new TCanvas("WCAL_dimu"," ",200,10,900,600);
  hMC->Rebin(4);
  hDATA->Rebin(4);
  hMC->SetStats(false);
  hMC->SetTitle("");
  hMC->SetXTitle("Energy in WCAL [GeV]");
  hMC->SetYTitle("Events");
  hDATA->SetXTitle("Energy in WCAL [GeV]");
  hDATA->SetYTitle("Events");

  Double_t NMC = 45720.*200.*12.;
  Double_t NDATA = 489800000.;
  hMC->Scale(NDATA/NMC);
  cout << "MC predicted = " << hMC->Integral() << " data/MC = " << hDATA->Integral()/hMC->Integral() << endl;

  hMC->Draw("HIST");
  hDATA->Draw("ESAME");
}
