{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_W_calib_e_MC.root");
  TFile myfile1("hist_W_calib_e_4238.root");

  TH1D* hMC = (TH1D*)myfile.Get("ehcal0");
  TH1D* hDATA = (TH1D*)myfile1.Get("ehcal0");

  c1 = new TCanvas("HCAL0_e"," ",200,10,900,600);
  hMC->SetStats(false);
  hMC->SetTitle("");
  hMC->SetXTitle("Energy in HCAL0 [GeV]");
  hMC->SetYTitle("Events");
  hDATA->SetXTitle("Energy in HCAL0 [GeV]");
  hDATA->SetYTitle("Events");

  Double_t NMC = 14.*46525.*200.;
  Double_t NDATA = 485000000.;
  //hMC->Scale(NDATA/NMC);
  //cout << "MC predicted = " << hMC->Integral() << " data/MC = " << hDATA->Integral()/hMC->Integral() << endl;
  hMC->Scale(hDATA->Integral()/hMC->Integral());

  //hMC->Draw("HIST");
  hDATA->Draw("E");
  hMC->Draw("HISTSAME");
}
