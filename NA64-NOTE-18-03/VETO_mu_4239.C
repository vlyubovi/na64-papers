{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_W_calib_mu_4239.root");
  TFile myfile1("hist_W_calib_mu_MC.root");

  TH1D* hMC = (TH1D*)myfile1.Get("eveto");
  TH1D* hDATA = (TH1D*)myfile.Get("eveto");

  c1 = new TCanvas("VETO_mu"," ",200,10,900,600);
//  hMC->Rebin(3);
//  hDATA->Rebin(3);
  hMC->SetStats(false);
  hMC->SetTitle("");
  hMC->SetXTitle("Energy in VETO [GeV]");
  hMC->SetYTitle("Events");
  hDATA->SetXTitle("Energy in VETO [GeV]");
  hDATA->SetYTitle("Events");
//  hMC->GetXaxis()->SetRange(2,20);

//  Double_t NMC = 4347.;
//  Double_t NDATA = 2348.;
  
  hMC->Scale(hDATA->Integral()/hMC->Integral());
//  cout << "MC predicted = " << hMC->Integral() << " data/MC = " << hMC->Integral()/hDATA->Integral() << endl;

  hMC->SetLineColor(2);
  hDATA->SetLineColor(4);

  hMC->Draw("HIST");
  hDATA->Draw("ESAME");
  
}
