{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_W_dimu_MC_hcal01cut_150_14f.root");
  TFile myfile1("hist_W_dimu_4244.root");

  TH1D* hMC = (TH1D*)myfile.Get("ev2");
  TH1D* hDATA = (TH1D*)myfile1.Get("ev2");

  c1 = new TCanvas("V2_dimu"," ",200,10,900,600);
  hMC->Rebin(2);
  hDATA->Rebin(2);
  hMC->SetStats(false);
  hMC->SetTitle("");
  hMC->SetXTitle("Energy in V2 [GeV]");
  hMC->SetYTitle("Events");
  hDATA->SetXTitle("Energy in V2 [GeV]");
  hDATA->SetYTitle("Events");

  Double_t NMC = 14.*46525.*200.;
  Double_t NDATA = 485000000.;
  //hMC->Scale(NDATA/NMC);
  //cout << "MC predicted = " << hMC->Integral() << " data/MC = " << hDATA->Integral()/hMC->Integral() << endl;
  hMC->Scale(hDATA->Integral()/hMC->Integral());

  hDATA->Draw("E");
  hMC->Draw("HISTSAME");
}
