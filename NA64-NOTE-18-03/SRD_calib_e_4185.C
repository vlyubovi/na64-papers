{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_calib_e_4185.root");
  TFile myfile1("hist_calib_e_MC.root");

  TH1D* hMC = (TH1D*)myfile1.Get("srdplot");
  TH1D* hDATA = (TH1D*)myfile.Get("srdplot");
  TH1D* hMC0 = (TH1D*)myfile1.Get("srdplot0");
  TH1D* hDATA0 = (TH1D*)myfile.Get("srdplot0");
  TH1D* hMC1 = (TH1D*)myfile1.Get("srdplot1");
  TH1D* hDATA1 = (TH1D*)myfile.Get("srdplot1");
  TH1D* hMC2 = (TH1D*)myfile1.Get("srdplot2");
  TH1D* hDATA2 = (TH1D*)myfile.Get("srdplot2");

  c1 = new TCanvas("srd_all_4185"," ",200,10,900,600);
//  hMC->Rebin(3);
//  hDATA->Rebin(3);
  hMC->SetStats(false);
  hMC->SetTitle("SRD");
  hMC0->SetStats(false);
  hMC0->SetTitle("SRD 0");
  hMC1->SetStats(false);
  hMC1->SetTitle("SRD 1");
  hMC2->SetStats(false);
  hMC2->SetTitle("SRD 2");

  hMC->SetXTitle("SRD");
  hMC->SetYTitle("Events");
  hMC0->SetXTitle("SRD");
  hMC0->SetYTitle("Events");
  hMC1->SetXTitle("SRD");
  hMC1->SetYTitle("Events");
  hMC2->SetXTitle("SRD");
  hMC2->SetYTitle("Events");

//  hDATA->SetXTitle("SRD all");
//  hDATA->SetYTitle("Events");
//  hMC->GetXaxis()->SetRange(2,20);

//hMC->GetXaxis()->SetLogY(2,20);
  c1->Divide(2,2)	;
  c1->cd(1);
  gPad->SetLogy();
  c1->cd(2);
  gPad->SetLogy();
  c1->cd(3);
  gPad->SetLogy();
  c1->cd(4);
  gPad->SetLogy();
//  c2->cd(0);
//  gPad->SetLogy();
//  Double_t NMC = 4347.;
//  Double_t NDATA = 2348.;
  
  hMC->Scale(hDATA->Integral()/hMC->Integral());
  cout << "MC predicted = " << hMC->Integral() << " data/MC = " << hMC->Integral()/hDATA->Integral() << endl;
  hMC0->Scale(hDATA0->Integral()/hMC0->Integral());
  cout << "MC predicted = " << hMC0->Integral() << " data/MC = " << hMC0->Integral()/hDATA0->Integral() << endl;
  hMC1->Scale(hDATA1->Integral()/hMC1->Integral());
  cout << "MC predicted = " << hMC1->Integral() << " data/MC = " << hMC1->Integral()/hDATA1->Integral() << endl;
  hMC2->Scale(hDATA2->Integral()/hMC2->Integral());
  cout << "MC predicted = " << hMC2->Integral() << " data/MC = " << hMC2->Integral()/hDATA2->Integral() << endl;

  hMC->SetLineColor(2);
  hDATA->SetLineColor(4);
  hMC0->SetLineColor(2);
  hDATA0->SetLineColor(4);
  hMC1->SetLineColor(2);
  hDATA1->SetLineColor(4);
  hMC2->SetLineColor(2);
  hDATA2->SetLineColor(4);

  hDATA->SetMarkerSize(0.5);
  hDATA0->SetMarkerSize(0.5);
  hDATA1->SetMarkerSize(0.5);
  hDATA2->SetMarkerSize(0.5);


  c1->cd(1);
  hMC0->Draw("HIST");
  hDATA0->Draw("ESAME");	

  c1->cd(2);
  hMC1->Draw("HIST");
  hDATA1->Draw("ESAME");

  c1->cd(3);
  hMC2->Draw("HIST");
  hDATA2->Draw("ESAME");

  c1->cd(4);
  hMC->Draw("HIST");
  hDATA->Draw("ESAME");



//  hMC->Draw("HIST");
//  hMC1->Draw("SAME");  
//  hMC2->Draw("SAME");

 // hDATA->Draw("ESAME");
  
//  hDATA1->Draw("ESAME");
 // hDATA2->Draw("ESAME");

  // c1->cd(1);
   auto legend1 = new TLegend(0.5,0.7,0.9,0.9);
   auto legend2 = new TLegend(0.5,0.7,0.9,0.9);
   auto legend3 = new TLegend(0.5,0.7,0.9,0.9);
   auto legend4 = new TLegend(0.5,0.7,0.9,0.9);

   //legend->SetHeader("SRD, all modules"); // option "C" allows to center the header
   legend4->AddEntry(hMC,"SRD MC","l");
   legend4->AddEntry(hDATA,"SRD run 4185","lep");
   legend1->AddEntry(hMC0,"SRD0 MC","l");
   legend1->AddEntry(hDATA0,"SRD0 run 4185","lep");
   legend2->AddEntry(hMC1,"SRD1 MC","l");
   legend2->AddEntry(hDATA1,"SRD1 run 4185","lep");
   legend3->AddEntry(hMC2,"SRD2 MC","l");
   legend3->AddEntry(hDATA2,"SRD2 run 4185","lep");

//   legend->AddEntry("gr","Graph with error bars","lep");
   c1->cd(1);legend1->Draw();
   c1->cd(2);legend2->Draw();
   c1->cd(3);legend3->Draw();
   c1->cd(4);legend4->Draw();
}
