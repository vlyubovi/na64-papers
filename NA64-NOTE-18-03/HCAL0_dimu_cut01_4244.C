{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_W_dimu_4244.root");
  TFile myfile1("hist_W_dimu_MC_hcal01cut_150_14f.root");

  TH1D* hMC = (TH1D*)myfile1.Get("ehcal0");
  TH1D* hDATA = (TH1D*)myfile.Get("ehcal0");

  c1 = new TCanvas("HCAL0_dimu_cut01_4244"," ",200,10,900,600);
//  hMC->Rebin(2);
//  hDATA->Rebin(2);
  hMC->SetStats(false);
  hMC->SetTitle("");
  hMC->SetXTitle("Energy in HCAL0 [GeV]");
  hMC->SetYTitle("Events");
  hDATA->SetXTitle("Energy in HCAL0 [GeV]");
  hDATA->SetYTitle("Events");

  hMC->Scale(hDATA->Integral()/hMC->Integral());
  cout << "MC predicted = " << hMC->Integral() << " data/MC = " << hMC->Integral()/hDATA->Integral() << endl;

  hMC->SetLineColor(2);
  hDATA->SetLineColor(4);

  hMC->Draw("HIST");
  hDATA->Draw("ESAME");
  
}
