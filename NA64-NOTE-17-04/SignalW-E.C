{
  gROOT->Reset();
  TFile myfile("hist_signalMC.root");

  TH1D* hMC = (TH1D*)myfile.Get("ewecal-ecal");

  c1 = new TCanvas("SignalW-E"," ",200,10,900,600);

  hMC->SetStats(false);
  hMC->SetTitle("");
  //hMC->SetTitleOffset(1.5, "Y");

  hMC->SetXTitle("Energy in WCAL [GeV]");
  hMC->SetYTitle("Energy in ECAL [GeV]");
  hMC->Draw();
}
