\documentclass[12pt]{article}
\renewcommand{\baselinestretch}{1.65}
\topmargin -59pt
\textwidth 6.3in
\textheight 9.5in

\usepackage{epsf,epsfig,rotating}


\begin{document}

	
\begin{center}
  {\large Search for dark photons decaying to electron-positron pairs $e N \rightarrow A' \rightarrow e^+ e^-$ in NA64}
\end{center}
	

\section{Introduction}


 The experiment NA64 is designed primarily to search for dark photons A' through missing
energy signatures in the SPS beams of electrons. The test run in the visible decay mode
configuration was also performed in 2016. This configuration consisted in two calorimeters,
WCAL and ECAL, the veto counter VTWC 25x25x1 $cm^3$ after WCAL, the $e+e-$ detection counter VTEC
before the ECAL and other usual detectors, as in the invisible mode configuration.


\section{Simulation of the dark photons production}
 

 The production of dark photons A' off nucleai, which is the signal that we search for, was
performed by the code described in \cite{DarkPhotons}, compiled as a part of the Geant4
application. We assumed that both electrons and positrons of the electromagnetic shower initiated
by the beam electron in the tungsten calorimeter WCAL can produce A' with the same cross section.
For the visible mode configuration the subsequent decay of A' to $e^+e^-$ was simulated.
The resulting electron - positron pair was traced by Geant4 in the same way as
all other particles. The life time of A' depends on its mass and on the mixing constant $\epsilon$.
We used the mass 16.7 MeV and $\epsilon = $ as a reference point.


\section{Choosing cuts for the signal selection}


 It turned out that the veto counter VTWC was wider than needed. Due to particles exiting through
the sides of WCAL, for the VTWC cut less than one MIP the efficiency to signal dropped significantly.
So the lower cut at approximately 1.3 MIP was chosen. The upper cut at the same value was chosen for
VTEC, selecting events with at least two charged particles before the ECAL.

 The distribution of the selected signal events on the $E_{WCAL}$ - $E_{ECAL}$ plot is shown in
Fig.~\ref{signalW-E}. The distribution of the total energy deposited in WCAL and ECAL is shown in
Fig.~\ref{signalWE}. This total energy deposition is the main variable for the signal selection.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{signalW-E.eps}}
\end{center}
\caption{Energy deposition in ECAL vs energy deposition in WCAL for the simulated signal events after selection}
  \label{signalW-E}
\end{figure}
 

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{signalWE.eps}}
\end{center}
\caption{Distribution of the total energy deposited in WCAL and ECAL for the simulated signal events after selection}
  \label{signalWE}
\end{figure}
 
 Two additional criteria were used for the signal selection. The first is the requirement that the ECAL cell with
the maximal energy deposition is the cell (3,3), the cell where the beam enters the ECAL when WCAL is not installed.
The signal simulation showed that the efficiency of this requirement for the signal is higher than 99\%. The second
requirement is the requirement of no energy ($ < 0.5 MIP$) in VETO, it ensures that there are no hadrons after
the WCAL.

 The reduction table for the signal and some backgrounds is below. The signal was simulated with the internal lower
cut on the energy of electrons/positrons that emit A' at 18 GeV, so that the trigger cut on the WCAL energy has
not tool low efficiancy. A' was forced to decay in the region between the last converter of WCAL and the S2 counter.
The corresponding weight was calculated for each event.

\begin{table}[h]
\caption{MC reduction table. SRD selection was not applied to $\pi^-$}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
Cut                    & $N_{accepted}$ signal & $N_{accepted}$ MC $e^-$  & $N_{accepted}$ MC $\pi^-$ \\
\hline
Initial                &   -                  &   100000               & 50000   \\
\hline
$WCAL energy < 70 GeV$ &  1720                &   16                   & 46000   \\
\hline
$VTWC < 1.3 MIP$       &  1132                &   1                    & 13767   \\
\hline
$VTEC > 1.3 MIP$       &  1131                &   0                    & 2781    \\
\hline
$VETO < 0.8 MIP$       &  1065                &                        & 21      \\
\hline
ECAL max in (3,3)      &  1063                &                        & 5       \\
\hline
$WCAL+ECAL > 92 GeV$   &  1062                &                        & 5       \\
\hline
\end{tabular}
\end{center}
\label{table:reduction1}
\end{table}


\section{Comparison with MC}


 The efficiency of the criteria described above to signal can be correctly estimated only if the subtetectors are
well calibrated and the distributions of energy deposition in WCAL, VTWC, VTEC and VETO are correctly simulated.
This is checked using the calibration runs, where there is no upper cut on the energy deposition in calorimeters.
The distribution of the WCAL energy deposition is shown in Fig.~\ref{WCAL_calib} (the first two files of the run).
The corresponding distribution in the full run is shown in Fig.~\ref{WCAL_calib_fullrun} and shows some
miscalibration.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{WCAL_calib0.eps}}
\end{center}
\caption{Distribution of the total energy deposited in WCAL in the calibration run (first two files).}
  \label{WCAL_calib}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{WCAL_calib_fullrun.eps}}
\end{center}
\caption{Distribution of the total energy deposited in WCAL in the calibration run}
  \label{WCAL_calib_fullrun}
\end{figure}

 One of the most important distributions to be compared is the distribution of energy in VTWC
(leakage energy). The calibration of this counter was checked using the selected dimuon events in the
physical runs. For the calibration run and simulated electrons the distributions are shown in Fig.~\ref{VTWC_calib}.
They are rather similar. Note that in the signal events this leakage is much smaller because significant part of the
beam energy is carried away from WCAL by A', see Fig.~\ref{VTWC_signal_MC}.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{VTWC_calib.eps}}
\end{center}
\caption{Distribution of the energy deposition in VTWC for 100 GeV electrons (histogram) and in the calibration run}
  \label{VTWC_calib}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{VTWC_signal.eps}}
\end{center}
\caption{Distribution of the energy deposition in VTWC in the simulated and selected events}
  \label{VTWC_signal_MC}
\end{figure}


\section{Background estimation}


 It is very difficult to calculate the background by simulating the full statistics of the
october 2016 run. Several studies on data and MC were performed in order to have some idea about
the background.

 The first study is the search for gammas escaping from WCAL. If such events are found, it is
possible to get the idea about the background multiplying their number by the small probability of
conversion in the air and in the counters between WCAL and ECAL. The search for gammas required that the
maximal energy deposition in ECAL is in (3,3), required no energy in VETO, less than 1.3 MIP inVTWC,
less than 0.7 MIP in VTEC. The result is shown in Fig.~\ref{neutrals}.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{neutrals.eps}}
\end{center}
\caption{WCAL - ECAL plot of neutrals.}
  \label{neutrals}
\end{figure}


\section{Results}


 The result of the search for signal events is shown in Fig.~\ref{result}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{DataW-E.eps}}
\end{center}
\caption{WCAL - ECAL plot of signal - like events.}
  \label{result}
\end{figure}

 The dat reduction table (for the moment on about 60\% of statistics) is below. Initial number of data events
is counted after the SRD selection.


\begin{table}[h]
\caption{Data reduction table}
\begin{center}
\begin{tabular}{|c|c|}
\hline
Cut                    & $N_{accepted}$        \\
\hline
Initial                &  2600000             \\
\hline
$WCAL energy < 70 GeV$ &  2500000             \\
\hline
$VTWC < 1.3 MIP$       &  850000              \\
\hline
$VTEC > 1.3 MIP$       &  630000              \\
\hline
$VETO < 0.8 MIP$       &  4000                \\
\hline
ECAL max in (3,3)      &  16                  \\
\hline
$WCAL+ECAL > 92 GeV$   &  0                   \\
\hline
\end{tabular}
\end{center}
\label{table:reduction2}
\end{table}







	
\begin{thebibliography}{299}
		
\bibitem{DarkPhotons}
S.N. Gninenko, N.V. Krasnikov, M.M. Kirsanov, D.V. Kirpichnikov, Missing energy signature from invisible decays of dark photons at the CERN SPS, Phys. Rev. D 94, 095025 (2016), arXiV:1604.08432 [hep-ph]
\end{thebibliography}
	
\end{document}
