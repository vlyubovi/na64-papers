{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_W0_all.root");
  TFile myfile1("hist_W_all.root");

  TH1D* hDATA = (TH1D*)myfile.Get("eweplot");
  TH1D* hDATA1 = (TH1D*)myfile1.Get("eweplot");

  c1 = new TCanvas("w-e-neutrals"," ",200,10,900,600);
  hDATA->SetStats(false);
  hDATA->SetTitle("");
//  hDATA->SetTitleOffset(2., "X");
//  hDATA->SetTitleOffset(2., "Y");
  hDATA->SetXTitle("Energy in WCAL [GeV]");
  hDATA->SetYTitle("Energy in ECAL [GeV]");

  hDATA->SetMarkerStyle(21);
  hDATA->SetMarkerSize(1.);
  hDATA->SetMarkerColor(4);

  hDATA->SetAxisRange(0., 120., "Y");
  hDATA->SetAxisRange(0., 120., "X");

  hDATA1->SetStats(false);
  hDATA1->SetTitle("");
  hDATA1->SetTitleOffset(2., "X");
  hDATA1->SetTitleOffset(2., "Y");
  hDATA1->SetXTitle("Energy in WCAL [GeV]");
  hDATA1->SetYTitle("Energy in ECAL [GeV]");

  hDATA1->SetMarkerStyle(21);
  hDATA1->SetMarkerSize(1.);
  hDATA1->SetMarkerColor(2);

  hDATA1->SetAxisRange(0., 120., "Y");
  hDATA1->SetAxisRange(0., 120., "X");

  for (int ix = 1; ix <= 70; ix++) {
    for (int iy = 1; iy <= 70; iy++) {
      int ip1=1; // remove events in the signal box that do not pass the shower profile cut
      int ip2=1;
      int ip3=1;
      double esum = 2.*(float)ix - 1. + 2.*(float)iy - 1.;
      if(esum > 89. && esum < 111.) {
        if( (fabs(2.*(float)ix - 1. - 22.14) > 1.1) || (fabs(2.*(float)iy - 1. - 66.79) > 1.1) ) ip1 = 0;
        if( (fabs(2.*(float)ix - 1. - 66.69) > 1.1) || (fabs(2.*(float)iy - 1. - 32.42) > 1.1) ) ip2 = 0;
        if( (fabs(2.*(float)ix - 1. - 46.33) > 1.1) || (fabs(2.*(float)iy - 1. - 47.71) > 1.1) ) ip3 = 0;
        if(!ip1 && !ip2 && !ip3) hDATA->SetBinContent(ix, iy, 0.);
      }
      if( (2.*(float)iy-1.) < 6. ) hDATA->SetBinContent(ix, iy, 0.); // remove events with ECAL < 6 GeV
    }
  }

  hDATA->Draw();
  hDATA1->Draw("SAME");

  /*
  // Draw signal box

  Double_t sx1[2] = {0., 90.};
  Double_t sy1[2] = {90., 0.};
  Double_t sx2[2] = {0., 110.};
  Double_t sy2[2] = {110., 0.};
  const Int_t N1 = 2;

  TGraph *gr3 = new TGraph(N1, sx1, sy1);
  gr3->SetLineColor(kGreen+2);
  gr3->SetLineWidth(2);
  gr3->SetMarkerSize(0.01);
  gr3->SetTitle("");
  gr3->Draw("CP");

  TGraph *gr4 = new TGraph(N1, sx2, sy2);
  gr4->SetLineColor(kGreen+2);
  gr4->SetLineWidth(2);
  gr4->SetMarkerSize(0.01);
  gr4->SetTitle("");
  gr4->Draw("CP");
  */

  // Define shaded area of the signal box
  const Int_t N = 2;
  Double_t xx[N], yLow[N], yUp[N];

  for (int i = 0; i < N; i++) {
    xx[i] = 70. * i;
    yLow[i] = -xx[i] + 90.;
    yUp[i] = -xx[i] + 110.;
  }

  TGraph* grshade = new TGraph(2*N);
  for (int i = 0; i < N; i++) {
    grshade->SetPoint(i, xx[i], yUp[i]);
    grshade->SetPoint(N+i, xx[N-i-1], yLow[N-i-1]);
  }
  grshade->SetFillStyle(1001); // solid fill
  grshade->SetFillColor(18);
  grshade->Draw("f");
  
  // re-plot data points on top of signal box area
  hDATA->Draw("SAME");
  hDATA1->Draw("SAME");
  
  // show grid lines
  c1->SetGrid();
  // reduce grid lines width for .pdf
  gStyle->SetLineScalePS(1);
  
  // redraw axis ticks on top of filled area
  c1->RedrawAxis("");
  
  c1->Print("w-e-neutrals.png");
  c1->Print("w-e-neutrals.pdf");

}
