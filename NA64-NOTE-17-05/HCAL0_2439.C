{
  gROOT->Reset();
  TFile myfile("hist_e100_111.root");
  TFile myfile1("hist_calib_2439.root");

  TH1D* hMC = (TH1D*)myfile.Get("ehcal0");	//eveto
  TH1D* hDATA = (TH1D*)myfile1.Get("ehcal0");	//eveto

  c1 = new TCanvas("HCAL0_2439"," ",200,10,900,600);
  c1->SetLogy();
//  hMC->Rebin(4);
//  hDATA->Rebin(4);
  hMC->SetStats(false);
  hMC->SetTitle("");
  hDATA->SetStats(false);
  hDATA->SetTitle("");
  hMC->SetXTitle("Energy in HCAL0 [GeV]");
  hMC->SetYTitle("Events");
  hDATA->SetXTitle("Energy in HCAL0 [GeV]");
  hDATA->SetYTitle("Events");
  hMC->SetLineWidth(2);
  hDATA->SetMarkerStyle(8);
  hDATA->SetMarkerColor(2);
  hDATA->SetLineColor(2);
  hDATA->SetLineWidth(2);
  Double_t NMC = 11.*46000.*200.;
  Double_t NDATA = 171000000.;
  hDATA->Sumw2();
  hMC->Sumw2();
  hDATA->Scale(1./hDATA->Integral());
  hMC->Scale(1./hMC->Integral());
  hDATA->SetAxisRange(0., 20.0, "X"); 
  hDATA->Draw("E");
  hMC->Draw("HISTSAME");
}
