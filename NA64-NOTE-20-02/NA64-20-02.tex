\documentclass[pdftex,12pt,a4paper]{article}
%\usepackage[affil-it]{authblk}
\usepackage[margin=0.7in]{geometry}
\usepackage{multicol}
\usepackage{enumerate}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd}
\usepackage[pdftex]{graphicx}
\usepackage[font={small,it}]{caption}
\usepackage[margin=8pt]{subfig}
%\usepackage{multirow}
\usepackage{longtable}
\usepackage{float}
%\usepackage{hyperref}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand\g{\gamma}
\newcommand\ee{e^+e^-}
\newcommand\aee{X\to e^+e^-}
\newcommand\ainv{X\to invisible}
\newcommand\xdecay{X \rightarrow e^+  e^-}
\newcommand\pair{e^+ e^-}

\begin{document} 

\title{\vspace{-1.5cm}{
\begin{center}
%\resizebox{7cm}
\includegraphics[scale=0.47]{na64-logo-pc.pdf}
\end{center}
\vskip1.cm
%%%%%%%%%%%%%%%%%%  Put here the correct number for your Note %%%%%%%%%%%%%%%%%%%
\begin{flushright}
\normalsize{\bf
NA64-NOTE-20-02~~~ \\
\today}
%March 8 2017}
\end{flushright}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Put here the Title, Author list and the Abstract 
\vskip3.5cm
\Large{\bf Feasibility study of new visible mode setup for invariant mass reconstruction}}
%\author{NA64 Collaboration}
\author{E. Depero}
\date{\vspace{-5ex}}
\maketitle
\abstract{We study here a novel setup aimed to probe the parameter space of dark matter characterized by high coupling $\epsilon$. This setup needs to be able to reconstruct the invariant mass of the particle at least at a precision of 10\% in order to check if this is compatible with the explanation of the Be$^8$-anomaly observed by the Atomki collaboration in terms of a new boson of mass 16.7 MeV. The results of this preliminary analysis show that the desired precision can be achieved using simple gas detectors with hit resolution of 80 $\mu$m. A magnetic field of 1.72 T$\cdot$m is also found to be sufficient to separate the dark matter enough for it to be resolved by the ECAL in two separate electromagnetic-showers.}

\newpage

\section{Motivation}
The main motivation for this work is to propose a new NA64 setup that has the capability to measure the invariant mass of a decay happening after the WCAL dump with a precision of at least 10\%.
This is justified by the possible discovery of a new particle by the Atomki collaboration \cite{Atomki}. As the mass of the new hypothetical particle was measured to be 16.7 MeV with a precision of $\sim$5\%, this gives our collaboration the opportunity to confirm this measurement with a different setup and method. To achieve this, a longer setup is used to improve the estimate on the angle reconstruction. At the same time, a magnetic field separates the two tracks downstream in order to make the two electromagnetic-showers resolvable in the ECAL.
  
 
\section{The setup}

The setup used for this study is derived from the original setup used in the visible mode of 2018. The new setup needs to accommodate the two requirements necessary to reconstruct the invariant mass, the first being the measurements of the very small angle of the decay of an X particle exiting the WCAL in the decay volume and the second being the knowledge of the energy of the two tracks of $\pair$ generated from the decay. A sketch of the setup can be seen in Fig.\ref{fig:setup}

To reconstruct very small angles, a long distance from the decay vertex is needed. This will be shown more in detail in Sec.\ref{sec:calc}. A 9m vacuum pipe is selected to minimize multiple scattering along the beam-direction and a pair of GEM detectors is placed after the tube to measure the two outgoing tracks. The first detector was placed immediately after the vacuum tube while the second 1 m after that. The mean separation of the particles at this distance is sufficient to distinguish the two hits and reconstruct the angle using the separation of the two tracks on the detector and the position of the vertex, which is known to a good approximation immediately after the end of the WCAL.

In order to reconstruct the energy of the particles, a magnetic field is needed to separate the tracks further. A magnetic field of different lengths and strengths was simulated. A 1m magnet of 1.72 Tesla is currently being considered for the job. The 40X$_0$ length ECAL is placed 2 m after the magnetic field with the HCAL immediately after it to complete the hermeticity of the setup.

Finally, the 30X$_0$ WCAL was optimized to be as short as possible to increase the signal yield for the very short-lived dark matter that populates the parameter space of the Beryllium anomaly. This was done by modifying the structure of the WCAL to be 6 mm of W + 2 mm of Sc for every layer. This reduces the length of the WCAL while still maintaining the same radiation length (30X$_0$) at the price of a worse energy resolution.

\begin{center}
  \begin{figure}[h!]
    % \resizebox{7cm}
    \includegraphics[scale=0.45]{setup.pdf}
    \caption{New setup studied for visible mode in 2021. The magnetic field used were tested at different strength (0.5 and 1.72 Tesla) and different length (1 or 2 m).}
  \label{fig:setup}
  \end{figure}
\end{center}

\label{sec:setup}

\section{Error budget in invariant mass reconstruction}
\label{sec:calc}

To reconstruct the invariant mass of a decay one needs to measure precisely the angle of the decay pair. The angle is expected to be around 0.1 mrad for an incoming 150 GeV electron, which means the error should be in the order of a few tens of $\mu$rad to reach the required precision. Two algorithms are presented in this section. For both methods only the two trackers before the magnet are used, as any additional tracker after the magnet has low impact on the angle estimate.

\subsection{Method 1}

In the first method, we assume no knowledge of the vertex position and use the two hit information to reconstruct the angle. We define $d_{e^-}$ and $d_{e^+}$ as the distance of the hit of each of the two particles between the first and second tracker and $D_d$ is the distance between the two trackers. Using simple trigonometry one can calculate the angle of the decay and the position of the vertex (Fig.\ref{fig:geo1}). After that one can use Gaussian error propagation to estimate the error since all measurements are uncorrelated:

\[
\sigma^2_{\theta} = D_d^2 \sigma^2_d \left( \frac{1}{(D_d^2 + d^2_{e^-})^2} + \frac{1}{(D_d^2 + d^2_{e^+})^2}  \right)
\]

We can further simplify the formula by assuming the two distance to be similar on average, i.e. $d = d_{e^-} \approx d_{e^+}$ and assume that $D_d \gg d$. This is a good assumption considering that $D_d$ is typically in the order of 1 m while d is below 1 mm. This reduce the formula to the simple:

\[
\sigma_{\theta} \approx 2 \times (\sigma_{hit} / D_d)
\]

Where $\sigma_d$ was exchanged with the hit resolution  $\sigma_{hit}$ of the tracker used. Although this method already achieves a good precision, given that the distance $d$ is in the order of mm and $D_d$ is of the order of m, this sums up to a precision of approximately $\sim$0.2 mrad, which is an order of magnitude away from the requirements.

\label{sec:calc:alg1}

\subsection{Method 2}

In the second method, we assume the position of the decay to be known with some precision. In our study the position is taken as the very end of the WCAL. This is possible since for large $\epsilon$ the decay length is much lower than the dump length. This means that X-bosons within the acceptance of our setup are in good approximation all decaying immediately after the end of the dump.

In this scenario, the distance $d$ of the $\pair$ in the trackers can be used to calculate the angle of the decay. The angle becomes simply:

\[
  \theta = \arctan d / D_v
\]

Where $D_v$ in this case is the distance of the tracker from the vertex position. In principle, this method can be used with a single tracker as well.

Using again the approximation $D_v \gg d$ we have the following formula for the error:

\[
  \sigma^2_{\theta} \approx (\sigma_{hit} / D_v)^2 + (\sigma_D / D_v)^2(d / D_v)^2
\]

Where the second term describes the impact of the uncertainty in the vertex position in the equation. Since $d \sim 1$ mm and $D_v$ is 10 m, the second term is very suppressed and starts to become relevant only when the vertex position is more than 10 cm off. In most of the scenarios where $\epsilon$ is between  $10^{-3}$ and $0.5 \cdot 10^{-3}$ the second term can be safely excluded, which simplify the error calculation to:

\[
  \sigma_{\theta} \approx (\sigma_{hit} / D_v)
\]

This is very similar to the results obtained in the previous section, but this time $D_v$ is not the distance between the two trackers but instead the distance of one of the trackers to the decay vertex, which is in our setup one order of magnitude larger. This improves the estimate on the angle down to the 10\% precision required for the invariant mass determination.

\begin{center}
  \begin{figure}[h!]
    % \resizebox{7cm}
    \includegraphics[scale=0.45]{Xdecay_alg1.pdf}
    \includegraphics[scale=0.45]{Xdecay_alg2.pdf}
    \caption{Geometry of the decay of an X-boson. One can use the distance of the hits of $\pair$ in both detectors to calculate the angle without assuming any previous knowledge on the vertex (left). Alternatively, one can assume previous knowledge of the vertex and obtained an estimate of the angle using the distance of the trackers from the decay vertex and the separation of the two at the tracker position.}
  \label{fig:geo1}
  \end{figure}
\end{center}

\label{sec:calc:alg1}

\section{Data analysis}

The setup was simulated using the geometry illustrated in Sec.\ref{sec:setup}. The dark matter scenario used is $\epsilon = 10^{-3}$ and M$_X = 16.7$ MeV. All the results presented come from simulations that have approximately 1000 signal events generated from an initial 150 GeV e$^-$. All hits in the GEM which deposited energy above 1 eV are recorded and later digitized in the reconstruction. For the digitization, hits that are closer than a certain threshold value are merged and a single hit with the averaged position is saved instead. Furthermore, hits are artificially smeared using a Gaussian with sigma equal to the hit resolution of the detectors. The process of digitization is described in detail in \cite{na64:note:19-03}. All values presented in this section were weighted with the probability of dark matter to decay within the acceptance of the setup. This means that more energetic events weight more on the histogram because of the boost received to the decay length. Finally, any dark matter produced with energy lower than 100 GeV was suppressed in the simulation. This is because the weight for such events is more than one order of magnitude lower than the ones with 150 GeV energy and thus bring a minimal correction to any of the distributions considered. 

The angle was then reconstructed using both algorithms described in the previous section. A full reconstruction of the track using a Kalman filter and the information from all 4 detectors was also used to estimate the angle. The results of this method are equivalent to the simple one illustrated in sec.\ref{sec:calc:alg1} as shown in Fig.\ref{fig:kalmanvssimple}. This is expected as a Kalman reconstruction also does not assume any previous knowledge on the vertex position and the additional information of the two detectors after the magnet is not enough to add any significant correction.

\begin{center}
  \begin{figure}[h!]
    % \resizebox{7cm}
    \includegraphics[width=\textwidth,height=.5\linewidth]{kalmanvssimple.png}
    \caption{Angle reconstructed for a Dark boson with mass M$_X$=16.7 MeV and $\epsilon=10^{-3}$. In red angle reconstructed using a simple line extrapolation with only the two trackers after the magnets is shown. In black angle reconstructed using a Kalman filter with all four detector is shown.}
  \label{fig:kalmanvssimple}
  \end{figure}  
\end{center}

First of all, the distance of the simulated track was checked in each of the detectors used in the simulation. The average distance of the $\pair$ was found to be sufficient for the geometry used, with an average distance of 1.6 mm in the first tracker and 99\% of the event within the resolvable distance of the two trackers. On the other hand the distance at the ECAL is larger than 10 cm using an integrated field of 1.72 T$\cdot$m (Fig.\ref{fig:dmdistance}). This is sufficient to separate the two showers, since the distance exceed two ECAL cells. An integrated field of 0.5 T$\cdot$ was tested as well but found not sufficient in the given setup, as the average distance between the shower was of only one cell ($\sim$4 cm).

\begin{center}
  \begin{figure}[h!]
    % \resizebox{7cm}
    \includegraphics[width=.5\textwidth,height=.3\linewidth]{gem1_dist.png}
    \includegraphics[width=.5\textwidth,height=.3\linewidth]{gem3_dist.png}
    \includegraphics[width=.5\textwidth,height=.3\linewidth]{ecal_dist.png}
  \caption{Distance of the dark matter in the first GEM (top left), third GEM (top right) and ECAL (bottom right) for an integrated magnetic field of 1.72 T$\cdot$m placed 1 m after the first tracker and 2 m before the ECAL.}
  \label{fig:dmdistance}
  \end{figure}  
\end{center}

Currently to calculate the invariant mass an error of 0.1 / $\sqrt{p [GeV]}$ was artificially added to the energy of the decay product. This is justified from the measured energy resolution of the ECAL that would allow such a precision once the two electromagnetic showers are resolved.

The results for the invariant mass reconstruction are shown in Fig.\ref{fig:invmass}. For hit resolutions of 80 $\mu$m and 10 $\mu$m to show the potential performance of both gas and silicon-based detectors. The invariant mass is fitted using two Gaussian distributions with the same mean but have different spread and normalization. The parameter labeled as SigmaCenter shows the sigma of the Gaussian used to estimate the core of the distribution and measures therefore the precision of the reconstructed m$_{\pair}$. As shown in sec.\ref{sec:calc} the precision improves by roughly one order of magnitude using the second algorithm, reaching a $\Delta m_{\pair} / m_{\pair}$ of 4\% for gas detector and 1\% in case of hit resolution comparable to pixel sensors. Fig.\ref{fig:invmass:overview} gives an overview of the efficiency as a function of the cut applied on the invariant mass. The conclusion is that hit resolution has low impact when the invariant mass is reconstructed using a fixed vertex position, whereas for a full reconstruction of the vertex position the efficiency drops of almost 30\% if a gas detector is used instead a pixel sensor.

\begin{center}
  \begin{figure}[h!]
    % \resizebox{7cm}
    \includegraphics[width=.5\textwidth,height=.3\linewidth]{invmass_alg1_80mum.png}
    \includegraphics[width=.5\textwidth,height=.3\linewidth]{invmass_alg1_10mum.png}
    \includegraphics[width=.5\textwidth,height=.3\linewidth]{invmass_alg2_80mum.png}
    \includegraphics[width=.5\textwidth,height=.3\linewidth]{invmass_alg2_10mum.png}
  \caption{Invariant mass reconstructed for Dark matter of M$_X$=16.7 MeV and $\epsilon$=10$^{-3}$. First line show the reconstruction assuming no knowledge of the exact decay vertex, which is reconstructed together with the angle. Second line show the reconstruction assuming the vertex of the decay to be immediately after the WCAL. First row assume a hit resolution of 80$\mu$m for the GEMs. The second row assume a resolution of 10$\mu$m.}
  \label{fig:invmass}
  \end{figure}  
\end{center}

\begin{center}
  \begin{figure}[h!]
    % \resizebox{7cm}
    \includegraphics[width=.5\textwidth,height=.3\linewidth]{invmass_alg1_overview.png}
    \includegraphics[width=.5\textwidth,height=.3\linewidth]{invmass_alg2_overview.png}
  \caption{Percentage of events inside the area with invariant mass reconstructed with the desired precision. Left plot show invariant mass obtained from a full vertex reconstruction. Plot on the right show invariant mass reconstructed assuming the vertex to be at the end of the WCAL. The curves are drawn for three different hit resolution of the trackers: 10$\mu$m(black), 50$\mu$m(blue) and 80$\mu$m(red).}
  \label{fig:invmass:overview}
  \end{figure}
\end{center}



\label{sec:data-analysis}

\newpage

\section{Conclusion}  

In conclusion, the setup of the visible mode presented in this study has the capability to measure the invariant mass of an hypothetical decay with a precision of $\sim$5\% using standard gas detector. This value can be further improved using pixel sensor down to a precision of 1\%. Using a distance of 9m from the dump, the separation predicted at tracker distance is above 1 mm for all event topology. This means that the detector already available in NA64 are capable of separating the two tracks coming from dark matter decay with an efficiency $>$90\%. To separate the two tracks at the ECAL, a distance of at least two cell is required ($\sim$76mm). To achieve this the tracks an MBPL magnet with an integrated field of 1.72 T$\cdot$m was found sufficient. Since the total radiation length of the dump is equal to the previous design, the background is not expected to increase compared to the one calculated for the previous setup. On the contrary the longer spacing between WCAL and ECAL is expected to suppress more efficiently the background coming from leaking neutrals.

To improve this study, the following items can be addressed:

\begin{itemize}
\item A study of the efficiency and hit resolution as a function of the distance between the particles could be performed using already available data to better understand the effect that this would have on the efficiency and the invariant mass reconstruction.
\item A complete reconstruction of the particle momentum using both tracking and ECAL needs to be implemented realistically in the simulation.
\item A new large simulation of the setup should be performed for a novel study of the background.
\end{itemize}

\vskip1.5cm
{\large \bf Acknowledgements}\\

I would like to thank Paolo Crivelli and Sergey Gninenko for the many useful discussion that guided me in this analysis.



\input NOTE-20-02-MK/NOTE-20-02-MK.tex

\newpage


\begin{thebibliography}{99}

\bibitem{Atomki}
  A.J. Krasznahorkay at al., New evidence supporting the existence of the hypothetic X17 particle, arXiV:1910.10459v1 [nucl-ex]

  \bibitem{na64:note:19-03}
E. Depero, Study of $\gamma + Z \rightarrow \mu^+\mu^-$ interactions using trackers,https://gitlab.cern.ch/P348/na64-papers/blob/master/NA64-NOTE-19-03/TrackingNote.pdf (2019)

\bibitem{feng1} J. Feng 
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo, 
Phys. Rev. Lett. 117, 071803 (2016) 
\bibitem{feng2}
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo,
 Phys. Rev. D 95, 035017 (2017)
\bibitem{DarkPhotons}
S.N. Gninenko, N.V. Krasnikov, M.M. Kirsanov, D.V. Kirpichnikov, Missing energy signature from
invisible decays of dark photons at the CERN SPS, Phys. Rev. D 94, 095025 (2016), arXiV:1604.08432 [hep-ph]


\end{thebibliography}

\end{document}  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
