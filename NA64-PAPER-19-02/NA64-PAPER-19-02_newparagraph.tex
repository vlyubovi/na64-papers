% ****** Start of file apssamp.tex ******
%
%   This file is part of the APS files in the REVTeX 4 distribution.
%   Version 4.0 of REVTeX, August 2001
%
%   Copyright (c) 2001 The American Physical Society.
%
%   See the REVTeX 4 README file for restrictions and more information.
%
% TeX'ing this file requires that you have AMS-LaTeX 2.0 installed
% as well as the rest of the prerequisites for REVTeX 4.0
%
% See the REVTeX 4 README file
% It also requires running BibTeX. The commands are as follows:
%
%  1)  latex apssamp.tex
%  2)  bibtex apssamp
%  3)  latex apssamp.tex
%  4)  latex apssamp.tex
%
% Different \documentclass : diffeent style for front page, one- or two column, distance between lines, ec.. 
\RequirePackage{lineno} 
%\documentclass[prd,twocolumn,showpacs,groupedaddress,superscriptaddress,amsmath,amssymb]{revtex4}
\documentclass[prd,onecolumn,groupedaddress,superscriptaddress,amsmath,amssymb]{revtex4}
%\documentclass[twocolumn,showpacs,preprintnumbers,amsmath,amssymb]{revtex4} <- authors by insitutes
%\documentclass[preprint,showpacs,preprintnumbers,amsmath,amssymb]{revtex4}
% Some other (several out of many) possibilities
%\documentclass[preprint,aps]{revtex4}
%\documentclass[preprint,aps,draft]{revtex4}
%\documentclass[prb]{revtex4}% Physical Review B


\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{lineno}
%\linenumbers

\newcommand\g{\gamma}
\newcommand\ee{e^+e^-}
\newcommand\pp{\pi^+ \pi^-}
\newcommand\mm{\mu^+ \mu^-}
\newcommand\xee{X \to e^+e^-}
\newcommand\aee{A' \to e^+e^-}
\newcommand\xdecay{X \rightarrow e^+  e^-}
\newcommand\ainv{A'\to invisible}
\newcommand\gp{ A'}
\newcommand\ma{m_{A'}}
\newcommand\pair{e^+e^-}
\newcommand\na{{n}_{A'}}
\newcommand\Na{{N}_{A'}}
\newcommand\ea{e^- Z \to e^- Z A'; A' \to invisible}
\newcommand\emu{e^- Z \to e^- Z \gamma; \gamma \to \mu^+ \mu^-}
\newcommand\dimu{\mu^+ \mu^-}
\newcommand\kosdec{K^0_S \to \pi^0 \pi^0; \pi^0 \to \g \ee}
\newcommand\kospio{K^0_S \to \pi^0 \pi^0}
\newcommand\kospi{K^0_S \to \pi^+ \pi^-}
\newcommand\ks{K^0_S }
\newcommand\kl{K^0_L }


\def\address{\@ifstar{\address@star}%
  {\@ifnextchar[{\address@optarg}{\address@noptarg}}}

\begin{document}


%\setpagewiselinenumbers
%\modulolinenumbers[1]-- should be on for numbering the lines 
%\linenumbers -- should be on for numbering the lines

%For CERN Preprint
%\begin{center}
%{\Large EUROPEAN ORGANIZATION FOR NUCLEAR RESEARCH}
%\end{center}
%\vskip1.5cm
%\begin{flushright}
%CERN-EP-2019-284\\
%\today
%December 16, 2019
%\end{flushright}


%\title{More sensitive search for a hypothetical $X(16.7)$ boson and a dark photon decaying into $\pair$ pairs in the NA64 experiment at the CERN SPS}
\title{Improved limits on a hypothetical $X(16.7)$ boson and a dark photon decaying into $\pair$ pairs}


\input ../AUTHORS/author_list.tex

%\date{\today}% It is always \today, today,
             %  but any date may be explicitly specified
%\date{June 17, 2009}% It is always \today, today,
             %  but any date may be explicitly specified

\begin{abstract}
  The improved results on a direct search for a new $X$(16.7 MeV) boson that could explain the anomalous excess of $\ee$ pairs observed
in the decays of the excited $^8$Be$^*$ nucleus ("Berillium anomaly") are reported. The $X$ boson could be produced
in the bremsstrahlung reaction $e^- Z \to e^- Z  X$ by a high energy beam of electrons incident on the active target in the NA64 experiment
at the CERN SPS and observed through its subsequent decay into $\ee$ pair. No evidence for such decays was found from the combined analysis
of the data samples with total statistics corresponding to $8.4\times 10^{10}$ electrons on target collected in 2017 and 2018.
This allows to set the new limits on the $X-e^-$ coupling in the range $1.2 \times 10^{-4}\lesssim \epsilon_e \lesssim 6.8 \times 10^{-4}$,
excluding part of the parameter space favored by the Berillium anomaly.
The non-observation of the decay $\aee$ allows also to set the new bounds on the mixing strength of photons with dark photons ($A'$)
with a mass $\lesssim 24$ MeV.
\end{abstract}

%\pacs{14.80.-j, 12.60.-i, 13.20.-v, 13.35.Hb}% PACS, the Physics and Astronomy
                             % Classification Scheme.
%\keywords{Suggested keywords}%Use showkeys class option if keyword
                              %display desired
\maketitle

 Recently, the search for new light bosons weakly coupled to SM particles was additionally inspired by the observation in the ATOMKI
experiment by Krasznahorkay et al. \cite{be8,be8-1} of a $\sim$7$\sigma$ excess of events in the invariant mass distribution
of $\pair$ pairs produced in the nuclear transitions of the excited $^8$Be$^*$ to its ground state via internal pair creation.
It was shown that this anomaly can be interpreted as the emission of a protophobic
gauge boson $X$ with a mass of 16.7 MeV decaying into $\pair$ pair \cite{feng1,feng2}.
This explanation of the anomaly was found to be consistent with the existing constraints assuming that the $X$ has
non-universal coupling to quarks, coupling to electrons in the range $2\times 10^{-4} \lesssim \epsilon_e \lesssim 1.4\times 10^{-3}$
and lifetime $10^{-14}\lesssim \tau_X \lesssim 10^{-12}$~s. It is interesting that a new boson with such relatively large
couplings to charged leptons could also resolve the so-called ($g_\mu - 2$ ) anomaly, a discrepancy between measured and predicted
values of the muon anomalous magnetic moment. This has motivated worldwide efforts towards the experimental searches,
see, e.g., Refs.~\cite{mb, nardi}, and studies of the phenomenological aspects of light vector bosons weakly coupled to quarks and leptons,
see, e.g., Refs.~\cite{jk, cheng, Zhang:2017zap, ia, liang, bart} and also earlier works of Refs.~\cite{fayet1, fayet2, fayet3, fayet4}.
The latest experimental results from the ATOMKI group show a similar excess of events at approximately the same invariant mass
in the nuclear transitions of another nucleus, $^4$He \cite{be8-2}. This further increases the importance of independent searches for
a new particle $X$.
 \par Another strong motivation to search for new light bosons decaying into $\ee$ pair comes from the
dark matter puzzle. An interesting possibility is that in addition to gravity a new force between the dark sector and
visible matter, carried by a new vector boson $A'$, called dark photon, might exist \cite{prw, pospelov}. Such $A'$ could have 
a mass $m_{A'}\lesssim 1$ GeV, associated with a spontaneously broken gauge $U(1)_D$ symmetry, and would couple to 
the Standard Model (SM) through the kinetic mixing with ordinary photon, $-\frac{1}{2}\epsilon F_{\mu\nu}A'^{\mu\nu}$, parameterized
by the mixing strength  $\epsilon \ll 1$ \cite{Okun:1982xi, Galison:1983pa, Holdom:1985ag}, for a review see, e.g., Refs.~\cite{mb, jr, report}. 

\begin{figure*}[tbh!!]
\centering
\includegraphics[width=.9\textwidth]{setup_2018_vis.pdf}
\caption{The setup to search for $A',X\to \ee$  decays of the bremsstrahlung $A',X$ produced in the reaction
$eZ \to eZA'(X) $ of the 150 GeV electrons incident on the active WCAL target.}
\label{setup}
\end{figure*}

 A number of previous experiments, such as beam dump \cite{jdb, charm, rio, e137, konaka, bross, dav,  ath, nomad, e787, essig1, blum,
sg1, blum1, sarah1}, fixed target \cite{apex,merkel,merkel1}, collider \cite{babar, curt, babar1} and rare particle decay searches
\cite{bern, sindrum, kloe, sg2, kloe2, wasa, hades, phenix, e949, na48, pol, kloe3}, put stringent
constraints on the $\epsilon$ and mass $m_{A'}$ of such dark photons, excluding, in particular,
the parameter space region favored by the $g_\mu-2$ anomaly. However, a large range of mixing strengths 
$10^{-4} \lesssim \epsilon \lesssim 10^{-3}$ corresponding to short-lived $A'$ remains unexplored. These values of $\epsilon$
can be obtained from the loop effects of particles charged under both the dark and SM $U(1)$ interactions.
Typically 1-loop value is $\epsilon = e g_D/16\pi^2$ \cite{Holdom:1985ag}, where $g_D$ is the coupling constant of the $U(1)_D$
gauge interactions. The search for $\ee$ decays of new short-lived particles at the CERN SPS was performed by the NA64 experiment
in 2017 \cite{NA64Be2017}. We report here the improved results from the NA64 experiment obtained using the data collected
in 2018 in the new run at the CERN SPS performed after optimization of the experiment configuration and parameters.

 \par The NA64 experiment employs the optimized electron beam from the H4 beam line of the CERN SPS.
The beam delivers $\simeq 5\times 10^6~e^-$ per SPS spill of 4.8 s produced by the primary 400 GeV proton beam with an intensity
of a few 10$^{12}$ protons on target. The NA64 setup designed for the searches of $X$ bosons and $A'$ is schematically
shown in Fig.~\ref{setup}. The thin scintillation counters, $S_1$ - $S_3$ and $V_0$, are used for the beam definition, while another one,
$S_4$, is used to detect the $\pair$ pairs. The detector is equipped with a magnetic spectrometer consisting of two MBPL magnets
and a tracker with low material budget. The tracker is a set of four upstream Micromegas (MM) chambers for the incoming
$e^-$ angle selection, four GEM chambers and three straw tube planes allowing the reconstruction of the outgoing tracks \cite{Banerjee:2015eno,track}.
To enhance the electron identification the synchrotron radiation (SR) emitted by electrons is used for their efficient
tagging and for additional suppression of the initial hadron contamination in the beam $\pi/e^- \simeq 10^{-2}$ down to the level
$\simeq 10^{-6}$ \cite{srd,na64-prd}. The use of SR detectors (SRD) is important for the hadron background suppression and the corresponding
improvement of the sensitivity as compared to the previous electron beam dump searches \cite{konaka,bross}.
 The dump is an electromagnetic (EM) calorimeter WCAL made as compact as possible to maximize the sensitivity to short
lifetimes while keeping the leakage of particles at a small level. The purpose of the WCAL design was to absorb {\it not the full energy}
of the shower generated by the primary electrons, but the energy of the showers produced by secondary particles and the recoil electrons
from the primary reaction (1), which is typically significantly lower. The WCAL is assembled from the tungsten and plastic scintillator
plates with wave length shifting fiber read-out. The first five layers of the WCAL are separated from the main part (WCAL preshower). Immediately
after the WCAL there are the veto counters $W_2$ and $V_2$, several meters downstream the decay counter $S_4$ and tracking detectors.
These detectors are followed by another EM calorimeter (ECAL), which is a matrix of $6\times 6$ shashlik-type lead - plastic scintillator sandwich
modules \cite{na64-prd}. The ECAL is 40 radiation lengths (X0) with the first 4 X0 serving as a preshower subdetector.
Downstream the ECAL the detector is equipped with a high-efficiency counter VETO and a thick hadron calorimeter (HCAL) \cite{na64-prd}
used as a hadron veto and muon identificator.

 The events are collected with a hardware trigger requiring in-time energy deposition in $S_1$ - $S_3$, no energy deposition in $V_0$ and
$E_{WCAL} \lesssim 0.7 \times E_{beam}$. The latter requirement was not used in the runs used for calibration (calibration beams).

 In order to increase the sensitivity to short-lived X bosons (higher $\epsilon$) the following optimization steps
were performed for the 2018 run: (i) Beam energy increased to 150 GeV; (ii) Thinner counter $W_2$ was installed
immediately after the last tungsten plate inside the WCAL box; (iii) more track detectors installed between WCAL and ECAL.
In addition, the vacuum pipe was installed immediately after the WCAL, and the distance between the WCAL and ECAL was increased.
These changes would allow to perform the full track and vertex reconstruction if the $\ee$ pair energy is not very high
as immediate additional checks in case of signal observation.

 To choose selection criteria, for the calculation of efficiencies and for background estimation the package based on
Geant4 \cite{Geant4-2002, Geant4-2006} for the detailed full simulation of the experiment is developed. It contains the subpackage
for the simulation of various types of dark matter particles based on the exact tree-level calculation of cross sections \cite{DMsimulation}.

 \par The method of the search for $\aee$ (or $\xdecay$) decays is described in \cite{Gninenko:2013rka, Andreas:2013lya, gkkk1, DMsimulation}.
The $A'$ can be produced via the coupling to electrons in the scattering of high-energy electrons off nuclei of the
active WCAL target-dump. Its production is followed by the decay into $\ee$ pairs:

\begin{equation}
e^- + Z \to e^- + Z + A'(X)   ;~ A'(X)\to \ee \,.
\label{ea}
\end{equation}
 
The WCAL serves as a dump to absorb the EM showers from the secondary particles emitted by the primary electrons before the
$A'$ production, with total energy $E_s$, that carry the fraction $s$ of the primary electron energy, $s = E_s/E_0$, and the
shower from the recoil electron of the reaction \eqref{ea}. The latter carries a fraction $f$ of the production electron energy.
The total energy that remains in the WCAL and is to be absorbed is $E_{WCAL} = E_0 (s + f(1-s))$. As shown in \cite{gkkk1, DMsimulation},
the value of $f$ is peaked at zero for the most interesting masses of $A'$.
In the detectable signal events $A'$ would penetrate the rest of the dump and the veto counter without interactions and then decay in flight
into an $\ee$ pair in the decay volume. The fraction $E_{ECAL} = E_0 (1-s)(1-f)$ of the primary electron energy is deposited
in the second downstream calorimeter ECAL, as shown in Fig.~\ref{setup}. In the most interesting region of the parameter space
the probability to decay after the dump significantly drops for low energy $A'$ because of short lifetime and small gamma-factor. For this
reason the detectable signal events typically have small values of $s$, thus the values of $E_{WCAL}$, as mentioned above, are typically
significantly smaller than $E_0$.
  
 The occurrence of $A'(X)$ produced in the $e^- Z $ interactions and $\aee$ decays would appear as an excess of events with two EM-like showers
in the setup: one shower in the WCAL and another one in the ECAL, with the total energy $E_{tot} = E_{WCAL} +E_{ECAL}$
compatible with the beam energy ($E_0$), above those expected from the background sources.

 \par The candidate events were selected with the following criteria:
(i) Small energy in the veto counter ($W_2$ in 2018), well below one $MIP$ (most probable energy deposition
of a minimum ionizing particle). The concrete cut was slightly different for different periods, it was optimized taking into account
the energy resolution, the electronic noise and the pileup effects in the counter;
(ii) The signal in the decay counter $S_4$ is consistent with two $MIP$s;
(iii) The sum of energies deposited in the WCAL+ECAL is equal to the beam energy within the boundaries determined by the energy
resolution of these detectors. At least 25 GeV should be deposited in the ECAL;
(iv) The shower in the WCAL should start to develop within a few first $X_0$, which is ensured by the WCAL preshower energy cut;
(v) The cell with maximal energy deposition in the ECAL should be (3,3): the cell on the axis of the beam bent by the magnets;
(vi) The longitudinal and lateral shape of the shower in the ECAL are consistent with a single EM one. The longitudinal shape
is checked by the cut on the energy deposition in the ECAL preshower. Checking the lateral shower shape
does not decrease the efficiency for signal events because the distance between $e^-$ and $e^+$ in the ECAL is significantly smaller
than the ECAL cell size. Finally, the rejection of events with hadrons in the final state was based on the energy deposition in the
VETO and HCAL.

 As in the previous analyses \cite{na64-prl, na64-prd}, in order to check efficiencies and the reliability of the MC simulations,
we selected a clean sample of $\simeq 10^5$ $\mu^+ \mu^-$ events with $E_{WCAL} < 0.6 \times E_{beam}$ from the QED muon pair
production in the dump (dimuons). This rare process is dominated by the reaction $e^- Z \to e^- Z \gamma; \gamma \to \mu^+ \mu^-$
of a photon conversion into muon pair on a dump nucleus. We performed a number of comparisons between these
events and the corresponding MC simulated sample and applied the estimated efficiency corrections to the MC events.

 The counter $W_2$ is very important for this analysis. It is made using the same technology as for the tiles of the WCAL and
installed inside the WCAL box to be as close to the possible place of $A'$ creation as possible. We paid special attention
to check that it works correctly and to make the MC simulation of this counter as close to the real data as possible.
In the simulation we took into account the following effects:
\begin{itemize}
\item fluctuations of the number of photoelectrons from the photocathode
\item pulse reconstruction threshold curve for the counter below 0.8 MIP
\item small cross-talk between the WCAL and W2 signals
\item uncertainties of the $W_2$ pulse reconstruction due to readout electronic noise and pileup effects
\end{itemize} 
The cross-talk between the neighboring WCAL and W2 signals include contributions from the light cross-talk and the electronic
cross-talk between the two channels. The average cross-talk value was assumed to be proportional to the energy deposition in the WCAL.

in Fig.~\ref{W2checks} the comparison of the MC simulation with data for selected muons in the hadron beam and for the electron beam
with several different selections is shown. There is some remaining disagreement for the electron calibration beam and for dimuons.
However, the agreement for dimuons becomes better for smaller energy in the WCAL, i.e. for the conditions that we would have in
signal events. For reliability we also estimated the systematic errors of the signal efficiency due to $W_2$ by changing the $W_2$
threshold (30\% up and down) and comparing the signal efficiencies. The systematic error calculated this way is 10\%. It was used
in the final statistical analysis together with other systematic errors.

 The energy deposition in $W_2$ expected for the detectable signal events (with $A'$ decays after the last tungsten plate) is shown
in Fig.~\ref{W2signal}. It is significantly smaller than for the electrons from the primary beam (Fig.~\ref{W2checks} lower left
plot). It is also smaller for the bigger value of $\epsilon$ since the short-lived $A'$ should have higher energy for the same
probability to decay after the WCAL tungsten plates, which means smaller energy of the recoil electron (shorter shower).

\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{WCAL2_checks.pdf}
\caption{The comparison of various distributions in the $W_2$ counter with MC predictions. Left upper plot: for muons
selected in the hadron calibration beam. Right upper plot: for dimuon events with the standard cut $E_{WCAL} < 0.6 \times E_{beam}$.
Right lower plot: for dimuon events with the cut $E_{WCAL} < 0.33 \times E_{beam}$. Left lower plot: for the electron
calibration beam.}
\label{W2checks}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{WCAL2_signal.pdf}
\caption{The expected energy in the $W_2$ counter from the signal events. Blue histogram: $\epsilon = 0.0003$.
Red histogram: $\epsilon = 0.0006$. The events are histogrammed with the weight corresponding to the probability for
the $A'$ to decay after the last tungsten plate of the WCAL.}
\label{W2signal}
\end{figure}

%{\bf BACKGROUND}

 The main background in this search comes from the $\kospio$ events from $K^0$ mainly produced by hadrons misidentified
as electrons \cite{NA64Be2017}. $K_0$ can pass the veto counters without energy deposition and decay into $\pi^0 \pi^0$. These
$\pi^0$ decay immediately into photons that can convert on the setup material into $\ee$ pair upstream of the $S_4$. The decay
chain $\kosdec$ is also possible.
We estimated this background using both simulation and data. For this, we selected the sample of neutral
events changing the cut (ii) to $E_{S_4}<0.5MIP$. This sample has 3 events in the 2017 data. No events were found with standard
criteria in the 2018 data, for this reason we relaxed for this sample the criteria iii) and vi). The distribution of neutral
events is shown in Fig.~\ref{w-e}.
The MC sample of $K^0_S$ was simulated according to distributions predicted for the hadron interactions in WCAL. With this sample
we calculated the number of neutral and signal-like events passing the criteria. This gives us the prediction of the number of background
events: 0.06 for the 2017 data and 0.005 for the 2018 data (Table~\ref{tab:BG}). The smaller number of neutral events and lower background
in the 2018 data are expected, because due to the increased distance between the WCAL and ECAL less $K^0_S$ events pass the criteria (v) and (vi).
In addition, the background is decreased due the vacuum pipe installed upstream of the $S_4$.

\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{w-e-neutrals.pdf}
\caption{Distribution of selected EM neutral events in the $(E_{WCAL},E_{ECAL})$ plane from the 2018 data.
Neutral events are shown as blue squares. The shadowed band represents the signal box region.}
\label{w-e}
\end{figure}

  The charge-exchange reaction $\pi^- p \to (\geq 1) \pi^0 + n + ...$ that can occur in the last layers of the WCAL, with decay
photons escaping the dump without interactions, accompanied by undetected secondaries, is another source of fake signal.
To evaluate this background we used the extrapolation of the charge-exchange cross sections, $\sigma \sim Z^{2/3}$, measured on
different nuclei \cite{vnb}. The beam pion flux suppression by the SRD tagging is taken into account in the estimation.
The background from the punchthrough $\pi^-$ can appear because of small inefficiency of the veto counter, mainly due to pile-up. It was estimated
using simulation and the data from the calibration runs with a hadron beam. The contribution from the beam kaon decays
in-flight $K^-  \to e^- \nu \pp (K_{e4})$ was estimated from the simulation with biased lifetime and found to be negligible.
The background from the dimuon production in the dump $e^- Z \to e^- Z \mu^+ \mu^-$ with either $\pp$ or $\mu^+ \mu^-$ pairs misidentified
as EM event in the ECAL was also found to be negligible.


\begin{table}[tbh!]
\begin{center}
\caption{Expected numbers of background events in the signal box that passed the selection criteria (i)-(vi)} \label{tab:BG}
\vspace{0.15cm}
\begin{tabular}{|c|c|c|}
\hline
\hline
Source of background                                                              & 2017 data          & 2018 data         \\
\hline
$K^0_S\to 2\pi^0$                                                                 & $0.06\pm0.034$     & $0.005\pm0.003$   \\
$\pi N\to (\geq 1) \pi^0 +n+...$                                                  & $0.01\pm0.004$     & $0.001\pm0.0004$  \\
punchthrough $\pi^-$                                                              & $0.0015\pm0.0008$  & $0.0007\pm0.0004$ \\
punchthrough $\g$                                                                 & $<0.001$           & $<0.0005$         \\
$\pi, K \to e \nu$, $K_{e4}$ decays                                               & \multicolumn{2}{c}{$ < 0.001$}         \vline \\
$eZ\to eZ \mm; \mu^\pm \to e^\pm \nu \bar\nu$                                         & \multicolumn{2}{c}{$ < 0.001$}         \vline \\
\hline
Total                                                                             & $0.07\pm 0.035$    & $0.006 \pm 0.003$ \\
\hline
\hline
\end{tabular}
\end{center}
\end{table}


\begin{figure}[tbh!!] 
\begin{center}
\includegraphics[width=0.5\textwidth]{new-excl-Be.pdf}
\caption{The  90\% C.L.\ exclusion areas  in the ($m_{X}; \epsilon$) plane from the NA64 experiment (blue area). For the mass of 16.7~MeV, 
 the  $X-e^-$ coupling region excluded by NA64  is  $1.2\times 10^{-4}< \epsilon_e < 6.8~\times~10^{-4}$. The full allowed range of $\epsilon_e$ 
 explaining the $^8$Be* anomaly, $2.0\times 10^{-4} \lesssim \epsilon_e \lesssim 1.4 \times 10^{-3}$ \cite{feng1, feng2}, is also shown (red area).
 The  constraints  on the mixing $\epsilon$ from the experiments E774~\cite{bross}, E141~\cite{rio}, BaBar ~\cite{babar1}, KLOE~\cite{kloe2},
 HADES~\cite{hades}, PHENIX~\cite{phenix}, NA48~\cite{na48}, and bounds from the electron anomalous magnetic moment $(g-2)_e$ \cite{hd} are also shown.}
 \label{exclvis}
\end{center}
\end{figure}

 \par Table~\ref{tab:BG} summarizes the estimated background inside the signal box. The main part of the total background uncertainty
comes from the statistical error of the number of observed EM neutral events.
There is also the uncertainty from the cross sections of the $\pi,K$ charge-exchange reactions on heavy nuclei (30\%).

 \par After determining and optimizing the selection criteria and estimating the background levels, we examined the signal box
and found no candidates.

 \par The combined 90\% confidence level (C.L.) upper limits for the mixing strength $\epsilon$ were determined from the 90\% C.L.
upper limit for the expected number of signal events, $N_{A'}^{90\%}$ by using the modified frequentist approach for confidence levels
(C.L.), taking the profile likelihood as a test statistic in the asymptotic approximation \cite{junk,limit,Read:2002hq}. The total
number of expected signal events in the signal box was the sum of expected events from the 2017 and 2018 runs:

\begin{equation}
\Na = \sum_{i=1}^{2} N_{A'}^i = \sum_{i=1}^{2} n_{EOT}^i  P_{tot}^i n_{A'}^i(\epsilon,\ma),
\label{nev}
\end{equation}

\noindent where $n_{EOT}^i$ is the effective number of $EOT$ in run-$i$ ($5.4\times 10^{10}$ and $3\times 10^{10}$), $P_{tot}^i$ is
the signal efficiency in the run $i$, and  $n_{A'}^i(\epsilon,\ma)$ is the number of the $\aee$
decays in the decay volume with energy $E_{A'} > 25$ GeV per EOT, calculated under assumption that this decay mode is predominant,
see, e.g., Eq.(3.7) in Ref. \cite{Andreas:2013lya}.
The value $n_{EOT}^i$ takes into account the data acquisition system (DAQ) dead time.
Each $i$-th entry in this sum was calculated by simulating signal events for the corresponding beam running conditions and processing
them through the reconstruction program with the same selection criteria and efficiency corrections as for the data sample from the run-$i$.
In the overall signal efficiency for each run the acceptance loss due to pileup in the veto detectors was taken into account

The $A'$ yield from the dump was calculated as described in Ref.\cite{DMsimulation}. These calculations were cross-checked with the
calculations of Ref.\cite{Liu:2016mqv, Liu:2017htz}.
The $\lesssim 10\%$ difference between the two calculations was accounted for as a systematic uncertainty in $n_{A'}(\epsilon, \ma)$.
The total systematic uncertainty on $N_{A'}$ calculated by combining all uncertainties did not exeed $\simeq 25\%$ for all runs.
The combined 90\% C.L. exclusion limits on the mixing strength $\epsilon$ as a function of the $A'$ mass is shown in Fig.~\ref{exclvis} together 
with the constraints from other experiments. Our results exclude $X$-boson as an explanation for the $^8$Be* anomaly
for the $X-e^-$ coupling $\epsilon_e \lesssim 6.8 \times 10^{-4}$ and mass value of 16.7 MeV, leaving some unexplored region at this
mass as an interesting prospect for further searches.

% We plan is to continue to explore the remaining short-lived parameter space with larger X-e couplings shown in Fig.5. For the complete
%coverage of this area a new shorter WCAL is required. This can be reached by increasing the WCAL absorber plate thickness. Our experience
%shows that the background rejection power will not significantly drop due to the corresponding increase of the sampling fluctuations.
%We study also other possibilities of improving the experimental setup performance.

We gratefully acknowledge the support of the CERN management and staff and the technical staffs of the participating institutions
for their vital contributions. This work was supported by the HISKP, University of Bonn (Germany), JINR (Dubna), MON and RAS (Russia),
ETH Zurich and SNSF Grant No. 169133 (Switzerland), and grants FONDECYT 1191103, 1190845, and 3170852, UTFSM PI M 18 13 and Basal FB0821 CONICYT (Chile).


\begin{thebibliography}{99}

\bibitem{be8}
A. Krasznahorkay et al., Phys. Rev. Lett. {\bf 116}, 042501 (2016).
%A. Krasznahorkay et al., Observation of Anomalous Internal Pair Creation in 8Be: A Possible Indication of a Light, Neutral Boson, Phys. Rev. Lett. 116, 042501 (2016).

\bibitem{be8-1}
A. J. Krasznahorkay et al., J. Phys. Conf. Ser. {\bf 1056}, 012028 (2018).

\bibitem{feng1}
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo,  Phys. Rev. Lett. {\bf 117}, 071803 (2016). 

\bibitem{feng2}
J. L. Feng, B. Fornal, I. Galon, S. Gardner, J. Smolinsky, T. M. P. Tait, Ph. Tanedo,  Phys. Rev. D {\bf 95}, 035017 (2017).

\bibitem{mb}
M. Battaglieri  et al.., US Cosmic Visions: New Ideas in Dark Matter 2017: Community Report, arXiv:1707.04591.

\bibitem{nardi}
E. Nardi, C. D.R. Carvajal, A. Ghoshal, D. Meloni, M. Raggi, Phys.\ Rev.\ D {\bf 97}, 095004 (2018).
%arXiv:1802.04756.

\bibitem{jk} J. Kozaczuk, Phys. Rev. D {\bf 97}, 015014 (2018).

\bibitem{cheng} 
Ch.-W. Chiang and   P.-Y.Tseng,  Phys. Lett. B {\bf 767}, 289 (2017). 

%\cite{Zhang:2017zap}
\bibitem{Zhang:2017zap}
  X.~Zhang and G.~A.~Miller,
  %``Can nuclear physics explain the anomaly observed in the internal pair production in the Beryllium-8 nucleus?,''
  Phys.\ Lett.\ B {\bf 773}, 159 (2017)
  
\bibitem{ia}
I. Alikhanov, E. A. Paschos, Phys.\ Rev.\ D {\bf 97}, 115004 (2018).
%arXiv:1710.10131.

\bibitem{liang}	
Y. Liang, L.-B. Chen, C.-F. Qiao, Chin. Phys. C {\bf 41},  063105 (2017). 

\bibitem{bart}
B. Fornal, Int. J. Mod. Phys. A {\bf 32},  1730020 (2017).

\bibitem{fayet1}
P. Fayet,
%The light U boson as the mediator of a new force, coupled to a combination of Q,B,L and dark matter. 
Eur. Phys.  J. (2017) C77, 53.

\bibitem{fayet2}
P. Fayet,
%On the search for a new Spin 1 Boson.
Nucl Phys. (1981) B187, 184.

\bibitem{fayet3}
P. Fayet,
%Extra U(1)’s and new forces.
Nucl Phys. (1990) B 347, 743.

\bibitem{fayet4}
P. Fayet,
%U-boson production in e+ e- annihilations, psi, and Upsilon decays, and Light Dark Matter. 
Phys. Rev. (2007) D75 115017.

\bibitem{be8-2}
A. J. Krasznahorkay et al., arXiv:1910.10459v1 [nucl-ex].

\bibitem{prw} M. Pospelov, A. Ritz, M. B. Voloshin, Phys. Lett. B {\bf 662}, 53 (2008). 

\bibitem{pospelov} M. Pospelov,  Phys. Rev. D {\bf 80}, 095002 (2009).

\bibitem{Okun:1982xi}
  L.~B.~Okun,
% ``Limits Of Electrodynamics: Paraphotons?,''
  Sov.\ Phys.\ JETP {\bf 56}, 502 (1982)
   [Zh.\ Eksp.\ Teor.\ Fiz.\  {\bf 83}, 892 (1982)].
% 204 citations counted in INSPIRE as of 01 févr. 2016

\bibitem{Galison:1983pa}
  P.~Galison and A.~Manohar,
%  ``Two $Z'$s or not  two $Z'$s ?'',
  Phys. Lett. B {\bf 136}, 279 {(1984).}
 % doi:10.1016/0370-2693(84)91161-4
  %%CITATION = doi:10.1016/0370-2693(84)91161-4;%%
  %52 citations counted in INSPIRE as of 01 févr. 2016

\bibitem{Holdom:1985ag} 
  B.~Holdom,
%  ``Two U(1)'s and Epsilon Charge Shifts,''
  Phys.\ Lett.\ B {\bf 166}, 196 (1986).
 % doi:10.1016/0370-2693(86)91377-8
  %%CITATION = doi:10.1016/0370-2693(86)91377-8;%%
  %910 citations counted in INSPIRE as of 01 Dec 2015

\bibitem{jr}
J. Jaeckel and A. Ringwald, Ann. Rev. Nucl. Part. Sci. {\bf 60}, 405 (2010).

\bibitem{report} J. Alexander et al., arXiv:1608.08632.
 
 \bibitem{jdb}J. D. Bjorken, R. Essig, P. Schuster, and N. Toro, Phys. Rev. D {\bf 80}, 075018 (2009).
\bibitem{charm} F. Bergsma et al. (CHARM Collaboration), Phys. Lett. B {\bf 166}, 473 (1986). 
\bibitem{rio} E. M. Riordan et al., Phys. Rev. Lett. {\bf 59}, 755 (1987). 
\bibitem{e137} J. D. Bjorken, S. Ecklund, W. R. Nelson, A. Abashian, C. Church,	B. Lu, L. W. Mo, T. A. Nunamaker and P. Rassmann,
 Phys. Rev. D {\bf 38}, 3375 (1988).

%\bibitem{e137}
%J.D. Bjorken et al., Phys. Rev. D {\bf 38}, 3375 (1988).
\bibitem{konaka} A. Konaka et al., Phys. Rev. Lett. {\bf 57}, 659 (1986).
\bibitem{bross} A. Bross, M. Crisler, S. H. Pordes, J. Volk, S. Errede, and J.
Wrbanek, Phys. Rev. Lett. {\bf 67}, 2942 (1991). 
\bibitem{dav} M. Davier and H. Nguyen Ngoc, Phys. Lett. B {\bf 229}, 150 (1989). 
\bibitem{ath} C. Athanassopoulos et al. (LSND Collaboration), Phys. Rev. C {\bf 58}, 2489 (1998). 
\bibitem{nomad} P. Astier et al. (NOMAD Collaboration), Phys. Lett. B {\bf 506}, 27 (2001). 
\bibitem{e787} S. Adler et al. (E787 Collaboration), Phys. Rev. D {\bf 70}, 037102 (2004).
\bibitem{essig1} R. Essig, R. Harnik, J. Kaplan, and N. Toro, Phys. Rev. D {\bf 82}, 113008 (2010).
\bibitem{blum} J. Blumlein and J. Brunner, Phys. Lett. B {\bf 701}, 155 (2011).
\bibitem{sg1}S.N. Gninenko, Phys. Lett. B {\bf 713}, 244 (2012). 
\bibitem{blum1}J. Blumlein and J. Brunner, Phys. Lett. B {\bf 731}, 320 (2014). 
\bibitem{sarah1}
S. Andreas, C. Niebuhr, and A. Ringwald, Phys. Rev. D {\bf 86}, 095019 (2012).
\bibitem{apex} S. Abrahamyan et al. (APEX Collaboration), Phys. Rev. Lett. {\bf 107}, 191804 (2011). 
\bibitem{merkel} H. Merkel et al., Phys. Rev. Lett. {\bf 112}, 221802 (2014).
\bibitem{merkel1} H. Merkel et al. (A1 Collaboration), Phys. Rev. Lett. {\bf 106}, 251802 (2011). 
\bibitem{babar} B. Aubert et al. (BABAR Collaboration), Phys. Rev. Lett. {\bf 103}, 081803 (2009).
\bibitem{curt} D. Curtin et al., Phys. Rev. D {\bf 90}, 075004 (2014). 
\bibitem{babar1} J. P. Lees et al. (BABAR Collaboration), Phys. Rev. Lett. {\bf 113}, 201801 (2014). 
\bibitem{bern} G. Bernardi, G. Carugno, J. Chauveau, F. Dicarlo, M. Dris et al., Phys. Lett. B {\bf 166}, 479 (1986). 
\bibitem{sindrum} R. Meijer Drees et al. (SINDRUM I Collaboration), Phys. Rev. Lett. {\bf 68}, 3845 (1992). 
\bibitem{kloe} F. Archilli et al. (KLOE-2 Collaboration), Phys. Lett. B {\bf 706}, 251 (2012). 
\bibitem{sg2} S. N. Gninenko, Phys. Rev. D {\bf 85}, 055027 (2012). 
\bibitem{kloe2} D. Babusci et al. (KLOE-2 Collaboration), Phys. Lett. B {\bf 720}, 111 (2013). 
\bibitem{wasa} P. Adlarson et al. (WASA-at-COSY Collaboration), Phys. Lett. B {\bf 726}, 187 (2013). 
\bibitem{hades} G. Agakishiev et al. (HADES Collaboration), Phys. Lett. B {\bf 731}, 265 (2014). 
\bibitem{phenix} A. Adare et al. (PHENIX Collaboration), Phys. Rev. C {\bf 91}, 031901 (2015). 
\bibitem{e949}A. V. Artamonov et al. (BNL-E949 Collaboration), Phys. Rev. D {\bf 79}, 092004 (2009).
\bibitem{na48} J. R. Batley et al. (NA48/2 Collaboration), Phys. Lett. B {\bf 746}, 178 (2015). 
\bibitem{pol}
V.V. Dubinina, N.P. Egorenkova, E.A. Pozharova, N.G. Polukhina, V.A. Smirnitsky, N.I. Starkov,  Phys. Atom. Nucl. {\bf 80}, 461  (2017)
[Yad. Fiz. {\bf 80}, 245  (2017)].
\bibitem{kloe3} A. Anastasi et al. (KLOE-2 Collaboration), Phys. Lett. B {\bf 757}, 356 (2016).

\bibitem{NA64Be2017}
NA64 Collaboration,
%``Search for a Hypothetical 16.7 MeV Gauge Boson and Dark Photons in the NA64 Experiment at CERN,'',
Phys. Rev. Lett. {\bf 120}, 231802 (2018).
%arXiV:1803.07748 [hep-ph].

\bibitem{Gninenko:2013rka}
S.~N.~Gninenko,
%``Search for MeV dark photons in a light-shining-through-walls experiment at CERN,''
  Phys.\ Rev.\ D {\bf 89},  075008 (2014).
 % doi:10.1103/PhysRevD.89.075008
 %  [arXiv:1308.6521 [hep-ph]].
  %%CITATION = doi:10.1103/PhysRevD.89.075008;%%
  %15 citations counted in INSPIRE as of 01 févr. 2016

%\bibitem{sngp348}
%S.N. Gninenko, Phys. Rev. D  {\bf 80}, 075018 (2009); arXiv:0906.0580 [hep-ph].

\bibitem{Andreas:2013lya} 
  S.~Andreas {\it et al.},
%``Proposal for an Experiment to Search for Light Dark Matter at the SPS,''
  arXiv:1312.3309 [hep-ex].
  %%CITATION = ARXIV:1312.3309;%%
  %14 citations counted in INSPIRE as of 20 Nov 2015

\bibitem{na64-prl} D. Banerjee et al.  (NA64 Collaboration), Phys. Rev. Lett. {\bf 118},  011802 (2017).
\bibitem{na64-prd}  D. Banerjee et al. (NA64 Collaboration), Phys. Rev. D {\bf 97}, 072002 (2018).
%arXiv:1710.00971 [hep-ex].

\bibitem{Banerjee:2015eno}
  D.~Banerjee, P.~Crivelli and A.~Rubbia,
%  ``Beam Purity for Light Dark Matter Search in Beam Dump Experiments,''
  Adv.\ High Energy Phys.\  {\bf 2015},  105730 (2015).
 % doi:10.1155/2015/105730

\bibitem{track} 
 D.~Banerjee et al., Nucl. Instr. Meth. Phys. Res. A {\bf 881}, 72 (2018).

\bibitem{srd}
E. Depero et al., Nucl. Instr. Meth. Phys. Res. A {\bf 866}, 196 (2017).
  
\bibitem{Geant4-2002}
S.~Agostinelli {\it et al.} (GEANT4 Collaboration),
%``GEANT4: A Simulation toolkit,''
Nucl.\ Instrum.\ Meth.\ A {\bf 506}, 250  (2003).

\bibitem{Geant4-2006}
J.~Allison {\it et al.},
%``Geant4 developments and applications,''
IEEE Trans.\ Nucl.\ Sci.\  {\bf 53}, 270 (2006).

\bibitem{gkkk1}
S.N. Gninenko, N.V. Krasnikov, M.M. Kirsanov, and D.V. Kirpichnikov,
%``Missing energy signature from invisible decays of dark photons at the CERN SPS,''
Phys. Rev. D {\bf 94}, 095025 (2016).
%arXiv:1604.08432.

\bibitem{DMsimulation}
S.N. Gninenko, D.V. Kirpichnikov, M.M. Kirsanov and N.V. Krasnikov,
%``The exact tree-level calculation of the dark photon production in high-energy electron scattering at the CERN SPS,''
Phys. Lett. B {\bf 782} 406 (2018).
%arXiV:1712.05706 [hep-ph].

\bibitem{vnb}
V.N. Bolotov, V.V. Isakov, V.A. Kachanov, D.B. Kakauridze,
V.M. Kutyin, Yu.D. Prokoshkin, E.A. Razuvaev, V.K. Semenov, Nucl. Phys. B {\bf 85}, 158 (1975).

\bibitem{junk}
T. Junk, 
%"Confidence Level Computation for Combining Searches with Small Statistics",
Nucl. Instrum. Meth. A {\bf 434}, 435 (1999).
%doi:10.1016/S0168-9002(99)00498-2
%arXiv:hep-ex/990200

\bibitem{limit}
G. Cowan, K. Cranmer, E. Gross, O. Vitells, 
%``Asymptotic formulae for likelihood-based tests of new physics'', 
Eur. Phys. J. C {\bf 71}, 1554 (2011).

\bibitem{Read:2002hq}
  A.~L.~Read,
 % ``Presentation of search results: The CL(s) technique,''
  J.\ Phys.\ G {\bf 28}, 2693 (2002).
  %doi:10.1088/0954-3899/28/10/313
  %%CITATION = doi:10.1088/0954-3899/28/10/313;%%
  %1920 citations counted in INSPIRE as of 07 Sep 2017

%\cite{Izaguirre:2014bca}

\bibitem{Liu:2016mqv} 
  Y.~S.~Liu, D.~McKeen and G.~A.~Miller,
 % ``Validity of the Weizs??cker-Williams approximation and the analysis of beam dump experiments: Production of a new scalar boson,''
  Phys.\ Rev.\ D {\bf 95}, 036010 (2017)
  %doi:10.1103/PhysRevD.95.036010
  %[arXiv:1609.06781 [hep-ph]].

\bibitem{Liu:2017htz} 
  Y.~S.~Liu and G.~A.~Miller,
  %``Validity of the Weizsacker-Williams approximation and the analysis of beam dump experiments: Production of an axion, a dark photon, or a new axial-vector boson,''
  Phys.\ Rev.\ D {\bf 96},  016004 (2017).
  %doi:10.1103/PhysRevD.96.016004
  %[arXiv:1705.01633 [hep-ph]].
  %%CITATION = doi:10.1103/PhysRevD.96.016004;%%

\bibitem{hd} H. Davoudiasl, H. S. Lee, and W. J. Marciano,
% Muon g-2, rare kaon decays, and parity violation from dark bosons, 
 Phys. Rev. D {\bf 89}, 095006 (2014).
 

\end{thebibliography}


\end{document} 

