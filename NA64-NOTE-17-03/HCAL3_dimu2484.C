{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_WCAL_dimuMC_12f.root");
  TFile myfile1("hist_dimu2484.root");

  TH1D* hMC = (TH1D*)myfile.Get("ehcal3");
  TH1D* hDATA = (TH1D*)myfile1.Get("ehcal3");

  c1 = new TCanvas("HCAL3_dimu2484"," ",200,10,900,600);
  hMC->SetStats(false);
  hMC->SetTitle("");
  hMC->SetXTitle("Energy in HCAL module [GeV]");
  hMC->SetYTitle("Events");
  hDATA->SetXTitle("Energy in HCAL module [GeV]");
  hDATA->SetYTitle("Events");

  Double_t NMC = 12.*46000.*200.;
  Double_t NDATA = 400000000.;
  hMC->Scale(0.7*(NDATA/NMC));

  hMC->Draw("HIST");
  hDATA->Draw("ESAME");
}
