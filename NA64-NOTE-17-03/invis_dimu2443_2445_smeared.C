{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_dimuMC32f_smeared.root");
  TFile myfile1("hist_dimu2443_2445.root");

  TH1D* hMC = (TH1D*)myfile.Get("eecal");
  TH1D* hDATA = (TH1D*)myfile1.Get("eecal");

  c1 = new TCanvas("invis_dimu2443_2445_smeared"," ",200,10,900,600);
  hMC->Rebin(4);
  hDATA->Rebin(4);
  hMC->SetStats(false);
  hMC->SetTitle("");
  hMC->SetXTitle("Energy in ECAL [GeV]");
  hMC->SetYTitle("Events");
  hDATA->SetXTitle("Energy in ECAL [GeV]");
  hDATA->SetYTitle("Events");
  hMC->SetTitleOffset(1.4, "Y");

  Double_t NMC = 32.*46000.*200.;
  Double_t NDATA = 597000000.+580000000.+596000000.;
  hMC->Scale(0.8*(NDATA/NMC));

  hMC->SetAxisRange(0., 80., "X");
  hMC->Draw("HIST");
  hDATA->Draw("ESAME");
}
