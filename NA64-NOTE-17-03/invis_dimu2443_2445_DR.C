{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_dimuMC32f.root");
  TFile myfile1("hist_dimu2443_2445.root");

  TH1D* hMC = (TH1D*)myfile.Get("eecal");
  TH1D* hDATA = (TH1D*)myfile1.Get("eecal");

  c1 = new TCanvas("invis_dimu2443_2445_DR"," ",200,10,900,600);
  hMC->Rebin(6);
  hDATA->Rebin(6);
  hMC->Sumw2();
  hDATA->Sumw2();
  hMC->SetStats(false);
  hDATA->SetStats(false);
  hMC->SetTitle("");
  hDATA->SetTitle("");
  hMC->SetXTitle("Energy in ECAL [GeV]");
  hMC->SetYTitle("RR");
  hDATA->SetXTitle("Energy in ECAL [GeV]");
  hDATA->SetYTitle("RR");
  //hMC->Integral();
  Double_t NMC = 32.*46000.*200.;
  Double_t NDATA = 597000000.+580000000.+596000000.;
  hMC->Scale(1./hMC->Integral());
  hDATA->Scale(1./hDATA->Integral());
  hDATA->Divide(hMC);

  hDATA->SetAxisRange(0., 80., "X");
  hDATA->Draw("E");
}
