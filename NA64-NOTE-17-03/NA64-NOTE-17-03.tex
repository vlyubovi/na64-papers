\documentclass[pdftex,12pt,a4paper]{article}
%\usepackage[affil-it]{authblk}
\usepackage[margin=0.7in]{geometry}
\usepackage{multicol}
\usepackage{enumerate}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd}
\usepackage[pdftex]{graphicx}
\usepackage[font={small,it}]{caption}
\usepackage[margin=8pt]{subfig}
%\usepackage{multirow}
\usepackage{longtable}
\usepackage{float}
%\usepackage{hyperref}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand\g{\gamma}
\newcommand\ee{e^+e^-}
\newcommand\aee{X\to e^+e^-}
\newcommand\ainv{X\to invisible}
\newcommand\xdecay{X \rightarrow e^+  e^-}
\newcommand\pair{e^+ e^-}

\begin{document}

\title{\vspace{-1.5cm}{
\begin{center}
%\resizebox{7cm}
\includegraphics[scale=0.47]{na64-logo-pc.pdf}
\end{center}
\vskip1.cm
%%%%%%%%%%%%%%%%%%  Put here the correct number for your Note %%%%%%%%%%%%%%%%%%%
\begin{flushright}
\normalsize{\bf
NA64-17-03-V5 \\
\today}
%March 8 2017}
\end{flushright}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Put here the Title, Author list and the Abstract
\vskip3.5cm
\Large{\bf Detection of muon pairs from $\gamma \rightarrow \mu \mu$ conversion in NA64}}
%\author{NA64 Collaboration}
\author{M. Kirsanov}
\date{\vspace{-5ex}}
\maketitle
\abstract{The detection of dimuons: conversions $\gamma Z \rightarrow \mu \mu Z$ (Z is a nucleus) in the NA64
  experiment is reported. It is used as a reference to check subdetectors and the efficiency of the
  Dark Photon detection, which of a primary goal of the experiment.}


\section{Introduction}


 The experiment NA64 is designed primarily to search for dark photons A' through missing
energy signatures in the SPS beams of electrons \cite{DarkPhotons}. It turned out that the rare
process $\gamma Z \rightarrow \mu \mu Z$ in the electromagnetic shower in the NA64 active target
(electromagnetic lead - plastic calorimeter ECAL or tungsten - plastic calorimeter WCAL) can be used
as a good reference process. One of the reasons is that the trigger requiring the energy deposition
in the active target, in the case of NA64 the calorimeters ECAL or WCAL, significantly smaller than
the electron beam energy enriches both the A' signal with invisible or visible decay mode and
the $\gamma \rightarrow \mu \mu$ process with well visible muons. The latter are identified in the
hadrom calorimeter modules of NA64.


\section{Simulation of the process $\gamma \rightarrow \mu \mu$}
 

 The code to simulate this process is present in Geant4. By default it is not activated
because it is rather rare and practically does not change the average properties of the
electromagnetic shower. In the simulation package of NA64 it is activated in the user
RunAction class of the Geant4 application. Geant4 $\gamma \rightarrow \mu \mu$ code has
a possibility to bias the cross section of this process in order to perform dedicated
simulations of enriched samples. The bias factor of 200 seems to be optimal. Biasing significantly
more than by this factor leads to some fraction of events with two conversions that practically
never happen in real life.


 \section{Selection of events with the conversion $\gamma Z \rightarrow \mu \mu Z$ in the invisible mode setup and the results.}


 First of all, the selection of electrons was performed using the synchrotron radiation detectors (SRD) \cite{SRD}.
More than 90\% of 100 GeV electrons produce the energy deposition higher than 1 MeV in all three modules, but in order
to decrease inefficiency due to pileup at high intensity only two out of three SRD modules were required to
be "good", i.e. to have the energy deposition in the range from 1 to 90 MeV. In addition, the sum of energy
depositions in all SRD modules was required to be higher than 6 MeV.

 The selection of events with energy deposition in the active target (ECAL in this case) significantly smaller than
the beam energy was partly performed by the trigger in the so called physical runs. The trigger accepted only events
with the calibrated energy deposition smaller than about 85 GeV in the 100 GeV electron beam. For more reliable selection
a stricter cut was applied in the offline selection, see below. The dimuon events in the 2D plot ECAL - HCAL can
be seen in Fig.~\ref{dimu_2D}. Note that the total energy in all 4 HCAL modules is plotted along the Y axis.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{invis_dimu_2D_2443_2445.pdf}}
\end{center}
\caption{Dimuon events in the 2D plot. ECAL energy is required to be smaller than 60 GeV}
  \label{dimu_2D}
\end{figure}

 Further selection of dimuon events was performed mainly using the energy deposition in the HCAL modules. This
rejects events with beam hadrons and lower energy electrons that accidentally pass the electron identification
and events with gamma-nuclear and electron-nuclear interactions in the active target because essentially only
muons can reach modules 1 - 3 of the HCAL, the hadrons being absorbed in the module 0. The dimuon peak in
module 2 for the data and MC is shown in Fig.~\ref{HCAL2_dimu2351} (see below for the selection cuts and
normalization).

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{HCAL2_dimu2351.pdf}}
\end{center}
\caption{Dimuon peak in the HCAL module 2 for the selected dimuons in the data (points) and MC (histogram)
in the invisible mode setup. The distributions are normalized to each other}
  \label{HCAL2_dimu2351}
\end{figure}

The following cuts were used for the selection:

\begin{itemize}
\item
$E_{ECAL} < 60$ GeV
\item
2.5 GeV $< E_{HCAL1} <$ 6.35 GeV
\item
2 GeV $< E_{HCAL3} <$ 6.35 GeV
\end{itemize}

 The energy deposition in the HCAL modules in MC was smeared by adding an additional value distributed
around zero with sigma 0.35 GeV in order to take into account worse energy resolution in data.
Hopefully this smearing will become unnecessary after the implementation of LED corrections
and careful offline calibration of the modules.

 The energy in the ECAL for the low intensity run 2351 and correspondingly normalized MC is shown
in Fig.~\ref{invis_dimu2351}.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{invis_dimu2351.pdf}}
\end{center}
\caption{Energy deposition in ECAL for the selected dimuons in the data (points with errors) and MC (histogram).
         The data from the low intensity run 2351 were used}
  \label{invis_dimu2351}
\end{figure}

The corresponding distribution for preshower is shown in Fig.~\ref{PRS_invis_dimu2351}.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{PRS_invis_dimu2351.pdf}}
\end{center}
\caption{Energy deposition in PRS for the selected dimuons in the data (points with errors) and MC (histogram).
         The data from the low intensity run 2351 were used}
  \label{PRS_invis_dimu2351}
\end{figure}

 Some muons don't reach the HCAL module 3, for this reason the small peak from one muon appears in it
as can be seen in Fig.~\ref{HCAL3_dimu2351}.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{HCAL3_dimu2351.pdf}}
\end{center}
\caption{Dimuon peak in the HCAL module 3 for the selected dimuons in the data (points) and MC (histogram)
in the invisible mode setup. The data from the low intensity run 2351 were used}
  \label{HCAL3_dimu2351}
\end{figure}


 For higher intensity runs an additional inefficiency because of pileup appears. The corresponding ECAL energy
distributions are shown in Fig.~\ref{invis_dimu2359} and Fig.~\ref{invis_dimu2443_2445}. In the normalization for
high intensity run the inefficiencies not simulated in MC, for example the inefficiency of DAQ due to dead time
and inefficiency of SRD identification due to pileup, are assumed to have the total value of 0.8. This value is
used in Fig.~\ref{invis_dimu2443_2445}. In future this value is to be estimated more accurately and as a function
of intensity.


\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{invis_dimu2359.pdf}}
\end{center}
\caption{Energy deposition in ECAL for the selected dimuons in the data (points with errors) and MC (histogram).
         The data from the medium intensity run 2359 were used}
  \label{invis_dimu2359}
\end{figure}


\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{invis_dimu2443_2445.pdf}}
\end{center}
\caption{Energy deposition in ECAL for the selected dimuons in the data (points with errors) and MC (histogram).
         The data from the high intensity runs 2443 - 2445 were used. Additional factor 0.8 was used in the MC normalization
         to take into account various inefficiencies not simulated in MC (pileup in SRD, DAQ)}
  \label{invis_dimu2443_2445}
\end{figure}


 The dependency of dimuon selection efficiency on intensity is summarized in Table~\ref{table:dimu_efficiency}.
The inefficiency of SRD and DAQ mentioned above (about 0.8) is not taken into account in the MC predictions.
Note that the runs 2443 - 2445 had a ECAL preshower included in trigger (threshold 0.5 GeV). This is taken into
account in the MC prediction, however the simulation shows that the effect is very small (less than 1\% of events
are lost).


\begin{table}[h]
  \caption{Dimuon selection results in a few runs with different intensity. The numbers in brackets in the last line
 are for the MC with pileup smearing in ECAL.}
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
Run        & I, $10^3$  & S, $10^6$ & MC prediction  & Found & Efficiency in data w.r.t MC \\
\hline
2351       &  1780      &  171      &  1383          &  1156  &  0.84        \\
\hline
2359       &  3170      &  208.5    &  1686          &  1253  &  0.74        \\
\hline
2443-2445  &  4600      &  1773     &  14339 (12261) &  8042  &  0.56 (0.66) \\
\hline
\end{tabular}
\end{center}
\label{table:dimu_efficiency}
\end{table}


 One possible reason of the inefficiency at high intensity can be the smearing of the ECAL total energy due to pileup.
The ECAL energy is more often increased as shown in Fig.~\ref{Calib2449}, which causes the increase of the mean value,
but can also be decreased because some cell can become out of time. The electron peak in the run with high intensity
is shifted by 5 GeV and has a sigma of 5 GeV, which is two times higher than the low intensity sigma. The most important
effect is the mean value shift because the selection includes ECAL energy cut. The MC simulation shows that the efficiency
with smearing is 83\% of the one without smearing. The distribution at low ECAL energy depositions in MC also changes,
see Fig.~\ref{invis_dimu2443_2445_smeared_var}. Note that in the signal selection the effect of losing events because of smearing is much
smaller because of different distribution of the ECAL energy depositions. Thus, the high intensity dimuon efficiency
that uses MC prediction with smearing is 71\%. This is the number that can be used to estimate the signal selection
efficiency correction. More accurate estimation can be done using the inefficiencies in several ECAL energy bins.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{Calib2449.pdf}}
\end{center}
\caption{Energy deposition in ECAL for the calibration high intensity run 2449}
  \label{Calib2449}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{invis_dimu2443_2445_smeared_var.pdf}}
\end{center}
\caption{Energy deposition in ECAL for the selected dimuons in the data (points with errors) and MC with ECAL
         smearing (histogram). The data from the high intensity runs 2443 -2445 were used}
  \label{invis_dimu2443_2445_smeared_var}
\end{figure}


\section{Selection of events with the conversion $\gamma Z \rightarrow \mu \mu Z$ in the visible mode setup and the results.}


 The visible mode setup differs from the previous one by positioning the compact tungsten calorimeter WCAL on the
beam line about 1.8 m upstream of the ECAL. The visible decays of A' could thus be visible as two electron tracks
that do not come from the WCAL but appear downstream of it so that the energy deposition in the veto counter VTWC
just after the WCAL is zero or is small (significantly smaller than from 2 MIPs). The selection of electrons
is similar to the invisible mode and uses the synchrotron radiation detectors (SRD) \cite{SRD}.
In the first runs of the visible mode WCAL was too close to SRD. There were many backscattered particles ant shower
tails from WCAL in the SRD module 0. For this reason the selection in these runs was performed using only two modules.
The selection of events with energy deposition in the active target (WCAL in this case) significantly smaller than
the beam energy was mainly performed by the trigger in the physical runs. The trigger accepted only events with
the calibrated energy deposition in WCAL smaller than 70 GeV in the 100 GeV electron beam. In order to avoid the
systematic error due to inaccurate preliminary calibration of active targets, a stricter cut was applied
in the offline selection, see below.

 Further selection of dimuon events is also similar to the invisible mode and was performed mainly using the energy
deposition in the HCAL modules. The dimuon peak in modules 2 and 3 is shown in Fig.~\ref{HCAL2_dimu2484} and
Fig.~\ref{HCAL3_dimu2484}.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{HCAL2_dimu2484.pdf}}
\end{center}
\caption{Energy deposition in the HCAL module 2 for the selected dimuons in the data and MC in the visible mode
setup}
  \label{HCAL2_dimu2484}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{HCAL3_dimu2484.pdf}}
\end{center}
\caption{Energy deposition in the HCAL module 3 for the selected dimuons in the data and MC in the visible mode
setup}
  \label{HCAL3_dimu2484}
\end{figure}

The cuts similar to the invisible mode were used for the selection.

 The energy in the WCAL for the data (two runs 2484 and 2485 with a total of $4\times10^8$ EOT) and correspondingly
normalized MC is shown in Fig.~\ref{WCALdimu}. In the normalization the inefficiencies not simulated in MC,
total value of 0.7, are taken into account. The corresponding distributions
in VTWC (veto counter close to WCAL) and VTEC (counter between WCAL and ECAL) are shown in Fig.~\ref{VTWCdimu}
and Fig.~\ref{VTECdimu}. It is seen that the counters are correctly calibrated, but it is difficult to exactly
reproduce the distribution in VTWC because it is wide and there are many sources of additional particles in it.
As to VTWC, the energy distribution in the data is smeared because of small amount of light transmitted to
the photomultiplier. In MC the small peak from one muon (very asymmetric conversion, the second muon is soft and
has a large angle) is seen and is well separated from the two muons peak.

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{WCAL_dimu.pdf}}
\end{center}
\caption{Energy deposition in WCAL for the selected dimuons in the data (points with errors) and MC (histogram).
         The data from two runs with $4\times10^8$ EOT were used}
  \label{WCALdimu}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{VTWC_dimu.pdf}}
\end{center}
\caption{Energy deposition in VTWC for the selected dimuons in the data and MC (histogram).}
  \label{VTWCdimu}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\includegraphics[width=0.95\textwidth]{VTEC_dimu.pdf}}
\end{center}
\caption{Energy deposition in VTEC for the selected dimuons in the data and MC (histogram).}
  \label{VTECdimu}
\end{figure}


\section{Conclusion}


  The process of gamma conversion to muons is a very important benchmark process that can be
registered in NA64. The agreement of the distributions reconstructed in data is in general in a good
agreement with the MC simulation performed with Geant 4. The number of reconstructed dimuons in data
contains most of the information about the real efficiency of the experiment to the signal of
dark matter. It can be used to optimize the selection criteria.


 
\begin{thebibliography}{299}
		
\bibitem{DarkPhotons}
S.N. Gninenko, N.V. Krasnikov, M.M. Kirsanov, D.V. Kirpichnikov, Missing energy signature from
invisible decays of dark photons at the CERN SPS, Phys. Rev. D 94, 095025 (2016), arXiV:1604.08432 [hep-ph]

\bibitem{SRD}
NA64 Collaboration, High purity 100 GeV electron identification with synchrotron radiation, arXiV:1703.05993 [hep-ex]

\end{thebibliography}
	
\end{document}
