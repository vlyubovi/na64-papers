{
  gROOT->Reset();
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->ForceStyle();

  TFile myfile("hist_dimu2443_2445.root");

  TH2D* hDATA = (TH2D*)myfile.Get("ehplot");

  c1 = new TCanvas("invis_dimu_2D_2443_2445"," ",200,10,900,600);
  hDATA->SetStats(false);
  hDATA->SetTitle("");
  hDATA->SetXTitle("Energy in ECAL [GeV]");
  hDATA->SetYTitle("Total energy in all HCAL [GeV]");

  hDATA->SetAxisRange(0., 100., "X");
  hDATA->Draw("colz");
}
