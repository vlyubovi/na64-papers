{
  gROOT->ForceStyle();

  gStyle->SetOptStat(1111111);

  // Defaults for LabelFont, LabelSize, LabelOffset
  //               42         0.035        0.005
  // Defaults for TitleFont, TitleSize, TitleOffset
  //               42         0.035        0.005
  gStyle->SetLabelOffset(0.007, "X");
  gStyle->SetTitleOffset(1.1, "X");
  gStyle->SetLabelOffset(0.007, "Y");
  gStyle->SetTitleOffset(1.1, "Y");

  //gStyle->SetHistLineColor(1);
  gStyle->SetHistLineWidth(2);
}
